import 'dart:convert';

import 'package:car_sharing_services/api_client/api_client.dart';
import 'package:car_sharing_services/models/history.dart';

Endpoint<WorkerHistoryPage> getHistory(
  String workerId, {
  int limit = 1,
  int offset = 0,
}) =>
    Endpoint(
      '/workers/$workerId/history',
      query: {
        'offset': offset,
        'limit': limit,
      },
      handlers: {
        200: (r) => WorkerHistoryPage.fromJson(jsonDecode(r.body)),
      },
      defaultHandler: (_) => throw Exception(),
    );

Endpoint<CommonHistoryInfo> getCommonHistoryInfo(String workerId) => Endpoint(
      '/workers/$workerId/history/common',
      handlers: {
        200: (r) => CommonHistoryInfo.fromJson(jsonDecode(r.body)),
      },
      defaultHandler: (_) => throw Exception(),
    );
