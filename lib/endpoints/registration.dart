import 'dart:convert';

import 'package:car_sharing_services/api_client/api_client.dart';
import 'package:car_sharing_services/models/worker.dart';
import 'package:flutter/foundation.dart';

Endpoint<String> registerWorker(
  String workerId,
  String token,
  WorkerInfo info,
  WorkerPassport passport,
  WorkerLicense license,
  WorkerCard card,
  WorkerAccount account,
) {
  var payload = {
    'worker': info.toJson(),
    'passport': passport.toJson(),
    'license': license.toJson(),
  };
  if (card != null) {
    payload['card'] = card.toJson();
  }
  if (account != null) {
    payload['account'] = account.toJson();
  }
  return Endpoint(
    '/workers/$workerId/register',
    method: 'POST',
    body: JsonBody(payload),
    auth: KeyAuth(token),
    handlers: {
      201: (r) {
        var json = jsonDecode(r.body);
        var id = json['worker']['id'];
        if (id == null) {
          throw Exception("Empty id returned from registration");
        }
        return id;
      },
    },
    defaultHandler: (_) => throw Exception(),
  );
}

Endpoint<void> updateWorker({
  @required String workerId,
  @required String token,
  Map<String, dynamic> info,
  Map<String, dynamic> passportData,
  String passportMainPhoto,
  String passportRegPhoto,
  String passportSelfiePhoto,
  Map<String, dynamic> licenseData,
  String licenseFrontPhoto,
  String licenseBackPhoto,
  WorkerCard card,
  WorkerAccount account,
}) {
  Map<String, dynamic> payload = {};
  if (info != null) {
    payload['worker'] = info;
  }
  if (passportData != null) {
    if (passportMainPhoto != null) {
      passportData['photo_main_link'] = passportMainPhoto;
    }
    if (passportRegPhoto != null) {
      passportData['photo_registration_link'] = passportRegPhoto;
    }
    if (passportSelfiePhoto != null) {
      passportData['photo_self_link'] = passportSelfiePhoto;
    }

    payload['passport'] = passportData;
  } else {
    var data = {};
    if (passportMainPhoto != null) {
      data['photo_main_link'] = passportMainPhoto;
    }
    if (passportRegPhoto != null) {
      data['photo_registration_link'] = passportRegPhoto;
    }
    if (passportSelfiePhoto != null) {
      data['photo_self_link'] = passportSelfiePhoto;
    }

    payload['passport'] = data;
  }

  if (licenseData != null) {
    if (licenseFrontPhoto != null) {
      licenseData['photo_front_link'] = licenseFrontPhoto;
    }
    if (licenseBackPhoto != null) {
      licenseData['photo_back_link'] = licenseBackPhoto;
    }

    payload['license'] = licenseData;
  } else {
    var data = {};
    if (licenseFrontPhoto != null) {
      data['photo_front_link'] = licenseFrontPhoto;
    }
    if (licenseBackPhoto != null) {
      data['photo_back_link'] = licenseBackPhoto;
    }

    payload['license'] = data;
  }

  if (card != null) {
    payload['card'] = card.toJson();
  }

  if (account != null) {
    payload['account'] = account.toJson();
  }

  return Endpoint(
    '/workers/$workerId/register',
    method: 'PUT',
    body: JsonBody(payload),
    auth: KeyAuth(token),
    handlers: {
      200: (r) {
        print('done');
      },
    },
    defaultHandler: (_) => throw Exception(),
  );
}

Endpoint<void> updateWorkerEmail({
  @required String workerId,
  @required String token,
  String email,
}) {
  var payload = {'mail': email};
  return Endpoint(
    '/workers/$workerId/register/email',
    method: 'PUT',
    body: JsonBody(payload),
    auth: KeyAuth(token),
    handlers: {
      200: (r) {
        print('done');
      },
    },
    defaultHandler: (_) => throw Exception(),
  );
}
