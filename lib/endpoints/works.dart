import 'dart:convert';

import 'package:car_sharing_services/api_client/api_client.dart';
import 'package:car_sharing_services/models/work.dart';

Endpoint<CurrentWork> startWork(
  String workerId,
  String serviceId,
  String carId, {
  String photoBeforeLink,
}) =>
    Endpoint(
      '/workers/$workerId/works',
      method: 'POST',
      body: JsonBody({
        'service_id': serviceId,
        'car_id': carId,
        'photo_before_link': photoBeforeLink ?? '',
      }),
      handlers: {
        201: (r) {
          print('hello there');
          var l = CurrentWork.fromJson(jsonDecode(r.body));
          print(l.toJson());
          return l;
        },
      },
      defaultHandler: (_) => throw Exception(),
    );

Endpoint addPhotoBeforeToWork(
  String workerId,
  String workId,
  String photoBeforeLink,
) =>
    Endpoint(
      '/workers/$workerId/works/$workId',
      method: 'PUT',
      body: JsonBody({
        'photo_before_link': photoBeforeLink,
      }),
      handlers: {200: (_) {}},
      defaultHandler: (_) => throw Exception(),
    );

Endpoint addPhotoAfterToWork(
  String workerId,
  String workId,
  String photoAfterLink,
) =>
    Endpoint(
      '/workers/$workerId/works/$workId',
      method: 'PUT',
      body: JsonBody({
        'photo_after_link': photoAfterLink,
      }),
      handlers: {200: (_) {}},
      defaultHandler: (_) => throw Exception(),
    );

Endpoint finishWork(String workerId, String workId) => Endpoint(
      '/workers/$workerId/works/$workId/finish',
      method: 'PUT',
      handlers: {200: (_) {}},
      defaultHandler: (_) => throw Exception(),
    );

Endpoint<List<CurrentWork>> getCurrentWorks(String workerId) => Endpoint(
      '/workers/$workerId/works',
      handlers: {
        200: (r) => (jsonDecode(r.body) as List)
            ?.map((e) => e == null
                ? null
                : CurrentWork.fromJson(e as Map<String, dynamic>))
            ?.where((e) => e != null)
            ?.toList(),
      },
      defaultHandler: (_) => throw Exception(),
    );

Endpoint<List<HistoryWork>> getHistoryWorks(String workerId) => Endpoint(
      '/workers/$workerId/history_works',
      handlers: {
        200: (r) => (jsonDecode(r.body) as List)
            ?.map((e) => e == null
                ? null
                : HistoryWork.fromJson(e as Map<String, dynamic>))
            ?.where((e) => e != null)
            ?.toList(),
      },
      defaultHandler: (_) => throw Exception(),
    );
