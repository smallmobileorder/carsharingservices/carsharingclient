import 'package:car_sharing_services/api_client/api_client.dart';
import 'package:flutter/foundation.dart';

Endpoint<void> sendReview({
  @required String workerId,
  @required String token,
  @required String text,
  String photoLink,
}) {
  var payload = {'text': text, 'photo_link': photoLink};
  return Endpoint(
    '/workers/$workerId/reviews',
    method: 'POST',
    body: JsonBody(payload),
    auth: KeyAuth(token),
    handlers: {
      200: (r) {
        print('done');
      },
    },
    defaultHandler: (_) => throw Exception(),
  );
}
