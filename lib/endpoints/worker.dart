import 'dart:convert';

import 'package:car_sharing_services/api_client/api_client.dart';
import 'package:car_sharing_services/models/reg_error.dart';
import 'package:car_sharing_services/models/status.dart';
import 'package:car_sharing_services/models/worker.dart';

Endpoint<Worker> getWorkerById(String workerId, String token) => Endpoint(
      '/workers/$workerId',
      auth: KeyAuth(token),
      handlers: {
        200: (r) => Worker.fromJson(jsonDecode(r.body)),
      },
      defaultHandler: (_) => null,
    );

Endpoint<String> getIdByToken(String token) => Endpoint(
      '/workers/id_by_token',
      auth: KeyAuth(token),
      handlers: {
        200: (r) => r.body,
      },
      defaultHandler: (_) => null,
    );

Endpoint<Status> getWorkerStatus(String token, String workerId) => Endpoint(
      '/workers/$workerId/status',
      auth: KeyAuth(token),
      handlers: {
        200: (r) => Status.fromJson(jsonDecode(r.body)),
      },
      defaultHandler: (_) => null,
    );

Endpoint<List<RegError>> getRegErrors(String token, String workerId) =>
    Endpoint(
      '/workers/$workerId/errors',
      auth: KeyAuth(token),
      handlers: {
        200: (r) {
          List<RegError> errors = [];
          var data = jsonDecode(r.body);
          for (var error in data){
            print(error);
            errors.add(RegError.fromJson(error['error']));
          }
          return errors;
        },
      },
      defaultHandler: (_) => null,
    );
