import 'dart:convert';

import 'package:car_sharing_services/api_client/api_client.dart';
import 'package:car_sharing_services/models/store_item.dart';

Endpoint<List<StoreItem>> getCarParams(String carId) => Endpoint(
      '/cars/$carId/params',
      handlers: {
        200: (r) => (jsonDecode(r.body) as List)
            ?.map((e) => e == null
                ? null
                : StoreItem.fromJson(e as Map<String, dynamic>))
            ?.where((e) => e != null)
            ?.toList(),
      },
      defaultHandler: (_) => throw Exception(),
    );

Endpoint<bool> addToCarParams(
        String workerId, String carId, String storeItemId, int amount) =>
    Endpoint(
      '/cars/$carId/params/add',
      method: 'POST',
      body: JsonBody({
        'worker_id': workerId,
        'nomenclature_id': storeItemId,
        'amount': amount
      }),
      handlers: {
        200: (r) => true,
      },
      defaultHandler: (_) => false,
    );

Endpoint<bool> removeFromCarParams(
        String workerId, String carId, String storeItemId, int amount) =>
    Endpoint(
      '/cars/$carId/params/remove',
      method: 'POST',
      body: JsonBody({
        'worker_id': workerId,
        'nomenclature_id': storeItemId,
        'amount': amount
      }),
      handlers: {
        200: (r) => true,
      },
      defaultHandler: (_) => false,
    );
