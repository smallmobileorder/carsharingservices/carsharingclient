import 'dart:convert';

import 'package:car_sharing_services/api_client/api_client.dart';
import 'package:car_sharing_services/models/region.dart';
import 'package:car_sharing_services/models/services.dart';

Endpoint<List<Region>> getAllRegions() =>
  Endpoint(
    '/regions',
    handlers: {
      200: (r) =>
        (jsonDecode(r.body) as List)
          ?.map((e) =>
        e == null ? null : Region.fromJson(e as Map<String, dynamic>))
          ?.where((e) => e != null)
          ?.toList(),
    },
    defaultHandler: (_) => throw Exception(),
  );

Endpoint<List<Agent>> getAllAgentForRegion(String regionId) =>
  Endpoint(
    '/regions/$regionId/agents',
    handlers: {
      200: (r) =>
        (jsonDecode(r.body) as List)
          ?.map((e) =>
        e == null ? null : Agent.fromJson(e as Map<String, dynamic>))
          ?.where((e) => e != null)
          ?.toList(),
    },
    defaultHandler: (_) => throw Exception(),
  );

Endpoint<List<Category>> getAllCategoriesForAgent(String agentId) =>
  Endpoint(
    '/agents/$agentId/categories',
    handlers: {
      200: (r) =>
        (jsonDecode(r.body) as List)
          ?.map((e) =>
        e == null ? null : Category.fromJson(e as Map<String, dynamic>))
          ?.where((e) => e != null)
          ?.toList(),
    },
    defaultHandler: (_) => throw Exception(),
  );

Endpoint<List<Service>> getAllServicesForCategory(String categoryId) =>
  Endpoint(
    '/categories/$categoryId/services',
    handlers: {
      200: (r) =>
        (jsonDecode(r.body) as List)
          ?.map((e) =>
        e == null ? null : Service.fromJson(e as Map<String, dynamic>))
          ?.where((e) => e != null)
          ?.toList(),
    },
    defaultHandler: (_) => throw Exception(),
  );

Endpoint<List<FullAgent>> getFullAgentsForRegion(String regionId) =>
  Endpoint(
    '/regions/$regionId/services',
    handlers: {
      200: (r) =>
        (jsonDecode(r.body) as List)
          ?.map((e) =>
        e == null ? null : FullAgent.fromJson(e as Map<String, dynamic>))
          ?.where((e) => e != null)
          ?.toList(),
    },
    defaultHandler: (_) => throw Exception(),
  );
