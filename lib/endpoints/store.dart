import 'dart:convert';

import 'package:car_sharing_services/api_client/api_client.dart';
import 'package:car_sharing_services/models/store_item.dart';
import 'package:car_sharing_services/models/storehouse.dart';

Endpoint<List<Storehouse>> getStorehouses() => Endpoint(
      '/stores',
      handlers: {
        200: (r) => (jsonDecode(r.body) as List)
            ?.map((e) => e == null
                ? null
                : Storehouse.fromJson(e as Map<String, dynamic>))
            ?.where((e) => e != null)
            ?.toList(),
      },
      defaultHandler: (_) => throw Exception(),
    );

Endpoint<List<StorehouseWithItems>> getStorehousesWithItems() => Endpoint(
      '/get-all-in-stores',
      handlers: {
        200: (r) => (jsonDecode(r.body) as List)
            ?.map((e) => e == null
                ? null
                : StorehouseWithItems.fromJson(e as Map<String, dynamic>))
            ?.where((e) => e != null)
            ?.toList(),
      },
      defaultHandler: (_) => throw Exception(),
    );

Endpoint<List<StoreItem>> getStorehouseItems(String storehouseId) => Endpoint(
      '/get-all-in-stores?store_id=$storehouseId',
      handlers: {
        200: (r) => (jsonDecode(r.body) as List)
            ?.map((e) => e == null
                ? null
                : StoreItem.fromJson(e as Map<String, dynamic>))
            ?.where((e) => e != null)
            ?.toList(),
      },
      defaultHandler: (_) => throw Exception(),
    );

Endpoint<List<StoreItem>> getInventory(String workerId) => Endpoint(
      '/workers/$workerId/inventory',
      handlers: {
        200: (r) => (jsonDecode(r.body) as List)
            ?.map((e) => e == null
                ? null
                : StoreItem.fromJson(e as Map<String, dynamic>))
            ?.where((e) => e != null)
            ?.toList(),
      },
      defaultHandler: (_) => throw Exception(),
    );

Endpoint<bool> addToInventory(
        String workerId, String storehouseId, String storeItemId, int amount) =>
    Endpoint(
      '/workers/$workerId/inventory/add',
      method: 'POST',
      body: JsonBody({
        'store_id': storehouseId,
        'nomenclature_id': storeItemId,
        'amount': amount
      }),
      handlers: {
        200: (r) => true,
      },
      defaultHandler: (_) => false,
    );

Endpoint<bool> removeFromInventory(
        String workerId, String storehouseId, String storeItemId, int amount) =>
    Endpoint(
      '/workers/$workerId/inventory/remove',
      method: 'POST',
      body: JsonBody({
        'store_id': storehouseId,
        'nomenclature_id': storeItemId,
        'amount': amount
      }),
      handlers: {
        200: (r) => true,
      },
      defaultHandler: (_) => false,
    );
