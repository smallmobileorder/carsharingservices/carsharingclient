import 'dart:convert';

import 'package:car_sharing_services/api_client/api_client.dart';
import 'package:car_sharing_services/models/car.dart';

Endpoint<Car> getCarByNumber(String number) => Endpoint(
      '/cars',
      query: {'number': number},
      handlers: {200: (r) => Car.fromJson(jsonDecode(r.body))},
      defaultHandler: (_) => throw Exception(),
    );
