import 'package:car_sharing_services/service/firebase/auth/auth_service.dart';
import 'package:car_sharing_services/service/storage/storage_service.dart';

class UserRepository {
  Future<String> uid() async {
    AuthService _authService = AuthService();
    var user = _authService.currentUser;
    if (user == null) {
      return await StorageService.getUid();
    } else {
      return user?.uid;
    }
  }
}