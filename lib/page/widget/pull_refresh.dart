import 'package:flutter/material.dart';

import 'loading_container.dart';

class PullRefresh extends StatefulWidget {
  final Widget child;
  final List<Widget> children;
  final Future Function() onRefresh;
  final Future Function() onInit;

  const PullRefresh({
    Key key,
    @required this.child,
    this.children,
    @required this.onRefresh,
    this.onInit,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _PullRefreshState();
}

class _PullRefreshState extends State<PullRefresh>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  Animation animation;

  double dy = 0;
  bool loading = false;
  final double pullHeight = 60;
  final double maxPullHeight = 150;

  @override
  void initState() {
    animationController = AnimationController(
      duration: Duration(milliseconds: 300),
      vsync: this,
    );
    animation = CurveTween(curve: Curves.easeOut).animate(animationController)
      ..addListener(() {
        setState(() {
          dy *= animation.value;
          if (loading) {
            dy = dy.clamp(pullHeight, MediaQuery.of(context).size.height);
          }
        });
      });
    if (widget.onInit != null) {
      widget.onInit();
    }
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Stack(
        alignment: Alignment.topCenter,
        children: [
          Transform.translate(
            offset: Offset(0, dy ?? 0),
            child: widget.child,
          ),
          Opacity(
            opacity: ((dy - 34) / (pullHeight - 34)).clamp(0.0, 1.0),
            child: LoadingContainer(strokeWidth: 2),
          ),
        ],
      ),
      onVerticalDragStart: (_) {
        animationController.stop();
      },
      onVerticalDragUpdate: (drag) {
        setState(() {
          dy += drag.delta.dy * (maxPullHeight - dy) / maxPullHeight;
          /*if (!listViewEnabled && dy < 0) {
            scrollController.jumpTo(-dy);
            setState(() => listViewEnabled = true);
          }*/
          dy = dy.clamp(0.0, MediaQuery.of(context).size.height);
        });
      },
      onVerticalDragEnd: (_) async {
        if (dy > pullHeight) {
          loading = true;
          animationController.reverse(from: 1);
          await widget.onRefresh();
        }
        loading = false;
        animationController.reverse(from: 1);
      },
    );
  }
}
