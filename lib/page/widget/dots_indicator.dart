import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DotsIndicator extends AnimatedWidget {
  DotsIndicator({
    this.controller,
    this.itemCount,
    this.currentDotColor: ColorStyle.mainColor,
    this.othersDotColor: ColorStyle.white,
  }) : super(listenable: controller);

  final PageController controller;
  final int itemCount;
  final Color currentDotColor;
  final Color othersDotColor;
  static const double _kDotSize = 8.0;
  static const double _kDotSpacing = 18.0;

  Widget _buildDot(int index) {
    bool next = false;
    bool previous = false;
    if ((controller?.page?.round() ?? 1) == index) next = true;
    if ((controller?.page?.floor() ?? 0) == index) previous = true;
    double opacity = 0.3;
    if (next) opacity = ((controller?.page ?? 0) % 1.00);
    if (previous) opacity = ((1 - ((controller?.page ?? 0) % 1.00)));
    opacity = opacity < 0.3 ? 0.3 : opacity;
    return Container(
      width: _kDotSpacing,
      child: Center(
        child: Material(
          color: currentDotColor.withOpacity(opacity),
          type: MaterialType.circle,
          child: Container(
            width: _kDotSize,
            height: _kDotSize,
          ),
        ),
      ),
    );
  }

  Widget build(BuildContext context) {
    return Container(
        //padding: EdgeInsets.only(bottom: 18, top: 18),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: List<Widget>.generate(itemCount, _buildDot),
        ));
  }
}
