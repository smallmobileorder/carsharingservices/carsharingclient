import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/material.dart';

class LoadingContainer extends StatelessWidget {
  final Color color;
  final double strokeWidth;
  final double value;

  LoadingContainer({
    Key key,
    this.color = ColorStyle.mainColor,
    this.strokeWidth = 5.0,
    this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CircularProgressIndicator(
      strokeWidth: strokeWidth,
      value: value,
      valueColor: AlwaysStoppedAnimation<Color>(
        color,
      ),
    );
  }
}
