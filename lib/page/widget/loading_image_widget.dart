import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LoadingImageWidget extends StatefulWidget {
  final bool enablePhotoView;
  final String path;
  final BoxFit fit;
  final Color color;

  const LoadingImageWidget({
    Key key,
    this.enablePhotoView = false,
    @required this.path,
    this.fit,
    this.color,
  }) : super(key: key);

  createState() => _LoadingImageWidgetState();
}

class _LoadingImageWidgetState extends State<LoadingImageWidget> {
  Widget image;

  @override
  void initState() {
    super.initState();
    if (widget.path?.contains('http') ?? false) {
      image = networkImage();
    } else {
      image = localImage();
    }
  }

  @override
  Widget build(BuildContext context) {
    return (widget?.enablePhotoView ?? false)
        ? Hero(
            tag: widget?.path ?? 'default',
            child: InkWell(
              child: image,
              //onTap: openImageViewer,
            ))
        : image;
  }

  networkImage() {
    if (widget?.path?.contains('.svg') ?? false) {
      return SvgPicture.network(
        '${widget?.path ?? ''}',

        // fit: widget?.fit,
      );
    } else {
      return Image(
        image: CachedNetworkImageProvider(
          '${widget?.path ?? ''}',
        ),
//         fit: BoxFit.cover,
      );
    }
  }

  Widget localImage() {
    if (widget?.path?.contains('.svg') ?? false) {
      return SvgPicture.asset(
        '${widget?.path ?? ''}',
        color: widget?.color,
      );
    } else {
      return Image.asset(
        '${widget?.path ?? ''}',
        color: widget?.color,
      );
    }
  }

//  void openImageViewer() {
//    Navigator.of(context).push(CupertinoPageRoute(
//        fullscreenDialog: true,
//        builder: (_) => ImageViewer(
//          image: image,
//          heroTag: widget?.url ?? 'default',
//        )));
//  }
}
