import 'dart:io';

import 'package:app_settings/app_settings.dart';
import 'package:flutter/material.dart';

import 'custom_cupertino_alert_dialog.dart';

class RequestPermissionDialog extends StatelessWidget {
  final NoPermissionReason reason;

  const RequestPermissionDialog({Key key, @required this.reason})
      : super(key: key);

  static final String _photos = 'Чтобы Вы могли загружать фотографии, '
      'Приложению CarSharingServices нужен доступ к галерее.\n'
      'Вы можете включить доступ в настройках приложения.';

  static final String _camera = 'Чтобы Вы могли снимать фотографии, '
      'Приложению CarSharingServices нужен доступ к камере.\n'
      'Вы можете включить доступ в настройках приложения.';

  @override
  Widget build(BuildContext context) {
    String text;
    switch (reason) {
      case NoPermissionReason.CAMERA:
        text = _camera;
        break;
      case NoPermissionReason.PHOTOS:
        text = _photos;
        break;
    }
    return CustomCupertinoAlertDialog(
      title: Text('Разрешите доступ'),
      content: Text(text),
      actions: <Widget>[
        CustomCupertinoDialogAction(
          child: Text('Не сейчас'),
          isDefaultAction: true,
          onPressed: () => Navigator.pop(context),
        ),
        CustomCupertinoDialogAction(
          child: Text('Настройки'),
          onPressed: () {
            Platform.isAndroid
                ? AppSettings.openAppSettings()
                : AppSettings.openNotificationSettings();
            Navigator.pop(context);
          },
        ),
      ],
    );
  }
}

enum NoPermissionReason {
  CAMERA,
  PHOTOS,
}
