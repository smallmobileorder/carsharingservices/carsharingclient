import 'package:car_sharing_services/page/widget/loading_container.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:rxdart/rxdart.dart';

class SmartPullRefresh extends StatefulWidget {
  final Widget child;
  final Function onRefresh;
  final RefreshController controller;

  const SmartPullRefresh({
    Key key,
    @required this.controller,
    @required this.onRefresh,
    @required this.child,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SmartPullRefreshState();
}

class _SmartPullRefreshState extends State<SmartPullRefresh> {
  BehaviorSubject<double> progress = BehaviorSubject();

  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
      controller: widget.controller,
      onOffsetChange: (_, offset) {
        /// 80 - smart refreshing trigger distance (hardcoded in plugin)
        double val = offset.clamp(0, 80) / 80;
        progress.add(val);
      },
      onRefresh: widget.onRefresh,
      physics: BouncingScrollPhysics(),
      header: CustomHeader(
        builder: (_, status) {
          if (status == RefreshStatus.idle) {
            return Center(
              child: StreamBuilder<double>(
                stream: progress.stream,
                builder: (_, snapshot) => SizedBox(
                  width: 48,
                  height: 48,
                  child: LoadingContainer(value: snapshot.data, strokeWidth: 2),
                ),
              ),
            );
          }
          return Center(
            child: SizedBox(
              width: 48,
              height: 48,
              child: LoadingContainer(strokeWidth: 2),
            ),
          );
        },
      ),
      child: widget.child,
    );
  }

  @override
  void dispose() {
    progress.close();
    super.dispose();
  }
}
