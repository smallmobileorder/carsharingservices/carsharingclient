import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';

import 'button.dart';

class NextFab extends StatelessWidget {
  final EdgeInsets padding;

  final Stream blockedStream;

  final Function nextFabPressed;

  final String text;

  final Color buttonContainerColor;

  const NextFab({
    Key key,
    this.padding = const EdgeInsets.only(left: 16, right: 16, bottom: 16),
    this.blockedStream,
    this.nextFabPressed,
    this.text,
    this.buttonContainerColor = ColorStyle.backgroundColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: buttonContainerColor,
      padding: padding,
      child: StreamBuilder<bool>(
          stream: blockedStream,
          initialData: true,
          builder: (context, snapshot) {
            return Button(
              disabled: snapshot?.data ?? true,
              onPressed: nextFabPressed,
              child: SizedBox(
                width: double.infinity,
                child: Text(
                  text ?? '',
                  textAlign: TextAlign.center,
                  style:
                      TextStyle(fontSize: 16.0, fontWeight: FontWeight.normal),
                ),
              ),
            );
          }),
    );
  }
}
