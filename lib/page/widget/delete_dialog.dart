import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'custom_cupertino_alert_dialog.dart';

class DeleteDialog extends StatelessWidget {
  final String text;

  const DeleteDialog({
    Key key,
    this.text = 'Вы действительно хотите удалить данную запись?',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCupertinoAlertDialog(
      //borderColor: ColorStyle.mainColor,
      title: Text(
        text,
        style: TextStyle(
          fontSize: 18,
          color: ColorStyle.txt,
          fontWeight: FontWeight.w500,
        ),
      ),
      actions: <Widget>[
        CustomCupertinoDialogAction(
          child: Text('Отмена'),
          onPressed: () => Navigator.of(context).pop(false),
        ),
        CustomCupertinoDialogAction(
          child: Text('Да'),
          isDefaultAction: true,
          onPressed: () => Navigator.of(context).pop(true),
        ),
      ],
    );
  }
}
