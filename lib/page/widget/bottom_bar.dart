import 'dart:async';

import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// Bottom bar with icons
class BottomBar extends StatefulWidget {
  final List<Widget> activeIcons;
  final List<Widget> disabledIcons;
  final StreamSink<int> selected;
  final Stream<int> outerSelected;
  final Color barColor;
  final Color iconColor;
  final int initSelected;
  final List<GlobalKey<NavigatorState>> navigatorKeys;

  const BottomBar({
    Key key,
    @required this.activeIcons,
    @required this.disabledIcons,
    @required this.selected,
    this.outerSelected,
    @required this.navigatorKeys,
    this.iconColor = ColorStyle.mainColor,
    this.barColor = ColorStyle.backgroundColor,
    this.initSelected = 0,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar>
    with SingleTickerProviderStateMixin {
  final double iconSize = 54;
  final double barSize = 60;
  final double iconRaising = 20;
  final double rowPadding = 20;

  AnimationController animationController;
  Animation animation;
  int selectedButton;
  int prevSelectedButton;
  double prevCircleOffset = 0;

  StreamSubscription outerSelectedSub;
  double circleOffset;

  @override
  void initState() {
    selectedButton = widget.initSelected;
    animationController = AnimationController(
      duration: Duration(milliseconds: 150),
      vsync: this,
    );
    animation = CurveTween(curve: Curves.easeOut).animate(animationController)
      ..addListener(() {
        setState(() {});
      });
    animationController.value = 1;
    outerSelectedSub = widget.outerSelected?.listen((index) {
      if (index == selectedButton) {
        widget.navigatorKeys[index].currentState.popUntil((r) => r.isFirst);
        return;
      }
      prevCircleOffset = circleOffset * selectedButton;
      prevSelectedButton = selectedButton;
      widget.selected.add(index);
      selectedButton = index;
      animationController.stop();
      animationController.forward(from: 0);
    });
    super.initState();
  }

  @override
  void dispose() {
    outerSelectedSub?.cancel();
    animationController.dispose();
    super.dispose();
  }

  ///
  /// 0 -> 10: 10-0 * val + 0 = good
  /// 10 -> 0: 0-10 * val + 10 = good
  /// 10 -> 20: 20-10 * val + 10 = good
  @override
  Widget build(BuildContext context) {
    var divider = widget.activeIcons.length - 1;
    circleOffset = MediaQuery.of(context).size.width - rowPadding * 2;
    circleOffset /= divider;
    circleOffset -= iconSize / divider;
    return Stack(
      alignment: Alignment.bottomCenter,
      children: <Widget>[
        Container(
          height: barSize,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(35),
              topLeft: Radius.circular(35),
            ),
            color: widget.barColor,
            boxShadow: [
              BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.07),
                blurRadius: 16,
              )
            ],
          ),
        ),
        Positioned(
          left: rowPadding,
          child: Transform.translate(
            offset: Offset(
              prevCircleOffset +
                  (circleOffset * selectedButton - prevCircleOffset) *
                      animation.value,
              -iconRaising,
            ),
            child: Container(
              width: iconSize,
              height: iconSize,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(iconSize / 2),
                color: widget.iconColor,
              ),
            ),
          ),
        ),
        Positioned.fill(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: rowPadding),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: List.generate(
                widget.activeIcons.length,
                    (i) => circle(
                  i == selectedButton && animation.value > 0.79
                      ? widget.activeIcons[i]
                      : widget.disabledIcons[i],
                  i,
                  i == selectedButton
                      ? animation.value * iconRaising
                      : ((prevSelectedButton != null && i == prevSelectedButton)
                      ? (1 - animation.value) * iconRaising
                      : 0),
                  circleOffset,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget circle(Widget icon, int index, double offset, double circleOffset) {
    return InkWell(
      onTap: () {
        if (index == selectedButton) {
          widget.navigatorKeys[index].currentState.popUntil((r) => r.isFirst);
          return;
        }
        prevCircleOffset = circleOffset * selectedButton;
        prevSelectedButton = selectedButton;
        widget.selected.add(index);
        selectedButton = index;
        animationController.stop();
        animationController.forward(from: 0);
      },
      splashColor: Colors.transparent,
      focusColor: Colors.transparent,
      hoverColor: Colors.transparent,
      highlightColor: Colors.transparent,
      child: Container(
        width: iconSize,
        height: iconSize,
        alignment: Alignment.center,
        child: Transform.translate(offset: Offset(0, -offset), child: icon),
      ),
    );
  }
}

/// Builder for BottomBar, put on Scaffold body
class BottomBarBuilder extends StatefulWidget {
  final List<WidgetBuilder> builders;
  final Stream<int> selected;
  final List<Widget Function(Widget)> wrappers;
  final List<Key> navigatorKeys;
  final List<NavigatorObserver> navigatorObservers;

  const BottomBarBuilder({
    Key key,
    @required this.builders,
    @required this.selected,
    @required this.navigatorKeys,
    this.wrappers,
    @required this.navigatorObservers,
  })  : assert(builders != null),
        assert((wrappers?.length ?? builders.length) == builders.length),
        super(key: key);

  @override
  State<StatefulWidget> createState() => _BottomBarBuilderState();
}

class _BottomBarBuilderState extends State<BottomBarBuilder> {
  List<Widget> pages;

  @override
  void initState() {
    pages = [];
    for (int i = 0; i < widget.builders.length; i++) {
      Widget res = Navigator(
        observers: [widget.navigatorObservers[i]],
        key: widget.navigatorKeys[i],
        onGenerateRoute: (_) => CupertinoPageRoute(
          builder: widget.builders[i],
          settings: RouteSettings(
            name: '${i}_menu',
          ),
        ),
      );
      if (widget.wrappers != null && widget.wrappers[i] != null) {
        Widget wrapped = widget.wrappers[i](res);
        pages.add(wrapped);
        continue;
      }
      pages.add(res);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<int>(
      stream: widget.selected,
      builder: (context, snapshot) {
        int selected = snapshot.data ?? pages.length - 1;
        return IndexedStack(
          children: pages,
          index: selected,
        );
      },
    );
  }
}
