import 'dart:async';
import 'dart:io';

import 'package:car_sharing_services/page/widget/loading_container.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Button extends StatefulWidget {
  final FutureOr Function() onPressed;
  final Function then;
  final Widget child;
  final ButtonStyle buttonStyle;
  final LoadingStyle loadingStyle;
  final Color color;
  final double borderRadius;
  final double borderWidth;
  final Color borderColor;
  final EdgeInsets padding;
  final EdgeInsets margin;
  final double width;
  final TextStyle textStyle;
  final bool disabled;
  final Color disabledColor;
  final List<BoxShadow> shadow;

  Button({
    Key key,
    this.onPressed,
    this.then,
    @required this.child,
    this.buttonStyle = ButtonStyle.adaptive,
    this.loadingStyle = LoadingStyle.disabling,
    this.color = ColorStyle.mainColor,
    this.borderRadius = 8,
    this.borderWidth = 0,
    this.borderColor = Colors.transparent,
    this.padding = const EdgeInsets.fromLTRB(30, 15, 30, 15),
    this.margin = const EdgeInsets.all(0),
    this.width,
    this.textStyle = const TextStyle(
      fontWeight: FontWeight.w600,
      color: Colors.white,
      fontSize: 17,
    ),
    this.disabled = false,
    this.disabledColor = ColorStyle.sideColor,
    this.shadow = const [],
  });

  @override
  State<StatefulWidget> createState() => _ButtonState();
}

class _ButtonState extends State<Button> {
  bool loading;
  bool disposed = false;

  @override
  void initState() {
    loading = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    switch (widget.buttonStyle) {
      case ButtonStyle.material:
        return materialButton();
      case ButtonStyle.cupertino:
        return cupertinoButton();
      case ButtonStyle.adaptive:
      default:
        if (Platform.isAndroid) {
          return materialButton();
        } else {
          return cupertinoButton();
        }
    }
  }

  @override
  dispose() {
    disposed = true;
    super.dispose();
  }

  /// Function returns [widget.child], if onPressed function is not in progress
  /// Else it returns Stack with loading indicator on top of it
  Widget createContentForButton() {
    Widget buttonContent;
    Widget buttonChild = DefaultTextStyle(
      child: widget.child,
      style: widget.textStyle,
    );
    if (widget.buttonStyle == ButtonStyle.cupertino && widget.borderWidth > 0) {
      buttonChild = Container(
        decoration: BoxDecoration(
          color: Colors.transparent,
          borderRadius: BorderRadius.circular(widget.borderRadius),
          border: Border.all(
            color: widget.borderColor,
            width: widget.borderWidth,
          ),
        ),
        child: buttonChild,
      );
    }
    if (loading && widget.loadingStyle == LoadingStyle.circular) {
      buttonContent = Stack(
        alignment: Alignment.center,
        children: <Widget>[
          buttonChild,
          Positioned.fill(
            child: CustomSingleChildLayout(
              delegate: _LoadingDelegate(),
              child: LoadingContainer(
                color: ColorStyle.white,
                strokeWidth: 3,
              ),
            ),
          ),
        ],
      );
    } else {
      buttonContent = buttonChild;
    }
    return buttonContent;
  }

  Widget materialButton() {
    return Container(
      width: widget.width,
      margin: widget.margin,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(widget.borderRadius),
        boxShadow: widget.shadow,
      ),
      child: RawMaterialButton(
        elevation: 0,
        padding: widget.padding,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(widget.borderRadius),
          side: BorderSide(
            width: widget.borderWidth,
            color: widget.borderColor,
            style:
                widget.borderWidth > 0 ? BorderStyle.solid : BorderStyle.none,
          ),
        ),
        splashColor: widget.color.withOpacity(0.12),
        fillColor:
            (loading || widget.disabled) ? widget.disabledColor : widget.color,
        //constraints: BoxConstraints.loose(MediaQuery.of(context).size),
        child: createContentForButton(),
        onPressed: disableOnPressedButton() ? null : onPressed,
      ),
    );
  }

  Widget cupertinoButton() {
    return Container(
      width: widget.width,
      margin: widget.margin,
      /*decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(widget.borderRadius),
        boxShadow: widget.shadow,
      ),*/
      child: CupertinoButton(
        padding: widget.padding,
        color: widget.color,
        disabledColor: widget.disabledColor,
        borderRadius: BorderRadius.circular(widget.borderRadius),
        child: createContentForButton(),
        onPressed: disableOnPressedButton() ? null : onPressed,
      ),
    );
  }

  bool disableOnPressedButton() =>
      widget.disabled ||
      widget.onPressed == null ||
      (loading && widget.loadingStyle == LoadingStyle.disabling);

  Future onPressed() async {
    if (loading) {
      return;
    }
    setState(() => loading = true);
    var res = await widget.onPressed();
    if (widget.then != null) {
      widget.then(res);
    }
    if (!disposed) {
      setState(() => loading = false);
    }
  }
}

enum ButtonStyle {
  adaptive,
  cupertino,
  material,
}

enum LoadingStyle {
  circular,
  disabling,
}

class _LoadingDelegate extends SingleChildLayoutDelegate {
  final double _biggestSize = 36;

  @override
  bool shouldRelayout(SingleChildLayoutDelegate oldDelegate) => true;

  @override
  BoxConstraints getConstraintsForChild(BoxConstraints constraints) {
    double size = constraints.smallest.shortestSide.clamp(0, _biggestSize);
    return BoxConstraints.tight(Size(size, size));
  }

  @override
  Offset getPositionForChild(Size size, Size childSize) {
    return Offset(size.width / 2 - childSize.width / 2,
        size.height / 2 - childSize.height / 2);
  }
}
