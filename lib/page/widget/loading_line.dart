import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoadingLine extends StatelessWidget {
  final double height;
  final Color color;
  final Color backgroundColor;

  const LoadingLine(
      {Key key, this.height = 2, this.color = ColorStyle.mainColor, this.backgroundColor = ColorStyle.backgroundColor})
      : super(key: key);

  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      child: LinearProgressIndicator(
        backgroundColor: backgroundColor,
        valueColor: AlwaysStoppedAnimation<Color>(color),
      ),
    );
  }
}
