import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'custom_cupertino_alert_dialog.dart';

class CustomWillPopWidget extends StatelessWidget {
  final Widget child;
  final String dialogTitle;
  final String dialogContent;
  final Function navigatorFunction;
  final bool useRootNavigator;

  const CustomWillPopWidget(
      {Key key,
      this.dialogTitle = 'Вы действительно хотите вернуться?',
      this.dialogContent = 'Придётся заново загрузить и подтвердить фотографию',
      this.navigatorFunction,
      this.useRootNavigator = true,
      this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        bool back = await showDialog(
            context: context,
            barrierDismissible: false,
            useRootNavigator: useRootNavigator,
            builder: (_) {
              return CustomCupertinoAlertDialog(
                title: Text(dialogTitle ?? ''),
                content: Text(dialogContent ?? ''),
                actions: <Widget>[
                  CustomCupertinoDialogAction(
                    child: Text('Отмена'),
                    onPressed: () => Navigator.of(context).pop(false),
                  ),
                  CustomCupertinoDialogAction(
                    isDestructiveAction: true,
                    child: Text('Да'),
                    onPressed: () => Navigator.of(context).pop(true),
                  ),
                ],
              );
            });
        if (back != true) {
          return Future.value(false);
        } else {
          if (navigatorFunction == null) {
            Navigator.of(context).pop();
          } else {
            navigatorFunction();
          }
          return Future.value(false);
        }
      },
      child: child,
    );
  }
}
