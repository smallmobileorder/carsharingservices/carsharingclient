import 'package:flutter/cupertino.dart';
import 'package:url_launcher/url_launcher.dart';

import 'custom_cupertino_alert_dialog.dart';

class UpdateAppDialog extends StatelessWidget {
  final String storeLink;
  final bool forced;

  UpdateAppDialog(this.storeLink, {this.forced = false});

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: CustomCupertinoAlertDialog(
        title: Text(
            'Версия вашего приложения устарела. Для работы требуется его обновить.'),
        actions: <Widget>[
          if (!forced)
            CustomCupertinoDialogAction(
              child: Text('Позже'),
              isDefaultAction: false,
              onPressed: () {
                //print('UPDATE LATER');
                Navigator.pop(context);
              },
            ),
          CustomCupertinoDialogAction(
            child: Text('Обновить'),
            isDefaultAction: true,
            onPressed: () {
              _launchURL(storeLink);
            },
          ),
        ],
      ),
    );
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      //print('Could not launch $url');
    }
  }
}
