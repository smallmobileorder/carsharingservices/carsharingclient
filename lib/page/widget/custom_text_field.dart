import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:car_sharing_services/util/date_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class CustomTextField extends StatelessWidget {
  final TextEditingController controller;
  final FocusNode focusNode;
  final TextInputType keyboardType;
  final List<TextInputFormatter> formatters;
  final Function(String text) validator;
  final Function(String text) onChanged;
  final Function(String text) onFieldSubmitted;
  final TextCapitalization capitalization;
  final TextInputAction textInputAction;
  final String label;
  final bool datePickMode;
  final String hint;
  final Function onTap;
  final bool enabled;
  final int maxLines;

  const CustomTextField({
    Key key,
    this.controller,
    this.focusNode,
    this.keyboardType,
    this.formatters,
    this.validator,
    @required this.label,
    this.onChanged,
    this.capitalization = TextCapitalization.words,
    this.datePickMode = false,
    this.hint,
    this.onTap,
    this.enabled = true,
    this.maxLines = 10, this.textInputAction, this.onFieldSubmitted,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          child: Text(
            label ?? '',
            style: TextStyle(fontSize: 13, color: ColorStyle.sideColor),
            textAlign: TextAlign.start,
          ),
        ),
        TextFormField(
          onFieldSubmitted: onFieldSubmitted,
          textInputAction: textInputAction,
          enabled: enabled,
          minLines: 1,
          maxLines: maxLines,
          controller: controller,
          focusNode: datePickMode ? null : focusNode,
          keyboardType: datePickMode ? TextInputType.datetime : keyboardType,
          validator: validator ??
              (value) {
                if (datePickMode) {
                  if (RegExp(r'[0-3]\d\.[0-1]\d\.[0-2]\d{3}')
                          .hasMatch(controller.text) &&
                      controller.text.length == 10) {
                    if (DateValidator.dateExist(controller.text)) {
                      return null;
                    } else {
                      return 'Некорректная дата';
                    }
                  } else {
                    return 'Некорректная дата';
                  }
                } else {
                  if ((value?.length ?? 0) < 2) return 'Некорректные данные';
                  return null;
                }
              },
          //validator,
          inputFormatters: datePickMode
              ? [
                  MaskTextInputFormatter(mask: '##.##.####', filter: {
                    "#": RegExp(r'[0-9]'),
                  })
                ]
              : formatters,
          decoration: InputDecoration(
              //helperText: '',
              suffixIcon: datePickMode
                  ? IconButton(
                      onPressed: onTap,
                      icon: Icon(Icons.calendar_today),
                    )
                  : null,
              errorMaxLines: 5,
              hintText: datePickMode ? 'ДД.ММ.ГГГГ' : hint,
              hintStyle: TextStyle(
                color: datePickMode
                    ? ColorStyle.txt
                    : ColorStyle.txt.withOpacity(0.5),
                fontSize: 18,
              ),
              border: UnderlineInputBorder(
                  borderRadius: BorderRadius.circular(0.5),
                  borderSide: BorderSide(color: ColorStyle.sideColor))),
          style: TextStyle(
            color: ColorStyle.txt,
            fontSize: 18,
          ),
          textCapitalization: capitalization,
          onChanged: datePickMode
              ? (text) {
                  if (text.length > 10) {
                    var temp = controller.selection.extentOffset;
                    controller.value = TextEditingValue(
                      text: text.substring(0, 10),
                      selection: TextSelection.fromPosition(
                        TextPosition(offset: temp > 10 ? 10 : temp),
                      ),
                    );
                  }
                  onChanged(text);
                }
              : onChanged,
          //onTap: onTap,
        )
      ],
    );
  }
}

class NoKeyboardEditableTextFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;

  @override
  bool consumeKeyboardToken() {
    return false;
  }
}
