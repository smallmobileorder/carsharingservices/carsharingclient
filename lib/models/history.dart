import 'package:flutter/cupertino.dart';
import 'package:json_annotation/json_annotation.dart';

part 'history.g.dart';

@JsonSerializable()
class HistoryMonth {
  final int month;
  @JsonKey(name: 'month_name')
  final String monthName;
  final int year;
  final int earned;
  final int payed;
  final int penalty;
  final List<HistoryTile> tiles;

  HistoryMonth(this.month, this.monthName, this.year, this.earned, this.payed,
      this.penalty, this.tiles);

  factory HistoryMonth.fromJson(Map<String, dynamic> json) =>
      _$HistoryMonthFromJson(json);

  Map<String, dynamic> toJson() => _$HistoryMonthToJson(this);

  Key getKey() {
    return ValueKey(month.toString() +
        year.toString() +
        (tiles ?? []).map((e) => e.date).join(","));
  }
}

@JsonSerializable()
class HistoryTile {
  final String name;
  final String date;
  final String time;
  final int amount;
  final bool positive;
  final String comment;
  final bool payout;

  HistoryTile(this.name, this.date, this.time, this.amount, this.positive,
      this.comment, this.payout);

  factory HistoryTile.fromJson(Map<String, dynamic> json) =>
      _$HistoryTileFromJson(json);

  Map<String, dynamic> toJson() => _$HistoryTileToJson(this);

  DateTime getDate() {
    var tokens = date.split('.');
    if (tokens.length != 3) {
      return null;
    }
    var newDate = '${tokens[2]}-${tokens[1]}-${tokens[0]}';
    return DateTime.parse(newDate);
  }
}

class WorkerHistoryPage {
  final int max;
  final int offset;
  final int lastMonth;
  final List<HistoryMonth> content;

  WorkerHistoryPage(this.max, this.offset, this.lastMonth, this.content);

  factory WorkerHistoryPage.fromJson(Map<String, dynamic> json) {
    return WorkerHistoryPage(
      json['max'] as int,
      json['offset'] as int,
      json['last_month'] as int,
      (json['content'] as List)
              ?.map((e) => e == null ? null : HistoryMonth.fromJson(e))
              ?.where((element) => element != null)
              ?.toList() ??
          [],
    );
  }
}

class CommonHistoryInfo {
  final int allSalary;
  final int allPayed;
  final int allPenalty;
  final int todayEearned;

  CommonHistoryInfo(
      this.allSalary, this.allPayed, this.allPenalty, this.todayEearned);

  factory CommonHistoryInfo.fromJson(Map<String, dynamic> json) =>
      CommonHistoryInfo(
        json['all_salary'] as int,
        json['all_payed'] as int,
        json['all_penalty'] as int,
        json['today_earned'] as int,
      );
}
