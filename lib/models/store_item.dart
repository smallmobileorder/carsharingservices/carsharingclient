import 'package:car_sharing_services/models/storehouse.dart';
import 'package:json_annotation/json_annotation.dart';

part 'store_item.g.dart';

@JsonSerializable(explicitToJson: true)
class StorehouseWithItems {
  @JsonKey(name: 'store_id')
  final String storeId;

  @JsonKey(name: 'store_name')
  final String storeName;

  @JsonKey(name: 'region_id')
  final String regionId;

  @JsonKey(name: 'region_name')
  final String regionName;

  List<StoreItem> items;

  StorehouseWithItems(
      this.storeId, this.storeName, this.regionId, this.regionName, this.items);

  factory StorehouseWithItems.fromJson(Map<String, dynamic> json) =>
      _$StorehouseWithItemsFromJson(json);

  factory StorehouseWithItems.fromStorehouse(Storehouse store,
          {List<StoreItem> items}) =>
      StorehouseWithItems(
          store.id, store.name, store.regionId, store.regionName, items);

  Map<String, dynamic> toJson() => _$StorehouseWithItemsToJson(this);
}

@JsonSerializable()
class StoreItem {
  @JsonKey(name: 'nomenclature_id')
  final String id;
  final int amount;
  final Nomenclature nomenclature;

  StoreItem(this.id,
      this.amount,
      this.nomenclature,);

  factory StoreItem.fromJson(Map<String, dynamic> json) =>
      _$StoreItemFromJson(json);

  Map<String, dynamic> toJson() => _$StoreItemToJson(this);

  factory StoreItem.stab() => StoreItem.fromJson(_json);

  factory StoreItem.stab2() => StoreItem.fromJson(_json1);

  get name => nomenclature?.name ?? 'Товар';

  get photoLink => nomenclature?.photoLink?.url ?? '';

  get measureName => nomenclature?.measure?.name ?? 'шт.';
}

@JsonSerializable()
class Nomenclature {
  final String id;
  @JsonKey(name: 'category_id')
  final String categoryId;
  final Category category;
  @JsonKey(name: 'photo_id')
  final String photoId;
  @JsonKey(name: 'photo_link')
  final PhotoLink photoLink;
  final String name;
  @JsonKey(name: 'vendor_code')
  final String vendorCode;
  @JsonKey(name: 'measure_id')
  final String measureId;
  final Measure measure;
  final List<Price> price;

  Nomenclature(this.id,
      this.categoryId,
      this.category,
      this.photoId,
      this.photoLink,
      this.name,
      this.vendorCode,
      this.measureId,
      this.measure,
      this.price);

  factory Nomenclature.fromJson(Map<String, dynamic> json) =>
      _$NomenclatureFromJson(json);

  Map<String, dynamic> toJson() => _$NomenclatureToJson(this);
}

@JsonSerializable()
class Price {
  final String id;
  @JsonKey(name: 'nomenclature_id')
  final String nomenclatureId;
  @JsonKey(name: 'region_id')
  final String regionId;
  final int price;

  Price(this.id, this.nomenclatureId, this.regionId, this.price);

  factory Price.fromJson(Map<String, dynamic> json) => _$PriceFromJson(json);

  Map<String, dynamic> toJson() => _$PriceToJson(this);
}

@JsonSerializable()
class Measure {
  final String id;
  final String name;

  Measure(this.id,
      this.name,);

  factory Measure.fromJson(Map<String, dynamic> json) =>
      _$MeasureFromJson(json);

  Map<String, dynamic> toJson() => _$MeasureToJson(this);
}

@JsonSerializable()
class PhotoLink {
  final String id;
  final String url;
  @JsonKey(name: 'date_create')
  final String dateCreate;

  PhotoLink(this.id,
      this.url,
      this.dateCreate,);

  factory PhotoLink.fromJson(Map<String, dynamic> json) =>
      _$PhotoLinkFromJson(json);

  Map<String, dynamic> toJson() => _$PhotoLinkToJson(this);
}

@JsonSerializable()
class Category {
  final String id;
  final String name;

  Category(this.id,
      this.name,);

  factory Category.fromJson(Map<String, dynamic> json) =>
      _$CategoryFromJson(json);

  Map<String, dynamic> toJson() => _$CategoryToJson(this);
}

const _json = {
  "nomenclature_id": "26c77d7a-a035-4a36-bcf9-ec657d10250d",
  "nomenclature": {
    "id": "26c77d7a-a035-4a36-bcf9-ec657d10250d",
    "category_id": "c9bf94c7-a9fe-4ee3-a852-80aeb3ad50b4",
    "category": {"id": "c9bf94c7-a9fe-4ee3-a852-80aeb3ad50b4", "name": "a"},
    "photo_id": "8a515b01-9b7a-4a8e-9c72-e3beedc67134",
    "photo_link": {
      "id": "8a515b01-9b7a-4a8e-9c72-e3beedc67134",
      "url": "test",
      "date_create": "2020-09-26T12:06:48.874625Z"
    },
    "name": "Hot Wheel from hyindai solaris 2020",
    "vendor_code": "",
    "measure_id": "44939e1e-ba00-4bf6-ac64-5591e3ec81d0",
    "measure": {"id": "44939e1e-ba00-4bf6-ac64-5591e3ec81d0", "name": "шт."},
    "price": [
      {
        "id": "2f2f6fad-4b4c-4971-857b-ca6240791109",
        "nomenclature_id": "5e02cdd1-d9ee-42bd-bb75-79e26f6475f5",
        "region_id": "be3c252b-ad00-4038-9f9d-455d7a7f337e",
        "price": 100
      },
      {
        "id": "fa677889-5e9d-465f-88a3-e1b5a758867f",
        "nomenclature_id": "5e02cdd1-d9ee-42bd-bb75-79e26f6475f5",
        "region_id": "c186a67d-3180-49e5-a037-03846a433c4d",
        "price": 200
      }
    ]
  },
  "amount": 4
};

const _json1 = {
  "nomenclature_id": "26c77d7a-a035-4a36-bcf9-ec657d10250d",
  "nomenclature": {
    "id": "26c77d7a-a035-4a36-bcf9-ec657d10250d",
    "category_id": "c9bf94c7-a9fe-4ee3-a852-80aeb3ad50b4",
    "category": {"id": "c9bf94c7-a9fe-4ee3-a852-80aeb3ad50b4", "name": "a"},
    "photo_id": "8a515b01-9b7a-4a8e-9c72-e3beedc67134",
    "photo_link": {
      "id": "8a515b01-9b7a-4a8e-9c72-e3beedc67134",
      "url": "test",
      "date_create": "2020-09-26T12:06:48.874625Z"
    },
    "name": "Hot Wheel from lancer 2008",
    "vendor_code": "",
    "measure_id": "44939e1e-ba00-4bf6-ac64-5591e3ec81d0",
    "measure": {"id": "44939e1e-ba00-4bf6-ac64-5591e3ec81d0", "name": "шт."},
    "price": [
      {
        "id": "2f2f6fad-4b4c-4971-857b-ca6240791109",
        "nomenclature_id": "5e02cdd1-d9ee-42bd-bb75-79e26f6475f5",
        "region_id": "be3c252b-ad00-4038-9f9d-455d7a7f337e",
        "price": 100
      },
      {
        "id": "fa677889-5e9d-465f-88a3-e1b5a758867f",
        "nomenclature_id": "5e02cdd1-d9ee-42bd-bb75-79e26f6475f5",
        "region_id": "c186a67d-3180-49e5-a037-03846a433c4d",
        "price": 200
      }
    ]
  },
  "amount": 3
};
