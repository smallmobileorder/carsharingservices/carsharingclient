// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'storehouse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Storehouse _$StorehouseFromJson(Map<String, dynamic> json) {
  return Storehouse(
    json['id'] as String,
    json['name'] as String,
    json['region_id'] as String,
    json['region_name'] as String,
  );
}

Map<String, dynamic> _$StorehouseToJson(Storehouse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'region_id': instance.regionId,
      'region_name': instance.regionName,
    };
