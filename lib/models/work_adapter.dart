abstract class WorkAdapter {
  String get id;

  String get name;

  String get carId;

  String get carNumber;

  String get categoryName;

  String get agentName;

  String get regionName;

  DateTime get startTime;

  DateTime get finishTime;

  String get photoBefore;

  String get photoAfter;

  int get minCost;

  int get availableFrom;

  int get availableTo;

  Duration currentTime() {
    if (startTime == null) {
      return Duration.zero;
    }
    if (finishTime == null) {
      return DateTime.now().difference(startTime);
    }
    return finishTime.difference(startTime);
  }
}
