class Page<T> {
  final int max;
  final int offset;
  final List<T> content;

  Page(this.max, this.offset, this.content);

  factory Page.empty() => Page(0, 0, []);
}
