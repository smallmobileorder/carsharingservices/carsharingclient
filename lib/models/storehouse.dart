import 'package:json_annotation/json_annotation.dart';

part 'storehouse.g.dart';

@JsonSerializable()
class Storehouse {
  final String id;
  final String name;
  @JsonKey(name: 'region_id')
  final String regionId;

  @JsonKey(name: 'region_name')
  final String regionName;

  Storehouse(
    this.id,
    this.name,
    this.regionId,
    this.regionName,
  );

  factory Storehouse.stab() => Storehouse.fromJson(_json);

  factory Storehouse.stab2() => Storehouse.fromJson(_json1);

  factory Storehouse.fromJson(Map<String, dynamic> json) =>
      _$StorehouseFromJson(json);

  Map<String, dynamic> toJson() => _$StorehouseToJson(this);
}

var _json = {
  'store_id': 'f13gdsg34gfdgdrhdrhfdg23r2',
  'name': 'Приморский склад (заглушка)',
  'region_id': 'fwgdsgw4t4543gdg43y3gdfsg435tsd'
};

var _json1 = {
  'store_id': 'f13gdsg34gfdgdrhdrhfdg23r2',
  'name': 'Балтийский склад (заглушка)',
  'region_id': 'fwgdsgw4t4543gdg43y3gdfsg435tsd'
};
