import 'package:json_annotation/json_annotation.dart';

part 'car.g.dart';

@JsonSerializable()
class Car {
  String id;

  @JsonKey(name: 'model_id')
  String modelId;

  @JsonKey(name: 'brand_id')
  String brandId;

  bool secret;
  String comment;
  String number;

  Car(
    this.id,
    this.modelId,
    this.brandId,
    this.secret,
    this.comment,
    this.number,
  );

  factory Car.fromJson(Map<String, dynamic> json) => _$CarFromJson(json);

  Map<String, dynamic> toJson() => _$CarToJson(this);
}
