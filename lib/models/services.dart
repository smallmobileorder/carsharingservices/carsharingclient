import 'package:json_annotation/json_annotation.dart';

part 'services.g.dart';

@JsonSerializable()
class Agent {
  String id;

  @JsonKey(name: 'region_id')
  String regionId;

  String name;
  String email;

  Agent(this.id, this.regionId, this.name);

  factory Agent.fromJson(Map<String, dynamic> json) => _$AgentFromJson(json);

  Map<String, dynamic> toJson() => _$AgentToJson(this);
}

@JsonSerializable()
class Category {
  String id;

  @JsonKey(name: 'agent_id')
  String agentId;

  String name;
  @JsonKey(name: 'icon_url')
  String iconUrl;

  Category(this.id, this.agentId, this.name, this.iconUrl);

  factory Category.fromJson(Map<String, dynamic> json) =>
      _$CategoryFromJson(json);

  Map<String, dynamic> toJson() => _$CategoryToJson(this);
}

@JsonSerializable()
class Service {
  String id;

  @JsonKey(name: 'category_id')
  String categoryId;

  String name;
  @JsonKey(name: 'min_cost')
  int minCost;
  @JsonKey(name: 'max_cost')
  int maxCost;
  int duration;
  @JsonKey(name: 'available_from')
  int availableFrom;
  @JsonKey(name: 'available_to')
  int availableTo;

  Service(this.id, this.categoryId, this.name, this.minCost, this.maxCost,
      this.duration, this.availableFrom, this.availableTo);

  factory Service.fromJson(Map<String, dynamic> json) =>
      _$ServiceFromJson(json);

  Map<String, dynamic> toJson() => _$ServiceToJson(this);
}

@JsonSerializable()
class FullAgent {
  String id;

  @JsonKey(name: 'region_id')
  String regionId;

  List<FullCategory> categories;

  String name;
  String email;

  FullAgent(this.id, this.regionId, this.name, this.categories);

  factory FullAgent.fromJson(Map<String, dynamic> json) =>
      _$FullAgentFromJson(json);

  Map<String, dynamic> toJson() => _$FullAgentToJson(this);

  /// Check if agent has categories that has at least one service
  bool isNotEmpty() {
    var l = categories?.where((c) => c != null && c.isNotEmpty())?.length;
    l ??= 0;
    return l > 0;
  }
}

@JsonSerializable()
class FullCategory {
  String id;

  @JsonKey(name: 'agent_id')
  String agentId;

  List<Service> services;

  String name;
  @JsonKey(name: 'icon_url')
  String iconUrl;

  FullCategory(this.id, this.agentId, this.name, this.iconUrl, this.services);

  factory FullCategory.fromJson(Map<String, dynamic> json) =>
      _$FullCategoryFromJson(json);

  Map<String, dynamic> toJson() => _$FullCategoryToJson(this);

  /// Check if category has at least one service
  bool isNotEmpty() {
    var l = services?.where((s) => s != null)?.length;
    l ??= 0;
    return l > 0;
  }
}
