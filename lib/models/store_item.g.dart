// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'store_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StorehouseWithItems _$StorehouseWithItemsFromJson(Map<String, dynamic> json) {
  return StorehouseWithItems(
    json['store_id'] as String,
    json['store_name'] as String,
    json['region_id'] as String,
    json['region_name'] as String,
    (json['items'] as List)
        ?.map((e) =>
            e == null ? null : StoreItem.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$StorehouseWithItemsToJson(
    StorehouseWithItems instance) =>
    <String, dynamic>{
      'store_id': instance.storeId,
      'store_name': instance.storeName,
      'region_id': instance.regionId,
      'region_name': instance.regionName,
      'items': instance.items?.map((e) => e?.toJson())?.toList(),
    };

StoreItem _$StoreItemFromJson(Map<String, dynamic> json) {
  return StoreItem(
    json['nomenclature_id'] as String,
    json['amount'] as int,
    json['nomenclature'] == null
        ? null
        : Nomenclature.fromJson(json['nomenclature'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$StoreItemToJson(StoreItem instance) => <String, dynamic>{
      'nomenclature_id': instance.id,
      'amount': instance.amount,
      'nomenclature': instance.nomenclature,
    };

Nomenclature _$NomenclatureFromJson(Map<String, dynamic> json) {
  return Nomenclature(
    json['id'] as String,
    json['category_id'] as String,
    json['category'] == null
        ? null
        : Category.fromJson(json['category'] as Map<String, dynamic>),
    json['photo_id'] as String,
    json['photo_link'] == null
        ? null
        : PhotoLink.fromJson(json['photo_link'] as Map<String, dynamic>),
    json['name'] as String,
    json['vendor_code'] as String,
    json['measure_id'] as String,
    json['measure'] == null
        ? null
        : Measure.fromJson(json['measure'] as Map<String, dynamic>),
    (json['price'] as List)
        ?.map(
            (e) => e == null ? null : Price.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$NomenclatureToJson(Nomenclature instance) =>
    <String, dynamic>{
      'id': instance.id,
      'category_id': instance.categoryId,
      'category': instance.category,
      'photo_id': instance.photoId,
      'photo_link': instance.photoLink,
      'name': instance.name,
      'vendor_code': instance.vendorCode,
      'measure_id': instance.measureId,
      'measure': instance.measure,
      'price': instance.price,
    };

Price _$PriceFromJson(Map<String, dynamic> json) {
  return Price(
    json['id'] as String,
    json['nomenclature_id'] as String,
    json['region_id'] as String,
    json['price'] as int,
  );
}

Map<String, dynamic> _$PriceToJson(Price instance) => <String, dynamic>{
      'id': instance.id,
      'nomenclature_id': instance.nomenclatureId,
      'region_id': instance.regionId,
      'price': instance.price,
    };

Measure _$MeasureFromJson(Map<String, dynamic> json) {
  return Measure(
    json['id'] as String,
    json['name'] as String,
  );
}

Map<String, dynamic> _$MeasureToJson(Measure instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };

PhotoLink _$PhotoLinkFromJson(Map<String, dynamic> json) {
  return PhotoLink(
    json['id'] as String,
    json['url'] as String,
    json['date_create'] as String,
  );
}

Map<String, dynamic> _$PhotoLinkToJson(PhotoLink instance) => <String, dynamic>{
      'id': instance.id,
      'url': instance.url,
      'date_create': instance.dateCreate,
    };

Category _$CategoryFromJson(Map<String, dynamic> json) {
  return Category(
    json['id'] as String,
    json['name'] as String,
  );
}

Map<String, dynamic> _$CategoryToJson(Category instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };
