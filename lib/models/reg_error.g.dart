// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reg_error.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegError _$RegErrorFromJson(Map<String, dynamic> json) {
  return RegError(
    json['id'] as int,
    json['name'] as String,
  );
}

Map<String, dynamic> _$RegErrorToJson(RegError instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };
