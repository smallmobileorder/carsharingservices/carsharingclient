class Status {
  final int id;
  final String name;

  Status(this.id, this.name);

  factory Status.fromJson(Map<String, dynamic> json) =>
      Status(json['id'] as int, json['name'] as String);
}
