// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'worker.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Worker _$WorkerFromJson(Map<String, dynamic> json) {
  return Worker(
    json['id'] as String,
    json['region_id'] as String,
    json['status_id'] as int,
    json['name'] as String,
    json['surname'] as String,
    json['second_name'] as String,
    json['male'] as bool,
    json['mail'] as String,
    json['phone'] as String,
    json['birthday'] == null
        ? null
        : DateTime.parse(json['birthday'] as String),
    json['deleted'] as bool,
  );
}

Map<String, dynamic> _$WorkerToJson(Worker instance) => <String, dynamic>{
      'id': instance.id,
      'region_id': instance.regionId,
      'status_id': instance.statusId,
      'name': instance.name,
      'surname': instance.surname,
      'second_name': instance.secondName,
      'male': instance.male,
      'mail': instance.mail,
      'phone': instance.phone,
      'birthday': instance.birthday?.toIso8601String(),
      'deleted': instance.deleted,
    };
