class Region {
  String id;
  String name;

  Region(this.id, this.name);

  factory Region.fromJson(Map<String, dynamic> json) =>
      Region(json['id'] as String, json['name'] as String);

  Map<String, dynamic> toJson() => {'id': id, 'name': name};
}
