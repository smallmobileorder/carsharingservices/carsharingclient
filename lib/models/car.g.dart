// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'car.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Car _$CarFromJson(Map<String, dynamic> json) {
  return Car(
    json['id'] as String,
    json['model_id'] as String,
    json['brand_id'] as String,
    json['secret'] as bool,
    json['comment'] as String,
    json['number'] as String,
  );
}

Map<String, dynamic> _$CarToJson(Car instance) => <String, dynamic>{
      'id': instance.id,
      'model_id': instance.modelId,
      'brand_id': instance.brandId,
      'secret': instance.secret,
      'comment': instance.comment,
      'number': instance.number,
    };
