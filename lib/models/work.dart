import 'package:car_sharing_services/models/work_adapter.dart';
import 'package:json_annotation/json_annotation.dart';

import 'car.dart';
import 'page.dart';
import 'region.dart';

part 'work.g.dart';

@JsonSerializable()
class CurrentWork extends WorkAdapter {
  String id;

  @JsonKey(name: "service_id")
  String serviceId;
  WorkService service;

  @JsonKey(name: "worker_id")
  String workerId;

  @JsonKey(name: "car_id")
  String carId;
  Car car;

  @JsonKey(name: "start_time")
  DateTime startTime;
  @JsonKey(name: "finish_time")
  DateTime finishTime;

  @JsonKey(name: "photo_before_link")
  String photoBeforeLink;
  @JsonKey(name: "photo_after_link")
  String photoAfterLink;

  CurrentWork(
    this.id,
    this.serviceId,
    this.service,
    this.workerId,
    this.carId,
    this.car, {
    this.startTime,
    this.finishTime,
    this.photoBeforeLink,
    this.photoAfterLink,
  });

  factory CurrentWork.fromJson(Map<String, dynamic> json) =>
      _$CurrentWorkFromJson(json);

  Map<String, dynamic> toJson() => _$CurrentWorkToJson(this);

//  Duration currentTime() {
//    if (startTime == null) {
//      return Duration.zero;
//    }
//    if (finishTime == null) {
//      return DateTime.now().difference(startTime);
//    }
//    return finishTime.difference(startTime);
//  }

  @override
  String get agentName => service.category.agent.name;

  @override
  String get carNumber => car.number;

  @override
  String get categoryName => service.category.name;

  @override
  String get name => service.name;

  @override
  String get photoAfter => photoAfterLink;

  @override
  String get photoBefore => photoBeforeLink;

  @override
  String get regionName => service.category.agent.region.name;

  @override
  int get availableFrom => service.availableFrom;

  @override
  int get availableTo => service.availableTo;

  @override
  int get minCost => service.minCost;
}

@JsonSerializable()
class HistoryWork extends WorkAdapter {
  final String id;

  @JsonKey(name: "worker_id")
  final String workerId;

  @JsonKey(name: "start_time")
  final DateTime startTime;

  @JsonKey(name: "finish_time")
  final DateTime finishTime;

  @JsonKey(name: "car_number")
  final String carNumber;

  @JsonKey(name: "car_id")
  final String carId;

  @JsonKey(name: "photo_before_link")
  final String photoBefore;

  @JsonKey(name: "photo_after_link")
  final String photoAfter;

  final String name;

  @JsonKey(name: "service_id")
  final String serviceId;

  @JsonKey(name: "category_name")
  final String categoryName;

  @JsonKey(name: "category_id")
  final String categoryId;

  @JsonKey(name: "agent_name")
  final String agentName;

  @JsonKey(name: "agent_id")
  final String agentId;

  @JsonKey(name: "region_name")
  final String regionName;

  @JsonKey(name: "region_id")
  final String regionId;

  @JsonKey(name: "min_cost")
  final int minCost;

  @JsonKey(name: "max_cost")
  final int maxCost;

  final Duration duration;

  @JsonKey(name: "available_from")
  final int availableFrom;

  @JsonKey(name: "available_to")
  final int availableTo;

  HistoryWork(
      this.id,
      this.workerId,
      this.startTime,
      this.finishTime,
      this.carNumber,
      this.carId,
      this.photoBefore,
      this.photoAfter,
      this.name,
      this.serviceId,
      this.categoryName,
      this.categoryId,
      this.agentName,
      this.agentId,
      this.regionName,
      this.regionId,
      this.minCost,
      this.maxCost,
      this.duration,
      this.availableFrom,
      this.availableTo);

  factory HistoryWork.fromJson(Map<String, dynamic> json) =>
      _$HistoryWorkFromJson(json);

  Map<String, dynamic> toJson() => _$HistoryWorkToJson(this);
}

@JsonSerializable()
class WorkAgent {
  String id;

  @JsonKey(name: 'region_id')
  String regionId;
  Region region;

  String name;
  String email;

  WorkAgent(this.id, this.regionId, this.region, this.name);

  factory WorkAgent.fromJson(Map<String, dynamic> json) =>
      _$WorkAgentFromJson(json);

  Map<String, dynamic> toJson() => _$WorkAgentToJson(this);
}

@JsonSerializable()
class WorkCategory {
  String id;

  @JsonKey(name: 'agent_id')
  String agentId;
  WorkAgent agent;

  String name;
  @JsonKey(name: 'icon_url')
  String iconUrl;

  WorkCategory(this.id, this.agentId, this.agent, this.name, this.iconUrl);

  factory WorkCategory.fromJson(Map<String, dynamic> json) =>
      _$WorkCategoryFromJson(json);

  Map<String, dynamic> toJson() => _$WorkCategoryToJson(this);
}

@JsonSerializable()
class WorkService {
  String id;

  @JsonKey(name: 'category_id')
  String categoryId;
  WorkCategory category;

  String name;
  @JsonKey(name: 'min_cost')
  int minCost;
  @JsonKey(name: 'max_cost')
  int maxCost;
  int duration;
  @JsonKey(name: 'available_from')
  int availableFrom;
  @JsonKey(name: 'available_to')
  int availableTo;

  WorkService(this.id, this.categoryId, this.category, this.name, this.minCost,
      this.maxCost, this.duration, this.availableFrom, this.availableTo);

  factory WorkService.fromJson(Map<String, dynamic> json) =>
      _$WorkServiceFromJson(json);

  Map<String, dynamic> toJson() => _$WorkServiceToJson(this);
}

//class CurrentWorksPage extends Page<CurrentWork> {
//  CurrentWorksPage(int max, int offset, List<CurrentWork> content)
//      : super(max, offset, content);
//
//  factory CurrentWorksPage.fromJson(Map<String, dynamic> json) {
//    return CurrentWorksPage(
//      json['max'] as int,
//      json['offset'] as int,
//      (json['content'] as List)
//          ?.map((e) => e == null
//              ? null
//              : CurrentWork.fromJson(e as Map<String, dynamic>))
//          ?.where((e) => e != null)
//          ?.toList(),
//    );
//  }
//
//  Map<String, dynamic> toJson() => <String, dynamic>{
//        'max': max,
//        'offset': offset,
//        'content': content,
//      };
//}
