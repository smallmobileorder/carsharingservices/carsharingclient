import 'package:car_sharing_services/utils/date_converter.dart';
import 'package:json_annotation/json_annotation.dart';

import 'page.dart';

part 'worker.g.dart';

@JsonSerializable()
class Worker {
  String id;
  @JsonKey(name: 'region_id')
  String regionId;
  @JsonKey(name: 'status_id')
  int statusId;
  String name;
  String surname;
  @JsonKey(name: 'second_name')
  String secondName;
  bool male;
  String mail;
  String phone;
  DateTime birthday;
  bool deleted;

  Worker(
    this.id,
    this.regionId,
    this.statusId,
    this.name,
    this.surname,
    this.secondName,
    this.male,
    this.mail,
    this.phone,
    this.birthday,
    this.deleted,
  );

  factory Worker.fromJson(Map<String, dynamic> json) => _$WorkerFromJson(json);

  Map<String, dynamic> toJson() => _$WorkerToJson(this);

  String get fullName => '${surname ?? ''} ${name ?? ''} ${secondName ?? ''}';

  String get stringBirthday => dateToString(birthday);
}

class WorkerPage extends Page {
  WorkerPage(int max, int offset, List content) : super(max, offset, content);

  factory WorkerPage.fromJson(Map<String, dynamic> json) {
    return WorkerPage(
      json['max'] as int,
      json['offset'] as int,
      (json['content'] as List)
          ?.map((e) =>
              e == null ? null : Worker.fromJson(e as Map<String, dynamic>))
          ?.where((e) => e != null)
          ?.toList(),
    );
  }
}

class WorkerInfo {
  String id;
  String name;
  String surname;
  String secondName;
  bool male;
  String mail;
  String phone;
  String birthday;

  WorkerInfo(this.id, this.name, this.surname, this.secondName, this.male,
      this.mail, this.phone, this.birthday);

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'surname': surname,
      'second_name': secondName,
      'male': male,
      'mail': mail,
      'phone': phone,
      'birthday': birthday,
    };
  }
}

class WorkerCard {
  String cardholder;
  String number;

  WorkerCard(this.cardholder, this.number);

  Map<String, dynamic> toJson() {
    return {
      'cardholder': cardholder,
      'number': number,
    };
  }
}

class WorkerAccount {
  String number;
  String receiverName;
  String bankName;
  String bic;
  String correspondentAccount;

  WorkerAccount(this.number, this.receiverName, this.bankName, this.bic,
      this.correspondentAccount);

  Map<String, dynamic> toJson() {
    return {
      'number': number,
      'receiver_name': receiverName,
      'bank_name': bankName,
      'bic': bic,
      'correspondent_account': correspondentAccount,
    };
  }
}

class WorkerLicense {
  String series;
  String number;
  String dateFrom;
  String dateTo;
  String photoFrontLink;
  String photoBackLink;

  WorkerLicense(this.series, this.number, this.dateFrom, this.dateTo,
      {this.photoFrontLink, this.photoBackLink});

  Map<String, dynamic> toJson() {
    return {
      'series': series,
      'number': number,
      'date_from': dateFrom,
      'date_to': dateTo,
      'photo_front_link': photoFrontLink,
      'photo_back_link': photoBackLink,
    };
  }
}

class WorkerPassport {
  String serial;
  String number;
  String date;
  String registration;
  String issuedBy;
  String photoMainLink;
  String photoRegistrationLink;
  String photoSelfLink;

  WorkerPassport(
    this.serial,
    this.number,
    this.date,
    this.registration,
    this.issuedBy,{
    this.photoMainLink,
    this.photoRegistrationLink,
    this.photoSelfLink,
  });

  Map<String, dynamic> toJson() {
    return {
      'serial': serial,
      'number': number,
      'date': date,
      'registration': registration,
      'issued_by': issuedBy,
      'photo_main_link': photoMainLink,
      'photo_registration_link': photoRegistrationLink,
      'photo_self_link': photoSelfLink,
    };
  }
}
