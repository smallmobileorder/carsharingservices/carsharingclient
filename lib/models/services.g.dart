// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'services.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Agent _$AgentFromJson(Map<String, dynamic> json) {
  return Agent(
    json['id'] as String,
    json['region_id'] as String,
    json['name'] as String,
  )..email = json['email'] as String;
}

Map<String, dynamic> _$AgentToJson(Agent instance) => <String, dynamic>{
      'id': instance.id,
      'region_id': instance.regionId,
      'name': instance.name,
      'email': instance.email,
    };

Category _$CategoryFromJson(Map<String, dynamic> json) {
  return Category(
    json['id'] as String,
    json['agent_id'] as String,
    json['name'] as String,
    json['icon_url'] as String,
  );
}

Map<String, dynamic> _$CategoryToJson(Category instance) => <String, dynamic>{
      'id': instance.id,
      'agent_id': instance.agentId,
      'name': instance.name,
      'icon_url': instance.iconUrl,
    };

Service _$ServiceFromJson(Map<String, dynamic> json) {
  return Service(
    json['id'] as String,
    json['category_id'] as String,
    json['name'] as String,
    json['min_cost'] as int,
    json['max_cost'] as int,
    json['duration'] as int,
    json['available_from'] as int,
    json['available_to'] as int,
  );
}

Map<String, dynamic> _$ServiceToJson(Service instance) => <String, dynamic>{
      'id': instance.id,
      'category_id': instance.categoryId,
      'name': instance.name,
      'min_cost': instance.minCost,
      'max_cost': instance.maxCost,
      'duration': instance.duration,
      'available_from': instance.availableFrom,
      'available_to': instance.availableTo,
    };

FullAgent _$FullAgentFromJson(Map<String, dynamic> json) {
  return FullAgent(
    json['id'] as String,
    json['region_id'] as String,
    json['name'] as String,
    (json['categories'] as List)
        ?.map((e) =>
            e == null ? null : FullCategory.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  )..email = json['email'] as String;
}

Map<String, dynamic> _$FullAgentToJson(FullAgent instance) => <String, dynamic>{
      'id': instance.id,
      'region_id': instance.regionId,
      'categories': instance.categories,
      'name': instance.name,
      'email': instance.email,
    };

FullCategory _$FullCategoryFromJson(Map<String, dynamic> json) {
  return FullCategory(
    json['id'] as String,
    json['agent_id'] as String,
    json['name'] as String,
    json['icon_url'] as String,
    (json['services'] as List)
        ?.map((e) =>
            e == null ? null : Service.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$FullCategoryToJson(FullCategory instance) =>
    <String, dynamic>{
      'id': instance.id,
      'agent_id': instance.agentId,
      'services': instance.services,
      'name': instance.name,
      'icon_url': instance.iconUrl,
    };
