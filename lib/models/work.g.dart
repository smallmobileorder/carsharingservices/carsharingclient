// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'work.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CurrentWork _$CurrentWorkFromJson(Map<String, dynamic> json) {
  return CurrentWork(
    json['id'] as String,
    json['service_id'] as String,
    json['service'] == null
        ? null
        : WorkService.fromJson(json['service'] as Map<String, dynamic>),
    json['worker_id'] as String,
    json['car_id'] as String,
    json['car'] == null
        ? null
        : Car.fromJson(json['car'] as Map<String, dynamic>),
    startTime: json['start_time'] == null
        ? null
        : DateTime.parse(json['start_time'] as String),
    finishTime: json['finish_time'] == null
        ? null
        : DateTime.parse(json['finish_time'] as String),
    photoBeforeLink: json['photo_before_link'] as String,
    photoAfterLink: json['photo_after_link'] as String,
  );
}

Map<String, dynamic> _$CurrentWorkToJson(CurrentWork instance) =>
    <String, dynamic>{
      'id': instance.id,
      'service_id': instance.serviceId,
      'service': instance.service,
      'worker_id': instance.workerId,
      'car_id': instance.carId,
      'car': instance.car,
      'start_time': instance.startTime?.toIso8601String(),
      'finish_time': instance.finishTime?.toIso8601String(),
      'photo_before_link': instance.photoBeforeLink,
      'photo_after_link': instance.photoAfterLink,
    };

HistoryWork _$HistoryWorkFromJson(Map<String, dynamic> json) {
  return HistoryWork(
    json['id'] as String,
    json['worker_id'] as String,
    json['start_time'] == null
        ? null
        : DateTime.parse(json['start_time'] as String),
    json['finish_time'] == null
        ? null
        : DateTime.parse(json['finish_time'] as String),
    json['car_number'] as String,
    json['car_id'] as String,
    json['photo_before_link'] as String,
    json['photo_after_link'] as String,
    json['name'] as String,
    json['service_id'] as String,
    json['category_name'] as String,
    json['category_id'] as String,
    json['agent_name'] as String,
    json['agent_id'] as String,
    json['region_name'] as String,
    json['region_id'] as String,
    json['min_cost'] as int,
    json['max_cost'] as int,
    json['duration'] == null
        ? null
        : Duration(microseconds: json['duration'] as int),
    json['available_from'] as int,
    json['available_to'] as int,
  );
}

Map<String, dynamic> _$HistoryWorkToJson(HistoryWork instance) =>
    <String, dynamic>{
      'id': instance.id,
      'worker_id': instance.workerId,
      'start_time': instance.startTime?.toIso8601String(),
      'finish_time': instance.finishTime?.toIso8601String(),
      'car_number': instance.carNumber,
      'car_id': instance.carId,
      'photo_before_link': instance.photoBefore,
      'photo_after_link': instance.photoAfter,
      'name': instance.name,
      'service_id': instance.serviceId,
      'category_name': instance.categoryName,
      'category_id': instance.categoryId,
      'agent_name': instance.agentName,
      'agent_id': instance.agentId,
      'region_name': instance.regionName,
      'region_id': instance.regionId,
      'min_cost': instance.minCost,
      'max_cost': instance.maxCost,
      'duration': instance.duration?.inMicroseconds,
      'available_from': instance.availableFrom,
      'available_to': instance.availableTo,
    };

WorkAgent _$WorkAgentFromJson(Map<String, dynamic> json) {
  return WorkAgent(
    json['id'] as String,
    json['region_id'] as String,
    json['region'] == null
        ? null
        : Region.fromJson(json['region'] as Map<String, dynamic>),
    json['name'] as String,
  )..email = json['email'] as String;
}

Map<String, dynamic> _$WorkAgentToJson(WorkAgent instance) => <String, dynamic>{
      'id': instance.id,
      'region_id': instance.regionId,
      'region': instance.region,
      'name': instance.name,
      'email': instance.email,
    };

WorkCategory _$WorkCategoryFromJson(Map<String, dynamic> json) {
  return WorkCategory(
    json['id'] as String,
    json['agent_id'] as String,
    json['agent'] == null
        ? null
        : WorkAgent.fromJson(json['agent'] as Map<String, dynamic>),
    json['name'] as String,
    json['icon_url'] as String,
  );
}

Map<String, dynamic> _$WorkCategoryToJson(WorkCategory instance) =>
    <String, dynamic>{
      'id': instance.id,
      'agent_id': instance.agentId,
      'agent': instance.agent,
      'name': instance.name,
      'icon_url': instance.iconUrl,
    };

WorkService _$WorkServiceFromJson(Map<String, dynamic> json) {
  return WorkService(
    json['id'] as String,
    json['category_id'] as String,
    json['category'] == null
        ? null
        : WorkCategory.fromJson(json['category'] as Map<String, dynamic>),
    json['name'] as String,
    json['min_cost'] as int,
    json['max_cost'] as int,
    json['duration'] as int,
    json['available_from'] as int,
    json['available_to'] as int,
  );
}

Map<String, dynamic> _$WorkServiceToJson(WorkService instance) =>
    <String, dynamic>{
      'id': instance.id,
      'category_id': instance.categoryId,
      'category': instance.category,
      'name': instance.name,
      'min_cost': instance.minCost,
      'max_cost': instance.maxCost,
      'duration': instance.duration,
      'available_from': instance.availableFrom,
      'available_to': instance.availableTo,
    };
