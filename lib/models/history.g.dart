// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'history.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HistoryMonth _$HistoryMonthFromJson(Map<String, dynamic> json) {
  return HistoryMonth(
    json['month'] as int,
    json['month_name'] as String,
    json['year'] as int,
    json['earned'] as int,
    json['payed'] as int,
    json['penalty'] as int,
    (json['tiles'] as List)
        ?.map((e) =>
            e == null ? null : HistoryTile.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$HistoryMonthToJson(HistoryMonth instance) =>
    <String, dynamic>{
      'month': instance.month,
      'month_name': instance.monthName,
      'year': instance.year,
      'earned': instance.earned,
      'payed': instance.payed,
      'penalty': instance.penalty,
      'tiles': instance.tiles,
    };

HistoryTile _$HistoryTileFromJson(Map<String, dynamic> json) {
  return HistoryTile(
    json['name'] as String,
    json['date'] as String,
    json['time'] as String,
    json['amount'] as int,
    json['positive'] as bool,
    json['comment'] as String,
    json['payout'] as bool,
  );
}

Map<String, dynamic> _$HistoryTileToJson(HistoryTile instance) =>
    <String, dynamic>{
      'name': instance.name,
      'date': instance.date,
      'time': instance.time,
      'amount': instance.amount,
      'positive': instance.positive,
      'comment': instance.comment,
      'payout': instance.payout,
    };
