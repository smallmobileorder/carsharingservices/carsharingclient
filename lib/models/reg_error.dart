import 'package:json_annotation/json_annotation.dart';

part 'reg_error.g.dart';

@JsonSerializable()
class RegError {
  int id;
  String name;

  RegError(
      this.id,
      this.name,
      );

  factory RegError.fromJson(Map<String, dynamic> json) => _$RegErrorFromJson(json);

  Map<String, dynamic> toJson() => _$RegErrorToJson(this);
}
