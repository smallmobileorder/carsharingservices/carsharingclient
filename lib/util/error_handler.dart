import 'dart:async';

import 'package:car_sharing_services/application.dart';

FutureOr errorHandler(FutureOr function,
    {errorValue,
    errorMessage = 'Error occurred',
    Function(Exception) errorFunction}) async {
  try {
    return await function;
  } catch (err) {
    logger.e('${errorMessage ?? ''}: ${err.toString()}');
    if (errorFunction != null) errorFunction(err);
    return errorValue;
  }
}
