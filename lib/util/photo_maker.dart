import 'dart:io';

import 'package:car_sharing_services/page/widget/request_permission_dialog.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

import '../application.dart';

class PhotoMaker {
  static Future<File> makePhoto(
    BuildContext context,
  ) async {
    File image;
    var res;
    await showDialog(
        context: context,
        builder: (_) => LoadImageDialog(
              onPressed: (r) => res = r,
            ));
    try {
      image = await ImagePicker.pickImage(
          source: res, maxHeight: 1000, maxWidth: 1000, imageQuality: 80);
      if (image == null) {
        image = (await ImagePicker.retrieveLostData())?.file;
      }
    } on PlatformException catch (e) {
      if (e.code == 'camera_access_denied' || e.code == 'photo_access_denied') {
        await showDialog(
          context: context,
          builder: (context) => RequestPermissionDialog(
            reason: e.code == 'camera_access_denied'
                ? NoPermissionReason.CAMERA
                : NoPermissionReason.PHOTOS,
          ),
        );
        //return image;
      } else {
        logger.e('PlatformException on getting photo: $e');
        //return image;
      }
    } catch (e) {
      logger.e('Error on getting photo: ${e.toString()}');
      //return image;
    }
    if (image != null) {
      File croppedFile = await ImageCropper.cropImage(
          sourcePath: image.path,
          androidUiSettings: AndroidUiSettings(
              toolbarTitle: 'Обработка фотографии',
              toolbarColor: ColorStyle.mainColor,
              toolbarWidgetColor: ColorStyle.white,
              initAspectRatio: CropAspectRatioPreset.original,
              lockAspectRatio: false),
          iosUiSettings: IOSUiSettings());
      image = croppedFile;
    }
    return image;
  }
}

class LoadImageDialog extends StatelessWidget {
  final Function(ImageSource) onPressed;

  const LoadImageDialog({this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
          border: Border.all(color: Theme.of(context).primaryColor, width: 2),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(height: 24),
            Text(
              'Фотография',
              style: TextStyle(
                fontSize: 17,
                fontWeight: FontWeight.bold,
              ),
            ),
            Container(height: 16),
            SizedBox(
              width: double.infinity,
              height: 36,
              child: FlatButton(
                child: Text(
                  'Загрузить с устройства',
                  style: TextStyle(fontWeight: FontWeight.normal, fontSize: 16),
                ),
                onPressed: () => onPressed == null
                    ? null
                    : () async {
                        await onPressed(ImageSource.gallery);
                        Navigator.pop(context);
                      }(),
              ),
            ),
            Divider(
              height: 10,
              indent: 40,
              endIndent: 40,
            ),
            SizedBox(
              width: double.infinity,
              height: 36,
              child: FlatButton(
                child: Text('Сделать снимок',
                    style:
                        TextStyle(fontWeight: FontWeight.normal, fontSize: 16)),
                onPressed: () => onPressed == null
                    ? null
                    : () async {
                        await onPressed(ImageSource.camera);
                        Navigator.pop(context);
                      }(),
              ),
            ),
            Container(height: 24),
          ],
        ),
      ),
    );
  }
}
