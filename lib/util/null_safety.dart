class NullSafety {
  ///Returns empty map
  static getValueFromMap(Map map, String key) {
    return (map == null || map.isEmpty) ? {}:map[key];
  }
}
