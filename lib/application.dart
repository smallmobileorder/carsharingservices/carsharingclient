import 'dart:io';

import 'package:car_sharing_services/api_client/api_client.dart';
import 'package:car_sharing_services/blocs/auth.bloc.dart';
import 'package:car_sharing_services/blocs/registration.bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:http/http.dart';
import 'package:logger/logger.dart';
import 'package:provider/provider.dart';

import 'blocs/core.bloc.dart';
import 'pages/splash.page.dart';
import 'pages/style/color_style.dart';

Logger logger = Logger();

// host + ":" + port

class Application extends StatelessWidget {
  final String serverAddr;

  const Application({Key key, this.serverAddr}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
      statusBarColor: ColorStyle.mainColor,
      //systemNavigationBarColor: ColorStyle.mainColor
    ));
    FlutterStatusbarcolor.setStatusBarColor(ColorStyle.mainColor);
    return MultiProvider(
      providers: [
        Provider<CoreBloc>(
          create: (_) => CoreBloc(),
          dispose: (_, bloc) => bloc.dispose(),
        ),
        Provider<ApiClient>(
          create: (_) => ApiClient(
            Client(),
            '',
            defaultPrefix: '/api/v1',
            defaultHost: StaticHost(serverAddr),
            logger: MyApiLogger(),
            defaultHandler: (_) => throw Exception(),
          ),
        ),
        ProxyProvider<ApiClient, AuthBloc>(
          create: (context) => AuthBloc(context, null),
          update: (context, api, bloc) => bloc
            ..context = context
            ..api = api,
          dispose: (_, bloc) => bloc.dispose(),
        ),
        ProxyProvider<ApiClient, RegistrationBloc>(
          create: (context) => RegistrationBloc(context, null),
          update: (context, api, bloc) => bloc
            ..context = context
            ..api = api,
          dispose: (_, bloc) => bloc.dispose(),
        ),
      ],
      child: MaterialApp(
        title: 'TSCars',
        themeMode: ThemeMode.light,
        theme: Platform.isIOS ? iosTheme() : androidTheme(),
        supportedLocales: [
          const Locale('ru', 'RU'),
        ],
        darkTheme: Platform.isIOS ? iosTheme() : androidTheme(),
        locale: const Locale('ru', 'RU'),
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
          const FallbackCupertinoLocalizationsDelegate(),
          const BackButtonTextDelegate(),
        ],
        home: SplashPage(),
        builder: (context, child) {
          return Scaffold(
            resizeToAvoidBottomInset: false,
            resizeToAvoidBottomPadding: true,
            body: child,
          );
        },
      ),
    );
  }

  ThemeData androidTheme() {
    return ThemeData(
      brightness: Brightness.light,
      scaffoldBackgroundColor: Colors.white,
      primaryColor: ColorStyle.mainColor,
      primarySwatch: black,
      unselectedWidgetColor: ColorStyle.sideColor,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      textSelectionColor: ColorStyle.mainColor.withOpacity(0.3),
      cursorColor: ColorStyle.mainColor,
      appBarTheme: AppBarTheme(brightness: Brightness.dark),
      textTheme: Typography.material2018(platform: TargetPlatform.iOS).black,
      cupertinoOverrideTheme: CupertinoThemeData(
        brightness: Brightness.dark,
        primaryColor: ColorStyle.mainColor,
        scaffoldBackgroundColor: Colors.white,
      ),
      pageTransitionsTheme: PageTransitionsTheme(builders: {
        TargetPlatform.android: CupertinoPageTransitionsBuilder(),
      }),
    );
  }

  ThemeData iosTheme({
    bool transparentCanvas = false,
  }) {
    return ThemeData(
      brightness: Brightness.light,
      primaryColor: ColorStyle.mainColor,
      primarySwatch: black,
      unselectedWidgetColor: ColorStyle.sideColor,
      scaffoldBackgroundColor: Colors.white,
      appBarTheme: AppBarTheme(brightness: Brightness.dark),
      cupertinoOverrideTheme: CupertinoThemeData(
        brightness: Brightness.light,
        scaffoldBackgroundColor: Colors.white,
        primaryColor: ColorStyle.mainColor,
        barBackgroundColor: Colors.white,
      ),
      textSelectionColor: ColorStyle.mainColor.withOpacity(0.3),
      cursorColor: ColorStyle.mainColor,
      canvasColor: transparentCanvas ? Colors.transparent : null,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      floatingActionButtonTheme: FloatingActionButtonThemeData(
          elevation: 0.0, highlightElevation: 0.0, hoverElevation: 0.0),
      textTheme: Typography.material2018(platform: TargetPlatform.iOS).black,
      pageTransitionsTheme: PageTransitionsTheme(builders: {
        TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
      }),
    );
  }

  static const MaterialColor black = const MaterialColor(
    0xFF000000,
    const <int, Color>{
      50: const Color(0xFF000000),
      100: const Color(0xFF000000),
      200: const Color(0xFF000000),
      300: const Color(0xFF000000),
      400: const Color(0xFF000000),
      500: const Color(0xFF000000),
      600: const Color(0xFF000000),
      700: const Color(0xFF000000),
      800: const Color(0xFF000000),
      900: const Color(0xFF000000),
    },
  );
}

class BackButtonTextOverride extends DefaultMaterialLocalizations {
  BackButtonTextOverride(Locale locale) : super();

  @override
  String get backButtonTooltip => null;
}

class BackButtonTextDelegate
    extends LocalizationsDelegate<MaterialLocalizations> {
  const BackButtonTextDelegate();

  @override
  bool isSupported(Locale locale) => true;

  @override
  Future<MaterialLocalizations> load(Locale locale) {
    return SynchronousFuture(BackButtonTextOverride(locale));
  }

  @override
  bool shouldReload(BackButtonTextDelegate old) => false;
}

class FallbackCupertinoLocalizationsDelegate
    extends LocalizationsDelegate<CupertinoLocalizations> {
  const FallbackCupertinoLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => true;

  @override
  Future<CupertinoLocalizations> load(Locale locale) =>
      DefaultCupertinoLocalizations.load(locale);

  @override
  bool shouldReload(FallbackCupertinoLocalizationsDelegate old) => false;
}

class MyApiLogger implements ApiLogger {
  @override
  void before(Endpoint ep, Request r) {
    print('Request "${ep.method.toUpperCase()}" '
        'to ${r.url} ${ep.headers ?? ''}');
  }

  @override
  void after(Endpoint ep, Response r) {
    var s = r.body.length > 100 ? r.body.substring(0, 100) + '..' : r.body;
    if (s.isEmpty) {
      s = '<empty body>';
    }
    print('Response "${ep.method.toUpperCase()}" '
        'from ${r.request.url}: ${r.statusCode} $s '
        '${r.headers ?? '<no headers>'}');
  }
}
