/// Dates
String dateToString(DateTime date) {
  return '${intToDateString(date.day)}'
      '.${intToDateString(date.month)}'
      '.${date.year}';
}

String timeToString(DateTime date) {
  return '${intToDateString(date.hour)}:${intToDateString(date.minute)}';
}

String dateTimeToString(DateTime date) {
  return dateToString(date) + " " + timeToString(date);
}

/// Durations
String durationToString(Duration d) {
  String res = '';
  if (d.inDays > 0) {
    res += '${d.inDays} ${getDaySuffix(d.inDays)} ';
  }
  if (d.inHours % 24 > 0) {
    res += '${d.inHours % 24} ч. ';
  }
  if (d.inMinutes % 60 > 0) {
    res += '${d.inMinutes % 60} мин. ';
  }
  return res.isEmpty ? '0 мин.' : res;
}

/// Utils
String intToDateString(int i) {
  if (i > 9) {
    return i.toString();
  }
  return '0$i';
}

String getDaySuffix(int day) {
  var _temp = day % 10;
  if (_temp == 1) {
    return 'день';
  } else if (_temp >= 2 && _temp <= 4) {
    return 'дня';
  } else {
    return 'дней';
  }
}
