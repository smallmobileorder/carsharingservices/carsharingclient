import 'dart:ui';

class ColorStyle {
  static const mainColor = const Color(0xFF21B561);
  static const sideColor = const Color(0xFF9F9F9F);
  static const white = const Color(0xFFF2F2F2);
  static const white2 = const Color(0xFFFEFEFE);
  static const txtCategory = const Color(0xFF22264C);
  static const txt = const Color(0xFF3F3F3F);
  static const waiting = const Color(0xFFF1AB5D);
  static const wrong = const Color(0xFFF25757);
  static const backgroundColor = const Color(0xFFF8F8F8);
  static const disabledColor = const Color(0xFFF9F9F9);
}
