import 'package:car_sharing_services/blocs/storehouse.bloc.dart';
import 'package:car_sharing_services/models/store_item.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/material.dart';

class ReturnToStoreSheet extends StatefulWidget {
  final StoreItem item;
  final StorehouseBloc bloc;

  const ReturnToStoreSheet({Key key, this.item, this.bloc}) : super(key: key);

  @override
  createState() => _ReturnToStoreSheetState();
}

class _ReturnToStoreSheetState extends State<ReturnToStoreSheet> {
  int amount;
  int returnedAmount;
  StorehouseWithItems returnedStorehouse;
  StorehouseBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = widget.bloc;
    amount = widget?.item?.amount ?? 0;
    returnedAmount = amount;
  }

  @override
  Widget build(BuildContext context) {
//    var storehouses = [
//      Storehouse.stab(),
//      Storehouse.stab2(),
//      Storehouse.stab(),
//      Storehouse.stab2(),
//      Storehouse.stab(),
//      Storehouse.stab2(),
//    ].toList(); //bloc.storehouses.value.keys.toList();

    var storehouses = bloc.storehouses?.value;
    if (storehouses != null && storehouses.isNotEmpty)
      returnedStorehouse = storehouses?.first;
    var dropdownItems = storehouses
            ?.map(
              (e) => DropdownMenuItem(
                child: Text(
                  e.storeName,
                ),
                value: e,
              ),
            )
            ?.toList() ??
        [];
    return SafeArea(
      child: SingleChildScrollView(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                alignment: Alignment.center,
                child: Text(
                  'Вернуть инвентарь на склад',
                  style: TextStyle(fontWeight: FontWeight.w500),
                ),
              ),
              Divider(),
              Container(
                height: 10,
              ),
              Text(
                'Наименование: ',
                style: TextStyle(
                    color: ColorStyle.txtCategory, fontWeight: FontWeight.w500),
              ),
              Text(
                '${widget?.item?.name}',
                // style: TextStyle(color: ColorStyle.sideColor),
                maxLines: 2,
              ),
              Container(
                height: 10,
              ),
              Text(
                'Склад: ',
                style: TextStyle(
                    color: ColorStyle.txtCategory, fontWeight: FontWeight.w500),
              ),
              StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) =>
                    DropdownButton(
                        underline: Container(),
                        isExpanded: true,
                        iconEnabledColor: ColorStyle.mainColor,
                        elevation: 0,
                        value: returnedStorehouse,
                        items: dropdownItems,
                        onChanged: (value) {
                          setState(() {
                            returnedStorehouse = value;
                          });
                        }),
              ),
              Container(
                height: 10,
              ),
              StatefulBuilder(
                  builder: (BuildContext context, StateSetter setState) {
                return Row(
                  children: [
                    Text(
                      'Количество: ',
                      style: TextStyle(
                          color: ColorStyle.txtCategory,
                          fontWeight: FontWeight.w500),
                    ),
                    IconButton(
                      color: ColorStyle.wrong,
                      icon: Icon(
                        Icons.remove_circle_outline,
                      ),
                      onPressed: (returnedAmount < 1)
                          ? null
                          : () => setState(() => returnedAmount--),
                    ),
                    Text(
                      '$returnedAmount ${widget?.item?.measureName}',
                    ),
                    IconButton(
                      color: ColorStyle.mainColor,
                      icon: Icon(
                        Icons.add_circle_outline,
                      ),
                      onPressed: (returnedAmount >= amount)
                          ? null
                          : () => setState(() => returnedAmount++),
                    )
                  ],
                );
              }),
              Container(
                height: 50,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  FlatButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    textColor: ColorStyle.white,
                    color: ColorStyle.mainColor,
                    onPressed:
                    returnedStorehouse != null ? returnToStoreHouse : null,
                    child: Text(
                      'Вернуть',
                    ),
                  ),
                ],
              )
            ],
          )),
    );
  }

  void returnToStoreHouse() async {
    await bloc.deleteFromInventory(
        storehouseId: returnedStorehouse.storeId,
        itemId: widget.item.id,
        amount: returnedAmount);
    Navigator.of(context, rootNavigator: true).pop();
  }
}
