import 'package:car_sharing_services/blocs/storehouse.bloc.dart';
import 'package:car_sharing_services/models/store_item.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/material.dart';

class TakeFromStoreSheet extends StatefulWidget {
  final StorehouseBloc bloc;

  const TakeFromStoreSheet({Key key, this.bloc}) : super(key: key);

  @override
  createState() => _TakeFromStoreSheetState();
}

class _TakeFromStoreSheetState extends State<TakeFromStoreSheet> {
  int amount;
  StorehouseWithItems storehouse;
  StoreItem item;
  StorehouseBloc bloc;
  List<StorehouseWithItems> storehouses;

  @override
  void initState() {
    super.initState();
    bloc = widget.bloc;

//    storehousesMap = {
//      Storehouse.stab(): [
//        StoreItem.stab(),
//        StoreItem.stab(),
//        StoreItem.stab(),
//        StoreItem.stab(),
//        StoreItem.stab(),
//        StoreItem.stab(),
//        StoreItem.stab(),
//        StoreItem.stab(),
//        StoreItem.stab(),
//        StoreItem.stab(),
//        StoreItem.stab(),
//        StoreItem.stab(),
//        StoreItem.stab(),
//        StoreItem.stab(),
//        StoreItem.stab(),
//        StoreItem.stab(),
//        StoreItem.stab(),
//        StoreItem.stab(),
//        StoreItem.stab(),
//        StoreItem.stab(),
//        StoreItem.stab(),
//        StoreItem.stab(),
//        StoreItem.stab(),
//      ],
//      Storehouse.stab2(): [
//        StoreItem.stab(),
//        StoreItem.stab2(),
//        StoreItem.stab(),
//      ],
//      Storehouse.stab(): [
//        StoreItem.stab(),
//        StoreItem.stab(),
//        StoreItem.stab(),
//        StoreItem.stab(),
//        StoreItem.stab(),
//      ],
//      Storehouse.stab2(): [StoreItem.stab()],
//    }; //bloc.storehouses.value.keys.toList();
    storehouses = bloc.storehouses.value;
    if (storehouses != null && storehouses.isNotEmpty) {
      storehouse = storehouses?.first;
      item = storehouse.items?.first;
    }
    amount = 1;
  }

  @override
  Widget build(BuildContext context) {
    var dropdownStorehouses = storehouses
        .map(
          (e) => DropdownMenuItem(
            child: Text(
              e.storeName,
            ),
            value: e,
          ),
        )
        .toList();

    var dropdownItems = storehouse?.items
            ?.map(
              (e) => DropdownMenuItem(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      e.name,
                    ),
                    Container(
                      height: 5,
                    ),
                    Text(
                      '${e.amount} ${e.measureName}',
                      style: TextStyle(color: ColorStyle.txt, fontSize: 13),
                    ),
                    Divider()
                  ],
                ),
                value: e,
              ),
            )
            ?.toList() ??
        [];

    return SafeArea(
      child: SingleChildScrollView(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                alignment: Alignment.center,
                child: Text(
                  'Взять со склада',
                  style: TextStyle(fontWeight: FontWeight.w500),
                ),
              ),
              Divider(),
              Container(
                height: 10,
              ),
              Text(
                'Склад: ',
                style: TextStyle(
                    color: ColorStyle.txtCategory, fontWeight: FontWeight.w500),
              ),
              DropdownButton<StorehouseWithItems>(
                  underline: Container(),
                  isExpanded: true,
                  iconEnabledColor: ColorStyle.mainColor,
                  elevation: 0,
                  value: storehouse,
                  items: dropdownStorehouses,
                  onChanged: (value) {
                    setState(() {
                      storehouse = value;
                      item = value?.items?.first;
                      amount = item.amount;
                    });
                  }),
              Container(
                height: 10,
              ),
              Text(
                'Наименование: ',
                style: TextStyle(
                    color: ColorStyle.txtCategory, fontWeight: FontWeight.w500),
              ),
              DropdownButton(
                  underline: Container(),
                  isExpanded: true,
                  iconEnabledColor: ColorStyle.mainColor,
                  elevation: 0,
                  value: item,
                  itemHeight: null,
                  items: dropdownItems,
                  onChanged: (value) {
                    setState(() {
                      item = value;
                      amount = item.amount;
                    });
                  }),
              Container(
                height: 10,
              ),
              Row(
                children: [
                  Text(
                    'Количество: ',
                    style: TextStyle(
                        color: ColorStyle.txtCategory,
                        fontWeight: FontWeight.w500),
                  ),
                  if (item != null)
                    IconButton(
                      color: ColorStyle.wrong,
                      icon: Icon(
                        Icons.remove_circle_outline,
                      ),
                      onPressed:
                      (amount < 1) ? null : () => setState(() => amount--),
                    ),
                  Text(
                    item == null ? '' : '$amount ${item?.measureName}',
                  ),
                  if (item != null)
                    IconButton(
                      color: ColorStyle.mainColor,
                      icon: Icon(
                        Icons.add_circle_outline,
                      ),
                      onPressed: (amount >= (item?.amount ?? 0))
                          ? null
                          : () => setState(() => amount++),
                    )
                ],
              ),
              Container(
                height: 50,
              ),
              if (item != null)
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    FlatButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)),
                      textColor: ColorStyle.white,
                      color: ColorStyle.mainColor,
                      onPressed: takeFromStoreHouse,
                      child: Text(
                        'Взять',
                      ),
                    ),
                  ],
                )
            ],
          )),
    );
  }

  void takeFromStoreHouse() async {
    await bloc.putToInventory(
        storehouseId: storehouse.storeId, itemId: item.id, amount: amount);
    Navigator.of(context, rootNavigator: true).pop();
  }
}
