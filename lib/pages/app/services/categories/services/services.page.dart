import 'package:auto_size_text/auto_size_text.dart';
import 'package:car_sharing_services/blocs/services.bloc.dart';
import 'package:car_sharing_services/models/services.dart';
import 'package:car_sharing_services/page/widget/button.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'car_number/car_number.page.dart';


class ServicesPage extends StatefulWidget {
  final FullCategory category;

  const ServicesPage(this.category, {Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ServicesPageState();
}

class _ServicesPageState extends State<ServicesPage> {
  ServicesBloc bloc;

  static const WHEELS_PAGE_CATEGORY = 'Шиномонтаж';

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc = ServicesBloc.of(context);
  }

  @override
  Widget build(BuildContext context) {
    var list = widget.category?.services
        ?.map((service) => Padding(
        padding: EdgeInsets.fromLTRB(24, 24, 24, 0),
        child: _ServiceBuilder(
          service,
          onPressed: () => onWorkPressed(service),
        )))
        ?.toList();
    list ??= [];
    return Scaffold(
      appBar: AppBar(
        title: Text('Услуги'),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      backgroundColor: ColorStyle.backgroundColor,
      body: ListView(children: list),
    );
  }

  void onWorkPressed(Service work) {
    Navigator.of(context).push(CupertinoPageRoute(
      builder: (_) => CarNumberPage(work),
    ));
    /*var bloc = BlocProvider.of<NewWorkBloc>(context);
    bloc.selectedWork.add(work);
    Navigator.of(context, rootNavigator: true).push(CupertinoPageRoute(
      builder: (_) =>
      bloc.selectedCategory.value.name == WHEELS_PAGE_CATEGORY
        ? CarSearchPage()
        : CarBeforePage(),
    ));*/
  }
}

class _ServiceBuilder extends StatelessWidget {
  final Service service;
  final Function onPressed;

  const _ServiceBuilder(
      this.service, {
        Key key,
        this.onPressed,
      }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool active = true;
    if (service.availableFrom != null && service.availableTo != null) {
      DateTime now = DateTime.now();
      DateTime from =
      DateTime(now.year, now.month, now.day, service.availableFrom);
      DateTime to = DateTime(now.year, now.month, now.day, service.availableTo);
      if (service.availableFrom < service.availableTo) {
        active = now.isBefore(to) && from.isBefore(now);
      } else {
        active = now.isBefore(to) || from.isBefore(now);
      }
    }
    return Button(
      disabled: !active,
      disabledColor: ColorStyle.disabledColor,
      color: ColorStyle.white2,
      onPressed: onPressed,
      padding: EdgeInsets.fromLTRB(16, 5, 0, 5),
      shadow: [BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.05), blurRadius: 12)],
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: Stack(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(height: 5),
                AutoSizeText(
                  service.name,
                  maxLines: 1,
                  style: TextStyle(
                    color: ColorStyle.txt,
                    fontSize: 13,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                Container(height: 10),
                AutoSizeText(
                  'Норма - ${service.duration ~/ 60}:${(service.duration % 60).truncate()}',
                  maxLines: 1,
                  style: TextStyle(
                    color: ColorStyle.sideColor,
                    fontSize: 11,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ],
            ),
            Positioned(
              right: 0,
              top: 0,
              child: Container(
                padding: EdgeInsets.fromLTRB(8, 2, 16, 4),
                decoration: BoxDecoration(
                  color: (active ? ColorStyle.mainColor : ColorStyle.sideColor),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(
                        MediaQuery.of(context).size.longestSide),
                    bottomLeft: Radius.circular(
                        MediaQuery.of(context).size.longestSide),
                  ),
                ),
                child: AutoSizeText(
                  '${service.minCost}р',
                  maxLines: 1,
                  style: TextStyle(
                    fontSize: 13,
                    fontWeight: FontWeight.w500,
                    color: ColorStyle.white2,
                  ),
                ),
              ),
            ),
            service.availableFrom != null && service.availableTo != null
                ? Positioned(
              right: 16,
              bottom: 0,
              child: AutoSizeText(
                'Доступно с ${service.availableFrom} до ${service.availableTo}',
                maxLines: 1,
                style: TextStyle(
                  color: ColorStyle.sideColor,
                  fontSize: 9,
                  fontWeight: FontWeight.w500,
                ),
              ),
            )
                : Container(),
          ],
        ),
      ),
    );
  }
}
