import 'package:auto_size_text/auto_size_text.dart';
import 'package:car_sharing_services/models/store_item.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/material.dart';

class CarParamsSheet extends StatelessWidget {
  final String carNumber;
  final List<StoreItem> params;

  const CarParamsSheet({Key key, this.carNumber, this.params})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                alignment: Alignment.center,
                child: Text(
                  carNumber,
                  style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18),
                ),
              ),
              Divider(),
              Container(
                height: 10,
              ),
              if (params != null && params.isNotEmpty)
                Flexible(
                  child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: params.length,
                      itemBuilder: (_, index) => _StoreItemBuilder(
                          item: params[index], onPressed: null)),
                ),
              if (params?.isEmpty ?? true)
                Expanded(
                  child: Center(
                    child: Text('Инвентарь данного автомобиля пуст'),
                  ),
                ),
              Container(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  FlatButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    textColor: ColorStyle.white,
                    color: ColorStyle.mainColor,
                    onPressed: () =>
                        Navigator.of(context, rootNavigator: true).pop(),
                    child: Text(
                      'OK',
                    ),
                  ),
                ],
              )
            ],
          )),
    );
  }
}

class _StoreItemBuilder extends StatelessWidget {
  final StoreItem item;
  final Function(StoreItem) onPressed;

  const _StoreItemBuilder({
    Key key,
    @required this.item,
    @required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        width: MediaQuery.of(context).size.width - 30,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AutoSizeText(
                    item.name,
                    maxLines: 2,
                    style: TextStyle(
                      color: ColorStyle.txt,
                      fontSize: 13,
                      fontWeight: FontWeight.w500,
                    ),
                    textAlign: TextAlign.start,
                  ),
                  Container(
                    height: 5,
                  ),
                  Container(
                    child: AutoSizeText(
                      "${item.amount} ${item.measureName}",
                      maxLines: 1,
                      style: TextStyle(
                        color: ColorStyle.sideColor,
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: 10,
            ),
            if (onPressed != null)
              Column(
                children: [
                  FlatButton(
                    padding: EdgeInsets.zero,
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    color: ColorStyle.waiting.withOpacity(0.8),
                    onPressed: () => onPressed(item),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      'На склад',
                      style: TextStyle(fontSize: 12),
                      textAlign: TextAlign.center,
                    ),
                  )
                ],
              )
          ],
        ),
      ),
      padding: EdgeInsets.fromLTRB(16, 22, 16, 22),
      decoration: BoxDecoration(
        color: ColorStyle.white2,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.05), blurRadius: 12),
        ],
      ),
    );
  }
}
