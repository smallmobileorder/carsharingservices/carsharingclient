import 'dart:async';
import 'dart:io';

import 'package:car_sharing_services/application.dart';
import 'package:car_sharing_services/blocs/core.bloc.dart';
import 'package:car_sharing_services/blocs/works.bloc.dart';
import 'package:car_sharing_services/endpoints/cars.dart';
import 'package:car_sharing_services/models/car.dart';
import 'package:car_sharing_services/models/services.dart';
import 'package:car_sharing_services/models/store_item.dart';
import 'package:car_sharing_services/page/widget/custom_text_field.dart';
import 'package:car_sharing_services/page/widget/next_fab.dart';
import 'package:car_sharing_services/pages/app/app_main.dart';
import 'package:car_sharing_services/pages/app/works/current_work/current_work.page.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

import 'car_params.sheet.dart';

class CarNumberPage extends StatefulWidget {
  final Service service;

  CarNumberPage(this.service, {Key key}) : super(key: key);

  @override
  _CarNumberPageState createState() => _CarNumberPageState();
}

class _CarNumberPageState extends State<CarNumberPage> {
  final validateFormKey = GlobalKey<FormState>();
  WorksBloc worksBloc;

  final BehaviorSubject<bool> loading = BehaviorSubject();
  final String text = 'Укажите номер автомобиля';
  final TextEditingController carNumberController = TextEditingController();
  final BehaviorSubject<bool> blocked = BehaviorSubject.seeded(true);
  final BehaviorSubject<bool> visible = BehaviorSubject();

  bool carNumberLoading = false;
  bool carNumberNotFound = false;
  Car car;

  ///car image
  File image;

  /// need fpr rebuild widget when use retake func
  String timeKey;

  //TODO use new work bloc?
  //RegBloc bloc;

  bool enadled;
  StreamSubscription subscription;

  @override
  void initState() {
    super.initState();
    carNumberController.addListener(checkControllers);
    carNumberController.addListener(checkCarNumber);
    /* if (BlocProvider.of<NewWorkBloc>(context)?.numberPlate?.isNotEmpty ??
        false) {
      carNumberController.text =
          BlocProvider.of<NewWorkBloc>(context).numberPlate;
      enadled = false;
    } else {
      enadled = true;
    }*/

    subscription = KeyboardVisibility.onChange.listen((event) {
      if (!event) {
        visible.add(true);
        FocusScope.of(context).unfocus();
      } else {
        visible.add(false);
      }
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    worksBloc = WorksBloc.of(context);
  }

  @override
  void dispose() {
    subscription?.cancel();
    blocked?.close();
    visible?.close();
    carNumberController.dispose();
    loading?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    bool landscapeMode = false;
    if (height <= width) landscapeMode = true;

    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        backgroundColor: ColorStyle.backgroundColor,
        appBar: AppBar(
          centerTitle: true,
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          title: Text('Поиск автомобиля'),
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.only(left: 36, right: 36, bottom: 70),
          child: Form(
            key: validateFormKey,
            child: Column(
              children: <Widget>[
                Container(
                  height: height / 3,
                ),
                Container(
                  height: landscapeMode ? 5 : 10,
                ),
                Container(
                  child: Text(
                    text ?? '',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 18,
                        color: ColorStyle.txt,
                        fontWeight: FontWeight.w600),
                  ),
                ),
                Container(
                  height: landscapeMode ? 6 : 16,
                ),
                CustomTextField(
                  textInputAction: TextInputAction.done,
                  onFieldSubmitted: (v) async {
                    FocusScope.of(context).unfocus();
                    await nextFabPressed();
                  },
                  enabled: enadled,
                  hint: 'С065МК78',
                  label: 'Введите номер ремонтируемой машины',
                  controller: carNumberController,
                  keyboardType: TextInputType.multiline,
                  capitalization: TextCapitalization.characters,
                  validator: carNumberValidator,
                ),
                carNumberLoading == true
                    ? Padding(
                        padding: const EdgeInsets.only(top: 16.0),
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      )
                    : Container(),
                carNumberNotFound == true
                    ? Padding(
                        padding: const EdgeInsets.only(top: 16.0),
                        child: Center(
                          child: Text(
                            'Машина с таким номером не найдена',
                            style: TextStyle(color: ColorStyle.wrong),
                          ),
                        ),
                      )
                    : Container(),
              ],
            ),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: StreamBuilder<bool>(
          stream: visible.stream,
          builder: (context, snapshot) {
            return Visibility(
              visible: snapshot?.data ?? true,
              child: NextFab(
                text: 'Начать работу',
                blockedStream: blocked.stream,
                nextFabPressed: nextFabPressed,
              ),
            );
          },
        ),
      ),
    );
  }

  void checkControllers() {
    if ((carNumberController.text?.isEmpty ?? true)) {
      blocked.add(true);
    } else {
      blocked.add(false);
    }
  }

  String carNumberValidator(String text) {
    if (text?.trim()?.isNotEmpty ?? false) {
      var temp = text.trim();
      if (RegExp(r'[а-яА-Я0-9]').hasMatch(temp)) {
        return null;
      }
    }
    return 'Номер введен неверно';
  }

  void checkCarNumber() {
    if (carNumberController.text.length > 9) {
      var temp = carNumberController.selection.extentOffset;
      carNumberController.value = TextEditingValue(
        text: carNumberController.text.substring(0, 9),
        selection: TextSelection.fromPosition(
          TextPosition(offset: temp > 9 ? 9 : temp),
        ),
      );
    }
  }

  FutureOr nextFabPressed() async {
    if (validateFormKey.currentState.validate()) {
      logger.i('good data');

      // Check car
      setState(() => carNumberLoading = true);
      try {
        car = await worksBloc.api
            .request(getCarByNumber(carNumberController.text));
      } catch (e) {
        print("Failed to get car by number: $e");
      }
      if (car == null) {
        //blocked.add(true);
        setState(() {
          carNumberLoading = false;
          carNumberNotFound = true;
        });
        return;
      } else {
        setState(() {
          carNumberLoading = false;
          carNumberNotFound = false;
        });
      }

      await onAccept();
    }
  }

  Future onAccept() async {
    loading.add(true);
    List<StoreItem> params = [];
    await Future.wait([
      worksBloc.loadCarParams(car?.id ?? ''),
      worksBloc.startNewWork(
        widget.service.id,
        car?.id ?? '',
      ),
    ]).then((value) => params = value[0]);
    await watchCarParams(car?.number ?? 'С770ХС47', params);
    car = null;
    loading.add(false);
    Navigator.of(context).popUntil((r) {
      if (r.isFirst) {
        return true;
      }
      return r.settings.name?.contains('menu') ?? false;
    });
    await Future.delayed(Duration(milliseconds: 100));
    Provider.of<CoreBloc>(context, listen: false).barIndex.add(1);
    await Future.delayed(Duration(milliseconds: 100));
    marksKey.currentState.push(
      CupertinoPageRoute(
        builder: (_) => CurrentWorkPage(),
        settings: RouteSettings(name: 'current_work'),
      ),
    );
  }

  watchCarParams(String carNumber, List<StoreItem> params) async {
    await showBarModalBottomSheet(
        useRootNavigator: true,
        context: context,
        backgroundColor: Colors.transparent,
        builder: (context) => CarParamsSheet(
              carNumber: carNumber,
              params: params,
            ));
  }
}
