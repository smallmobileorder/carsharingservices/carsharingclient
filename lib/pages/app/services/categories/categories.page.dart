import 'package:auto_size_text/auto_size_text.dart';
import 'package:car_sharing_services/blocs/services.bloc.dart';
import 'package:car_sharing_services/models/services.dart';
import 'package:car_sharing_services/page/widget/button.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'services/services.page.dart';

class CategoriesPage extends StatefulWidget {
  final FullAgent agent;

  const CategoriesPage(this.agent, {Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _CategoriesPageState();
}

class _CategoriesPageState extends State<CategoriesPage> {
  ServicesBloc bloc;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc = ServicesBloc.of(context);
  }

  @override
  Widget build(BuildContext context) {
    var list = widget.agent?.categories
        ?.where((c) => c != null && c.isNotEmpty())
        ?.map((category) => Padding(
      padding: EdgeInsets.fromLTRB(24, 24, 24, 0),
      child: _CategoryWidget(
        category: category,
        onPressed: () => Navigator.of(context).push(
          CupertinoPageRoute(builder: (_) => ServicesPage(category)),
        ),
      ),
    ))
        ?.toList();
    list ??= [];
    return Scaffold(
      appBar: AppBar(
        title: Text('Категории'),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      backgroundColor: ColorStyle.backgroundColor,
      body: ListView(children: list),
    );
  }
}

class _CategoryWidget extends StatelessWidget {
  final FullCategory category;
  final Function onPressed;

  const _CategoryWidget({
    Key key,
    @required this.category,
    @required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Button(
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: Row(
          children: <Widget>[
            Container(
              /*child: Image(
                image: NetworkImage(category.iconUrl),
                width: 36,
                height: 36,
              ),*/
              width: 36,
              height: 36,
              child: Icon(
                Icons.directions_car,
                color: ColorStyle.mainColor,
              ),
            ),
            Container(width: 16, height: 0),
            AutoSizeText(category.name, maxLines: 1),
          ],
        ),
      ),
      padding: EdgeInsets.fromLTRB(16, 12, 16, 12),
      color: ColorStyle.white2,
      shadow: [BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.05), blurRadius: 12)],
      textStyle: TextStyle(
        color: ColorStyle.txt,
        fontSize: 13,
        fontWeight: FontWeight.w500,
      ),
      onPressed: onPressed,
    );
  }
}
