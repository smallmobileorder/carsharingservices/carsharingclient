import 'package:auto_size_text/auto_size_text.dart';
import 'package:car_sharing_services/application.dart';
import 'package:car_sharing_services/blocs/services.bloc.dart';
import 'package:car_sharing_services/models/services.dart';
import 'package:car_sharing_services/page/widget/button.dart';
import 'package:car_sharing_services/page/widget/smart_pull_refresh.dart';
import 'package:car_sharing_services/pages/app/app_main.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:rxdart/rxdart.dart';

import 'categories/categories.page.dart';

class AgentsPage extends StatefulWidget {
  final BehaviorSubject barVisibilityController;

  const AgentsPage({Key key, @required this.barVisibilityController})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _AgentsPageState();
}

class _AgentsPageState extends State<AgentsPage> with RouteAware {
  RefreshController controller = RefreshController(initialRefresh: true);
  ServicesBloc bloc;

  @override
  void didChangeDependencies() {
    listRouteObserver.subscribe(this, ModalRoute.of(context));
    bloc = ServicesBloc.of(context);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    controller.dispose();
    listRouteObserver.unsubscribe(this);
    super.dispose();
  }

  @override
  void didPopNext() {
    super.didPopNext();
    widget.barVisibilityController.add(true);
  }

  @override
  void didPushNext() {
    super.didPushNext();
    widget.barVisibilityController.add(false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Начать новую работу'),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      backgroundColor: ColorStyle.backgroundColor,
      body: StreamBuilder<List<FullAgent>>(
        stream: bloc.fullAgents,
        builder: (context, snapshot) {
          var list = snapshot.data
              ?.where((a) => a.isNotEmpty())
              ?.map((a) => Padding(
                  padding: EdgeInsets.fromLTRB(24, 24, 24, 0),
                  child: _AgentBuilder(
                    agent: a,
                    onPressed: () =>
                        Navigator.of(context, rootNavigator: false).push(
                      CupertinoPageRoute(builder: (_) => CategoriesPage(a)),
                    ),
                  )))
              ?.cast<Widget>()
              ?.toList();
          list?.add(Container(height: 70));
          list ??= [];
          return SmartPullRefresh(
            controller: controller,
            child: ListView(children: list),
            onRefresh: () async {
              try {
                await bloc.updateAgents();
              } catch (e) {
                logger.e('Error on fetching services', e);
              }
              controller.refreshCompleted();
            },
          );
        },
      ),
    );
  }
}

class _AgentBuilder extends StatelessWidget {
  final FullAgent agent;
  final Function onPressed;

  const _AgentBuilder({
    Key key,
    @required this.agent,
    @required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Button(
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: AutoSizeText(agent.name, maxLines: 1),
      ),
      padding: EdgeInsets.fromLTRB(16, 22, 16, 22),
      color: ColorStyle.white2,
      borderRadius: 8,
      shadow: [
        BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.05), blurRadius: 12),
      ],
      textStyle: TextStyle(
        color: ColorStyle.txt,
        fontSize: 13,
        fontWeight: FontWeight.w500,
      ),
      onPressed: onPressed,
    );
  }
}
