import 'dart:async';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:car_sharing_services/blocs/works.bloc.dart';
import 'package:car_sharing_services/models/work_adapter.dart';
import 'package:car_sharing_services/page/widget/button.dart';
import 'package:car_sharing_services/page/widget/loading_image_widget.dart';
import 'package:car_sharing_services/page/widget/smart_pull_refresh.dart';
import 'package:car_sharing_services/pages/app/app_main.dart';
import 'package:car_sharing_services/pages/app/works/current_work/current_work.page.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:rxdart/rxdart.dart';

class WorksPage extends StatefulWidget {
  final BehaviorSubject barVisibilityController;

  const WorksPage({Key key, this.barVisibilityController}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MyWorkPageState();
}

class _MyWorkPageState extends State<WorksPage> with RouteAware {
  RefreshController mainController = RefreshController(initialRefresh: true);
  RefreshController currentController = RefreshController(initialRefresh: true);
  RefreshController historyController = RefreshController(initialRefresh: true);
  WorksBloc worksBloc;
  StreamSubscription sub;

  BehaviorSubject<int> tab = BehaviorSubject();
  int currentTab;

  PageController pageController;

  @override
  void initState() {
    super.initState();
    currentTab = 0;
    tab.add(currentTab);
    pageController = PageController(initialPage: currentTab);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    marksRouteObserver.subscribe(this, ModalRoute.of(context));
    worksBloc = WorksBloc.of(context);
  }

  @override
  void dispose() {
    sub?.cancel();
    tab?.close();
    currentController?.dispose();
    historyController?.dispose();
    mainController?.dispose();
    marksRouteObserver.unsubscribe(this);
    super.dispose();
  }

  @override
  void didPopNext() {
    super.didPopNext();
    widget.barVisibilityController.add(true);
  }

  @override
  void didPushNext() {
    super.didPushNext();
    widget.barVisibilityController.add(false);
  }

//  @override
//  void initState() {
//    worksBloc = BlocProvider.of<WorksBloc>(context);
//    sub = worksBloc?.updateWorks?.listen((update) {
//      if (update) {
//        controller.requestRefresh();
//      }
//    });
//    super.initState();
//  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Список работ'),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      backgroundColor: ColorStyle.backgroundColor,
      body: StreamBuilder<List<WorkAdapter>>(
        initialData: [],
        stream: worksBloc.historyWorks,
        builder: (_, snapshotHistory) => StreamBuilder<List<WorkAdapter>>(
          stream: worksBloc.currentWorks,
          initialData: [],
          builder: (_, snapshotCurrent) {
            Widget content;
            // if no widgets then show stab
            if ((snapshotCurrent.data == null ||
                    snapshotCurrent.data.isEmpty) &&
                (snapshotHistory.data == null ||
                    snapshotHistory.data.isEmpty)) {
              double pad = MediaQuery.of(context).size.width / 8;
              double top = MediaQuery.of(context).size.height / 2 + 27 - 60;
              bool vertical = MediaQuery.of(context).size.height >
                  MediaQuery.of(context).size.width;
              content = Stack(
                children: <Widget>[
                  Center(
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(pad, 0, pad, 60),
                      child: AutoSizeText(
                        'Пока у вас нет текущих или выполненных работ',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                          color: ColorStyle.txt,
                        ),
                        maxLines: 2,
                      ),
                    ),
                  ),
                  vertical
                      ? Positioned(
                          bottom: 60,
                          top: top,
                          left: 0,
                          right: 0,
                          child: Center(
                            child: SvgPicture.asset(
                              'assets/vectors/no_works.svg',
                            ),
                          ),
                        )
                      : Container(),
                ],
              );
            } else {
              // sort work groups
              //List<CarWorksGroup> done = [];
//              List<CurrentWork> inProgress = [];

              // slice groups in two groups
//            snapshot.data.forEach((g) =>
//            g?.finishTimeInMs == null ? inProgress.add(g) : done.add(g));
//            done.sort((a, b) => a.startTimeInMs.compareTo(b.startTimeInMs));
//            inProgress
//                .sort((a, b) => a.startTimeInMs.compareTo(b.startTimeInMs));

              // create widgets for them
//            List<Widget> doneWidgets = snapshot.data
//                .map((g) => Padding(
//                      padding: const EdgeInsets.fromLTRB(24, 24, 24, 0),
//                      child: _GroupTileWidget(group: g),
//                    ))
//                .toList();

//              List<Widget> inProgressWidgets = snapshot.data
//                  .map((g) => Padding(
//                        padding: const EdgeInsets.fromLTRB(24, 24, 24, 0),
//                        child: _WorkTileWidget(work: g),
//                      ))
//                  .toList();
//
//              content = ListView(
//                physics: BouncingScrollPhysics(),
//                children: <Widget>[
//                  inProgress.isNotEmpty
//                      ? Padding(
//                          padding: const EdgeInsets.only(left: 24),
//                          child: Text(
//                            'Выполняется',
//                            style: TextStyle(
//                              fontSize: 18,
//                              fontWeight: FontWeight.bold,
//                              color: ColorStyle.waiting,
//                            ),
//                          ),
//                        )
//                      : Container(),
//                  ...inProgressWidgets,
//                  Container(height: 100),
//                ],
//              );
              content = StreamBuilder<int>(
                  stream: tab,
                  builder: (context, snapshot) {
                    return body(snapshotCurrent, snapshotHistory, snapshot);
                  });
            }
            return Column(
              children: <Widget>[
                StreamBuilder<Object>(
                    stream: tab,
                    builder: (context, snapshot) {
                      if ((snapshotHistory?.data?.length ?? 0) > 0 ||
                          (snapshotCurrent?.data?.length ?? 0) > 0)
                        return Row(
                          children: <Widget>[
                            GestureDetector(
                              onTap: () => changeTab(0),
                              child: Container(
                                margin: EdgeInsets.fromLTRB(15, 0, 0, 0),
                                padding: EdgeInsets.all(5),
                                child: Text(
                                  'Текущие',
                                  textAlign: TextAlign.center,
                                ),
                                decoration: snapshot.data != 0
                                    ? null
                                    : BoxDecoration(
                                        borderRadius: BorderRadius.circular(15),
                                        color: Colors.white,
                                        border:
                                            Border.all(color: Colors.green)),
                                height: 30,
                              ),
                            ),
                            GestureDetector(
                              onTap: () => changeTab(1),
                              child: Container(
                                margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                padding: EdgeInsets.all(5),
                                child: Text(
                                  'Выполненные',
                                  textAlign: TextAlign.center,
                                ),
                                decoration: snapshot.data != 1
                                    ? null
                                    : BoxDecoration(
                                        borderRadius: BorderRadius.circular(15),
                                        color: Colors.white,
                                        border:
                                            Border.all(color: Colors.green)),
                                height: 30,
                              ),
                            ),
                          ],
                        );
                      return Container();
                    }),
                Expanded(
                  child: SmartPullRefresh(
                    controller: mainController,
                    child: content,
                    onRefresh: () async {
                      print('a');
                      worksBloc.getAllCurrentWorks();
                      worksBloc.getAllHistoryWorks();
                      mainController.refreshCompleted();
                    },
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  Widget body(
      AsyncSnapshot<List<WorkAdapter>> snapshotCurrent,
      AsyncSnapshot<List<WorkAdapter>> snapshotHistory,
      AsyncSnapshot<int> tabData) {
    double pad = MediaQuery.of(context).size.width / 8;
    double top = MediaQuery.of(context).size.height / 2 + 27 - 60;
    bool vertical =
        MediaQuery.of(context).size.height > MediaQuery.of(context).size.width;
    var content = (text) => Stack(
          children: <Widget>[
            Center(
              child: Padding(
                padding: EdgeInsets.fromLTRB(pad, 0, pad, 60),
                child: AutoSizeText(
                  'Пока у вас нет $text работ',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: ColorStyle.txt,
                  ),
                  maxLines: 2,
                ),
              ),
            ),
            vertical
                ? Positioned(
                    bottom: 60,
                    top: top,
                    left: 0,
                    right: 0,
                    child: Center(
                      child: SvgPicture.asset(
                        'assets/vectors/no_works.svg',
                      ),
                    ),
                  )
                : Container(),
          ],
        );
    return PageView(
      allowImplicitScrolling: true,
      children: <Widget>[
        SmartPullRefresh(
          controller: currentController,
          onRefresh: () async {
            print('b');
            worksBloc.getAllCurrentWorks();
            worksBloc.getAllHistoryWorks();
            currentController.refreshCompleted();
          },
          child: (snapshotCurrent?.data?.length ?? 0) > 0
              ? ListView.builder(
                  padding: EdgeInsets.only(left: 16, right: 16, bottom: 70),
                  itemCount: snapshotCurrent.data.length,
                  itemBuilder: (_, index) => Container(
                      margin: EdgeInsets.only(top: 8, bottom: 8),
                      child: _WorkTileWidget(
                        work: snapshotCurrent.data[index],
                        onFinished: () {
                          currentController.requestRefresh();
                          historyController.requestRefresh();
                        },
                      )),
                )
              : content('текущих'),
        ),
        SmartPullRefresh(
          controller: historyController,
          onRefresh: () async {
            print('c');
            worksBloc.getAllCurrentWorks();
            worksBloc.getAllHistoryWorks();
            historyController.refreshCompleted();
          },
          child: (snapshotHistory?.data?.length ?? 0) > 0
              ? ListView.builder(
            padding: EdgeInsets.only(left: 16, right: 16, bottom: 70),
            itemCount: snapshotHistory.data.length,
            itemBuilder: (_, index) =>
                Container(
                    margin: EdgeInsets.only(top: 8, bottom: 8),
                    child: _WorkTileWidget(
                      work: snapshotHistory.data[index],
                      onFinished: () {
                        currentController.requestRefresh();
                        historyController.requestRefresh();
                      },
                    )),
          )
              : content('выполненных'),
        )
      ],
      controller: pageController,
      onPageChanged: (page) => changeTab(page),
    );
  }

  changeTab(int newTab) {
    if (pageController == null && !pageController.hasClients) {
      return;
    }
    if ((newTab == 0 || newTab == 1) && currentTab != newTab) {
      currentTab = newTab;
      pageController.animateToPage(currentTab,
          duration: Duration(milliseconds: 400), curve: Curves.easeInOut);
      tab.add(currentTab);
    }
  }
}

class _WorkTileWidget extends StatelessWidget {
  final WorkAdapter work;
  final Function onFinished;

  const _WorkTileWidget({Key key, @required this.work, this.onFinished})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // calculate some params
    String cost = work?.minCost?.toString() ?? '';
    bool done = work?.finishTime != null;
    Duration time = work?.currentTime() ?? Duration.zero;
    int minutes = time.inMilliseconds ~/ 60000;
    String timeSpent = '${minutes ~/ 60} ч : ${minutes % 60} мин';

    // get all photos
    var photoLink = work?.photoBefore ?? '';

    Widget image = ClipRRect(
      child: (photoLink.isEmpty || photoLink == null)
          ? Container(
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.transparent,
        ),
        width: 48,
        height: 48,
      )
          : Container(
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.transparent,
        ),
        width: 48,
        height: 48,
        child: FittedBox(
          fit: BoxFit.cover,
          child: LoadingImageWidget(
            path: photoLink,
          ),
        ),
      ),
      borderRadius: BorderRadius.circular(24),
    );

    // Info block in widget
    Widget info = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        AutoSizeText(
          work?.carNumber ?? '',
          style: TextStyle(
            fontSize: 13,
            fontWeight: FontWeight.w500,
            color: done ? ColorStyle.mainColor : ColorStyle.waiting,
          ),
          maxLines: 1,
        ),
        AutoSizeText.rich(
          TextSpan(
            children: [
              TextSpan(
                text: 'Сервис - ',
                style: TextStyle(
                  fontSize: 11,
                  fontWeight: FontWeight.normal,
                  color: ColorStyle.txt,
                ),
              ),
              TextSpan(
                text: work?.name ?? '',
                style: TextStyle(
                  fontSize: 11,
                  fontWeight: FontWeight.bold,
                  color: ColorStyle.txt,
                ),
              ),
            ],
          ),
          maxLines: 1,
        ),
        AutoSizeText(
          timeSpent,
          maxLines: 1,
          style: TextStyle(
            fontSize: 11,
            fontWeight: FontWeight.normal,
            color: done ? ColorStyle.mainColor : ColorStyle.waiting,
          ),
        ),
      ],
    );

    return Button(
      color: ColorStyle.white2,
      shadow: [BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.05), blurRadius: 12)],
      padding: EdgeInsets.fromLTRB(16, 12, 0, 8),
      borderColor: done ? ColorStyle.mainColor : ColorStyle.waiting,
      borderWidth: 1,
      onPressed: () {
        Provider.of<WorksBloc>(context, listen: false).selectedWork.add(work);
        Navigator.of(context, rootNavigator: false)
            .push(
          CupertinoPageRoute(
            builder: (_) => CurrentWorkPage(),
            settings: RouteSettings(name: 'current_work'),
          ),
        )
            .then((res) {
          if (res is bool && res == true && onFinished != null) {
            onFinished();
          }
        });
      },
      child: Container(
        width: double.infinity,
        child: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 4, bottom: 8),
              child: image,
            ),
            Positioned(
              top: 0,
              bottom: 0,
              left: 48.0 + 16.0, //size of image and padding
              child: Padding(
                padding: const EdgeInsets.only(top: 4, bottom: 8),
                child: info,
              ),
            ),
            Positioned(
              right: 0,
              top: 0,
              child: Container(
                padding: EdgeInsets.fromLTRB(8, 2, 16, 4),
                decoration: BoxDecoration(
                  color: done ? ColorStyle.mainColor : ColorStyle.waiting,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(
                        MediaQuery.of(context).size.longestSide),
                    bottomLeft: Radius.circular(
                        MediaQuery.of(context).size.longestSide),
                  ),
                ),
                child: AutoSizeText(
                  '$costр',
                  maxLines: 1,
                  style: TextStyle(
                    fontSize: 13,
                    fontWeight: FontWeight.w500,
                    color: ColorStyle.white2,
                  ),
                ),
              ),
            ),
            Positioned(
              right: 8,
              bottom: 0,
              child: Icon(
                done ? CupertinoIcons.check_mark_circled : CupertinoIcons.time,
                color: done ? ColorStyle.mainColor : ColorStyle.waiting,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
