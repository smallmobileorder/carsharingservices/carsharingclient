import 'dart:async';
import 'dart:io';
import 'package:car_sharing_services/blocs/works.bloc.dart';
import 'package:car_sharing_services/pages/photo/questions.page.dart';
import 'package:car_sharing_services/util/photo_maker.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

import 'package:car_sharing_services/pages/photo/photo_review.page.dart';

class CarBeforePage extends StatefulWidget {
  CarBeforePage({Key key}) : super(key: key);

  @override
  _CarBeforePageState createState() => _CarBeforePageState();
}

class _CarBeforePageState extends State<CarBeforePage> {
  File image;
  final BehaviorSubject<bool> imageLoaded = BehaviorSubject();
  final String title = 'Фото до начала работ';
  final String text = 'Сделайте фото автомобиля до начала работ';
  final String imageStubPath = 'assets/vectors/car_before.svg';
  final List<String> questionsForReview = [
    'То, что необходимо исправить, отчётливо видно? ',
  ];

  /// need fpr rebuild widget when use retake func
  String timeKey;

  @override
  void initState() {
    super.initState();
    imageLoaded.add(false);
  }

  @override
  void dispose() {
    imageLoaded?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
        stream: imageLoaded,
        initialData: false,
        builder: (context, snapshot) {
          if (snapshot?.data ?? false)
            return QuestionsPage(
              key: Key(timeKey ?? 'default'),
              title: title,
              image: image,
              questions: questionsForReview,
              accept: onAccept,
              back: onBack,
              retake: onRetake,
            );
          //TODO if u want to block back button and show some dialog use field at comments
          ///CustomWillPopWidget(
          ///navigatorFunction: ,
          ///dialogContent: ,
          ///child:
          return PhotoReviewPage(
            title: title,
            text: text,
            imageStubPath: imageStubPath,
            nextFabPressed: nextFabPressed,
          );
        });
  }

  FutureOr nextFabPressed() async {
    image = await PhotoMaker.makePhoto(context);
    if (image != null) {
      timeKey = DateTime.now().toIso8601String();
      imageLoaded.add(true);
    }
  }

  onAccept() async {
    imageLoaded.add(false);
    Provider.of<WorksBloc>(context, listen: false)
        .updatePhotoBefore(image.readAsBytesSync());
    Navigator.of(context, rootNavigator: false).popUntil(
      (r) => r.settings.name == 'current_work',
    );
  }

  onBack() {
    imageLoaded.add(false);
    image = null;
  }

  onRetake() async {
    image = await PhotoMaker.makePhoto(context);
    if (image != null) {
      timeKey = DateTime.now().toIso8601String();
      imageLoaded.add(true);
    }
  }
}
