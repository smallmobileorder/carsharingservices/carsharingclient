import 'package:car_sharing_services/blocs/storehouse.bloc.dart';
import 'package:car_sharing_services/blocs/works.bloc.dart';
import 'package:car_sharing_services/models/store_item.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/material.dart';

class TakeFromWorkerInventory extends StatefulWidget {
  final StorehouseBloc storeBloc;
  final WorksBloc worksBloc;

  const TakeFromWorkerInventory({Key key, this.worksBloc, this.storeBloc})
      : super(key: key);

  @override
  createState() => _TakeFromWorkerInventoryState();
}

class _TakeFromWorkerInventoryState extends State<TakeFromWorkerInventory> {
  int amount;

  // Storehouse storehouse;
  StoreItem item;
  StorehouseBloc storeBloc;
  WorksBloc worksBloc;

  // Map<Storehouse, List<StoreItem>> storehousesMap;

  @override
  void initState() {
    super.initState();
    storeBloc = widget.storeBloc;
    worksBloc = widget.worksBloc;

    // storehousesMap = {
    //   Storehouse.stab(): [
    //     StoreItem.stab(),
    //     StoreItem.stab(),
    //     StoreItem.stab(),
    //     StoreItem.stab(),
    //     StoreItem.stab(),
    //     StoreItem.stab(),
    //     StoreItem.stab(),
    //     StoreItem.stab(),
    //     StoreItem.stab(),
    //     StoreItem.stab(),
    //     StoreItem.stab(),
    //     StoreItem.stab(),
    //     StoreItem.stab(),
    //     StoreItem.stab(),
    //     StoreItem.stab(),
    //     StoreItem.stab(),
    //     StoreItem.stab(),
    //     StoreItem.stab(),
    //     StoreItem.stab(),
    //     StoreItem.stab(),
    //     StoreItem.stab(),
    //     StoreItem.stab(),
    //     StoreItem.stab(),
    //   ],
    //   Storehouse.stab2(): [
    //     StoreItem.stab(),
    //     StoreItem.stab2(),
    //     StoreItem.stab(),
    //   ],
    //   Storehouse.stab(): [
    //     StoreItem.stab(),
    //     StoreItem.stab(),
    //     StoreItem.stab(),
    //     StoreItem.stab(),
    //     StoreItem.stab(),
    //   ],
    //   Storehouse.stab2(): [StoreItem.stab()],
    // }; //bloc.storehouses.value.keys.toList();
    // storehouse = storehousesMap.keys.first;
    item = storeBloc.inventory.value.first;
    amount = item.amount;
  }

  @override
  Widget build(BuildContext context) {
    var dropdownItems = storeBloc?.inventory?.value
            ?.map(
              (e) => DropdownMenuItem(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      e.name,
                    ),
                    Container(
                      height: 5,
                    ),
                    Text(
                      '${e.amount} ${e.measureName}',
                      style: TextStyle(color: ColorStyle.txt, fontSize: 13),
                    ),
                    Divider()
                  ],
                ),
                value: e,
              ),
            )
            ?.toList() ??
        [];

    return SafeArea(
      child: SingleChildScrollView(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                alignment: Alignment.center,
                child: Text(
                  'Взять из инвентаря',
                  style: TextStyle(fontWeight: FontWeight.w500),
                ),
              ),
              Divider(),
              Container(
                height: 10,
              ),
              Text(
                'Наименование: ',
                style: TextStyle(
                    color: ColorStyle.txtCategory, fontWeight: FontWeight.w500),
              ),
              DropdownButton(
                  underline: Container(),
                  isExpanded: true,
                  iconEnabledColor: ColorStyle.mainColor,
                  elevation: 0,
                  value: item,
                  itemHeight: null,
                  items: dropdownItems,
                  onChanged: (value) {
                    setState(() {
                      item = value;
                      amount = item.amount;
                    });
                  }),
              Container(
                height: 10,
              ),
              Row(
                children: [
                  Text(
                    'Количество: ',
                    style: TextStyle(
                        color: ColorStyle.txtCategory,
                        fontWeight: FontWeight.w500),
                  ),
                  IconButton(
                    color: ColorStyle.wrong,
                    icon: Icon(
                      Icons.remove_circle_outline,
                    ),
                    onPressed:
                        (amount < 1) ? null : () => setState(() => amount--),
                  ),
                  Text(
                    '$amount ${item?.measureName}',
                  ),
                  IconButton(
                    color: ColorStyle.mainColor,
                    icon: Icon(
                      Icons.add_circle_outline,
                    ),
                    onPressed: (amount >= item.amount)
                        ? null
                        : () => setState(() => amount++),
                  )
                ],
              ),
              Container(
                height: 50,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  FlatButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    textColor: ColorStyle.white,
                    color: ColorStyle.mainColor,
                    onPressed: amount > 0 ? takeFromStoreHouse : null,
                    child: Text(
                      'Взять',
                    ),
                  ),
                ],
              )
            ],
          )),
    );
  }

  void takeFromStoreHouse() async {
    var carId = worksBloc.selectedWork.value.carId;
    await worksBloc.addCarParams(carId, item.id, amount);
    Navigator.of(context, rootNavigator: true).pop();
  }
}
