import 'package:car_sharing_services/blocs/storehouse.bloc.dart';
import 'package:car_sharing_services/blocs/works.bloc.dart';
import 'package:car_sharing_services/models/store_item.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/material.dart';

class ReturnToWorkerInventory extends StatefulWidget {
  final StoreItem item;
  final StorehouseBloc storeBloc;
  final WorksBloc worksBloc;

  const ReturnToWorkerInventory(
      {Key key, this.item, this.storeBloc, this.worksBloc})
      : super(key: key);

  @override
  createState() => _ReturnToWorkerInventoryState();
}

class _ReturnToWorkerInventoryState extends State<ReturnToWorkerInventory> {
  int amount;
  int returnedAmount;

  // Storehouse returnedStorehouse;
  StorehouseBloc storeBloc;
  WorksBloc worksBloc;

  @override
  void initState() {
    super.initState();
    storeBloc = widget.storeBloc;
    worksBloc = widget.worksBloc;
    amount = widget?.item?.amount ?? 0;
    returnedAmount = amount;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                alignment: Alignment.center,
                child: Text(
                  'Вернуть инвентарь на склад',
                  style: TextStyle(fontWeight: FontWeight.w500),
                ),
              ),
              Divider(),
              Container(
                height: 10,
              ),
              Text(
                'Наименование: ',
                style: TextStyle(
                    color: ColorStyle.txtCategory, fontWeight: FontWeight.w500),
              ),
              Text(
                '${widget?.item?.name}',
                // style: TextStyle(color: ColorStyle.sideColor),
                maxLines: 2,
              ),
              Container(
                height: 10,
              ),
              Container(
                height: 10,
              ),
              StatefulBuilder(
                  builder: (BuildContext context, StateSetter setState) {
                return Row(
                  children: [
                    Text(
                      'Количество: ',
                      style: TextStyle(
                          color: ColorStyle.txtCategory,
                          fontWeight: FontWeight.w500),
                    ),
                    IconButton(
                      color: ColorStyle.wrong,
                      icon: Icon(
                        Icons.remove_circle_outline,
                      ),
                      onPressed: (returnedAmount < 1)
                          ? null
                          : () => setState(() => returnedAmount--),
                    ),
                    Text(
                      '$returnedAmount ${widget?.item?.measureName}',
                    ),
                    IconButton(
                      color: ColorStyle.mainColor,
                      icon: Icon(
                        Icons.add_circle_outline,
                      ),
                      onPressed: (returnedAmount >= amount)
                          ? null
                          : () => setState(() => returnedAmount++),
                    )
                  ],
                );
              }),
              Container(
                height: 50,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  FlatButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    textColor: ColorStyle.white,
                    color: ColorStyle.mainColor,
                    onPressed: returnedAmount > 0 ? returnToStoreHouse : null,
                    child: Text(
                      'Вернуть',
                    ),
                  ),
                ],
              )
            ],
          )),
    );
  }

  void returnToStoreHouse() async {
    var carId = worksBloc.selectedWork.value.carId;
    await worksBloc.removeCarParams(carId, widget.item.id, returnedAmount);
    Navigator.of(context, rootNavigator: true).pop();
  }
}
