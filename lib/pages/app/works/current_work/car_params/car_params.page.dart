import 'package:auto_size_text/auto_size_text.dart';
import 'package:car_sharing_services/application.dart';
import 'package:car_sharing_services/blocs/storehouse.bloc.dart';
import 'package:car_sharing_services/blocs/works.bloc.dart';
import 'package:car_sharing_services/models/store_item.dart';
import 'package:car_sharing_services/page/widget/smart_pull_refresh.dart';
import 'package:car_sharing_services/pages/app/works/current_work/car_params/return_to_worker_inventory.sheet.dart';
import 'package:car_sharing_services/pages/app/works/current_work/car_params/take_from_worker_inventory.sheet.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:rxdart/rxdart.dart';

class CarParamsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CarParamsPageState();
}

class _CarParamsPageState extends State<CarParamsPage> {
  RefreshController controller = RefreshController(initialRefresh: true);
  StorehouseBloc _storeBloc;
  WorksBloc _worksBloc;
  BehaviorSubject<List<StoreItem>> carParams = BehaviorSubject.seeded([]);

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    _storeBloc = StorehouseBloc.of(context);
    _worksBloc = WorksBloc.of(context);
    _worksBloc
        .loadCarParams(_worksBloc.selectedWork.value.carId)
        .then((value) => carParams.add(value));
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    controller.dispose();
    carParams.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: Padding(
        padding: EdgeInsets.only(
            bottom: 0 + MediaQuery.of(context).viewPadding.bottom),
        child: SizedBox(
          height: 35,
          width: MediaQuery.of(context).size.width / 3,
          child: FloatingActionButton(
              // isExtended: true,
              mini: true,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8),
                      topRight: Radius.circular(8))),
              onPressed: addItemFromWorkInventory,
              backgroundColor: ColorStyle.mainColor.withOpacity(0.8),
              child: Text(
                'Взять из инвентаря',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 12),
              )),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      appBar: AppBar(
        title: Text(_worksBloc.selectedWork.value.carNumber),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        // automaticallyImplyLeading: false,
        // leading: IconButton(
        //     icon: Icon(
        //       Icons.close,
        //       // color: ColorStyle.mainColor,
        //     ),
        //     onPressed: () => Navigator.of(context).pop()),
        // actions: [
        //   IconButton(
        //       icon: Icon(
        //         Icons.done,
        //         color: ColorStyle.mainColor,
        //       ),
        //       onPressed: () => Navigator.of(context).pop())
        // ],
      ),
      backgroundColor: ColorStyle.backgroundColor,
      body: StreamBuilder<List<StoreItem>>(
        stream: carParams,
        builder: (_, snapshot) {
          var temp = snapshot?.data ?? [];
          var widgets = temp
              .map((element) => Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: _StoreItemBuilder(
                      item: element,
                      onPressed: sendItemToInventory,
                    ),
                  ))
              .cast<Widget>()
              .toList();
          widgets.add(Container(
            height: 70,
          ));
          return SmartPullRefresh(
            controller: controller,
            child: ListView(children: widgets),
            onRefresh: () async {
              try {
                await _worksBloc
                    .loadCarParams(_worksBloc.selectedWork.value.carId)
                    .then((value) => carParams.add(value));
                await _storeBloc.fetchInventory();
              } catch (e) {
                logger.e('Error on fetching services', e);
              }
              controller.refreshCompleted();
            },
          );
        },
      ),
      // body: StreamBuilder<List<FullAgent>>(
      //   stream: bloc.fullAgents,
      //   builder: (context, snapshot) {
      //     var list = snapshot.data
      //         ?.where((a) => a.isNotEmpty())
      //         ?.map((a) => Padding(
      //         padding: EdgeInsets.fromLTRB(24, 24, 24, 0),
      //         child: _AgentBuilder(
      //           agent: a,
      //           onPressed: () =>
      //               Navigator.of(context, rootNavigator: false).push(
      //                 CupertinoPageRoute(builder: (_) => CategoriesPage(a)),
      //               ),
      //         )))
      //         ?.cast<Widget>()
      //         ?.toList();
      //     list?.add(Container(height: 70));
      //     list ??= [];
      //     return SmartPullRefresh(
      //       controller: controller,
      //       child: ListView(children: list),
      //       onRefresh: () async {
      //         try {
      //           await bloc.updateAgents();
      //         } catch (e) {
      //           logger.e('Error on fetching services', e);
      //         }
      //         controller.refreshCompleted();
      //       },
      //     );
      //   },
      // ),
    );
  }

  void addItemFromWorkInventory() {
    showBarModalBottomSheet(
        useRootNavigator: true,
        context: context,
        backgroundColor: Colors.transparent,
        builder: (context) => TakeFromWorkerInventory(
              storeBloc: _storeBloc,
              worksBloc: _worksBloc,
            )).whenComplete(() => controller.requestRefresh());
  }

  sendItemToInventory(StoreItem p1) {
    showBarModalBottomSheet(
        useRootNavigator: true,
        context: context,
        backgroundColor: Colors.transparent,
        builder: (context) =>
            ReturnToWorkerInventory(
              item: p1,
              storeBloc: _storeBloc,
              worksBloc: _worksBloc,
            )).whenComplete(() => controller.requestRefresh());
  }
}

class _StoreItemBuilder extends StatelessWidget {
  final StoreItem item;
  final Function(StoreItem) onPressed;

  const _StoreItemBuilder({
    Key key,
    @required this.item,
    @required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        width: MediaQuery.of(context).size.width - 30,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AutoSizeText(
                    item.name,
                    maxLines: 2,
                    style: TextStyle(
                      color: ColorStyle.txt,
                      fontSize: 13,
                      fontWeight: FontWeight.w500,
                    ),
                    textAlign: TextAlign.start,
                  ),
                  Container(
                    height: 5,
                  ),
                  Container(
                    child: AutoSizeText(
                      "${item.amount} ${item.measureName}",
                      maxLines: 1,
                      style: TextStyle(
                        color: ColorStyle.sideColor,
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: 10,
            ),
            Column(
              children: [
                FlatButton(
                  padding: EdgeInsets.zero,
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  color: ColorStyle.waiting.withOpacity(0.8),
                  onPressed: () => onPressed(item),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)),
                  child: Text(
                    'В инвентарь',
                    style: TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  ),
                )
              ],
            )
          ],
        ),
      ),
      padding: EdgeInsets.fromLTRB(16, 22, 16, 22),
      decoration: BoxDecoration(
        color: ColorStyle.white2,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.05), blurRadius: 12),
        ],
      ),
    );
  }
}
