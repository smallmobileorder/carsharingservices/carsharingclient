import 'package:car_sharing_services/page/widget/custom_cupertino_alert_dialog.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';

class FinishWorkDialog extends StatelessWidget {
  final Duration time;
  final Function changeCarParams;

  const FinishWorkDialog({Key key, this.time, this.changeCarParams})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    int minutes = time.inMilliseconds ~/ 60000;
    String timeSpent = '${minutes ~/ 60} ч : ${minutes % 60} мин';
    return CustomCupertinoAlertDialog(
      //borderColor: ColorStyle.mainColor,
      title: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          children: [
            TextSpan(
              text: 'Спасибо',
              style: TextStyle(
                fontSize: 18,
                color: ColorStyle.mainColor,
                fontWeight: FontWeight.w500,
              ),
            ),
            TextSpan(
              text: ' за работу',
              style: TextStyle(
                fontSize: 18,
                color: ColorStyle.txt,
                fontWeight: FontWeight.w500,
              ),
            ),
            TextSpan(
              text: '!',
              style: TextStyle(
                fontSize: 18,
                color: ColorStyle.mainColor,
                fontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
      ),
      content: Column(
        children: <Widget>[
          Text(
            'Ваш отчет отправлен на рассмотрение',
            style: TextStyle(
              fontSize: 18,
              color: ColorStyle.txt,
              fontWeight: FontWeight.w500,
            ),
          ),
          Container(height: 24),
          Text(
            'Время проведения:',
            style: TextStyle(
              fontSize: 18,
              color: ColorStyle.txt,
              fontWeight: FontWeight.w500,
            ),
          ),
          Container(height: 12),
          Text(
            timeSpent,
            style: TextStyle(
              fontSize: 24,
              color: ColorStyle.mainColor,
              fontWeight: FontWeight.w500,
            ),
          ),
        ],
      ),
      actions: <Widget>[
        CustomCupertinoDialogAction(
          child: Text('Ок'),
          onPressed: () => Navigator.of(context).pop(true),
          isDefaultAction: true,
        ),
        if (changeCarParams != null)
          CustomCupertinoDialogAction(
            child: Text(
              'Изменить инвентарь машины',
              style: TextStyle(color: ColorStyle.waiting, fontSize: 30),
              maxLines: 2,
            ),
            onPressed: () async {
              await changeCarParams();
              Navigator.of(context, rootNavigator: true).pop(true);
            },
            isDefaultAction: false,
          ),
      ],
    );
  }
}
