import 'dart:math';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:car_sharing_services/blocs/works.bloc.dart';
import 'package:car_sharing_services/models/work_adapter.dart';
import 'package:car_sharing_services/page/widget/button.dart';
import 'package:car_sharing_services/page/widget/loading_container.dart';
import 'package:car_sharing_services/pages/app/works/current_work/add_photo/car_after.page.dart';
import 'package:car_sharing_services/pages/app/works/current_work/car_params/car_params.page.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'add_photo/car_before.page.dart';
import 'finish_work_dialog.dart';

class CurrentWorkPage extends StatefulWidget {
  const CurrentWorkPage({
    Key key,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _CurrentWorkPageState();
}

class _CurrentWorkPageState extends State<CurrentWorkPage> {
  WorksBloc worksBloc;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    worksBloc = WorksBloc.of(context);
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<WorkAdapter>(
        stream: worksBloc.selectedWork,
        builder: (context, snapshot) {
          if (!snapshot.hasData) return LoadingContainer();
          var work = snapshot.data;
          return Scaffold(
            appBar: AppBar(
              title: Text(work?.name ?? ''),
              backgroundColor: Colors.transparent,
              elevation: 0.0,
            ),
            backgroundColor: ColorStyle.backgroundColor,
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerFloat,
            floatingActionButton: Container(
              height: 100,
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  (work.photoBefore?.isEmpty ?? true)
                      ? Button(
                          padding: EdgeInsets.fromLTRB(16, 12, 16, 12),
                          onPressed: () {
                            Navigator.of(context).push(CupertinoPageRoute(
                              builder: (_) => CarBeforePage(),
                            ));
                          },
                          width:
                              MediaQuery.of(context).size.width - 48 - 64 - 16,
                          color: ColorStyle.white2,
                          shadow: [
                            BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.05),
                                blurRadius: 12)
                          ],
                          textStyle: TextStyle(
                            color: ColorStyle.txt,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              SvgPicture.asset('assets/vectors/photo.svg'),
                              Container(width: 8, height: 0),
                              Expanded(
                                  child: AutoSizeText(
                                'Добавить фото до работы',
                                maxLines: 2,
                                minFontSize: 9,
                                textAlign: TextAlign.center,
                              )),
                            ],
                          ),
                        )
                      : Container(),
                  ((work.photoAfter?.isEmpty ?? true) &&
                          (work.photoBefore?.isNotEmpty ?? false))
                      ? Button(
                    padding: EdgeInsets.fromLTRB(16, 12, 16, 12),
                    onPressed: () {
                      Navigator.of(context).push(CupertinoPageRoute(
                        builder: (_) => CarAfterPage(),
                      ));
                    },
                    width:
                    MediaQuery
                        .of(context)
                        .size
                        .width - 48 - 64 - 16,
                    color: ColorStyle.white2,
                    shadow: [
                      BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.05),
                          blurRadius: 12)
                    ],
                    textStyle: TextStyle(
                      color: ColorStyle.txt,
                      fontSize: 13,
                      fontWeight: FontWeight.w500,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        SvgPicture.asset('assets/vectors/photo.svg'),
                        Container(width: 8, height: 0),
                        Expanded(
                            child: AutoSizeText(
                              'Добавить фото после работы',
                              maxLines: 2,
                              minFontSize: 9,
                              textAlign: TextAlign.center,
                            )),
                      ],
                    ),
                  )
                      : Container(),

                  ((work.photoBefore?.isNotEmpty ?? false) &&
                      (work.photoAfter?.isNotEmpty ?? false) &&
                      work.finishTime == null)
                      ? Button(
                    padding: EdgeInsets.fromLTRB(16, 12, 16, 12),
                    onPressed: () async {
                      var success = await worksBloc.closeWork();
                      if (success)
                        showDialog(
                            context: context,
                            builder: (_) =>
                                FinishWorkDialog(
                                  time: work.currentTime(),
                                  // changeCarParams: changeCarParams
                                )).then((res) {
                          changeCarParams().whenComplete(
                                  () => Navigator.of(context).pop(res));
                        });
                    },
                    width:
                    MediaQuery
                        .of(context)
                        .size
                        .width - 48 - 64 - 16,
                    color: ColorStyle.white2,
                    shadow: [
                      BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.05),
                          blurRadius: 12)
                    ],
                    textStyle: TextStyle(
                      color: ColorStyle.txt,
                      fontSize: 13,
                      fontWeight: FontWeight.w500,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        //SvgPicture.asset('assets/vectors/photo.svg'),
                        //Container(width: 8, height: 0),
                        Expanded(
                            child: AutoSizeText(
                              'Завершить работу',
                              maxLines: 2,
                              minFontSize: 9,
                              textAlign: TextAlign.center,
                            )),
                      ],
                    ),
                  )
                      : Container()

//            IconButton(
//              onPressed: () {
//                showDialog(
//                  context: context,
//                  builder: (_) => DeleteDialog(
//                    text: 'Удалить выполнение задания?',
//                  ),
//                ).then((delete) {
//                  if (delete == false) {
//                    return;
//                  } else {
//                    worksBloc.deleteSelectedWorkFromSelectedGroup();
//                    Navigator.of(context).pop();
//                  }
//                });
//                //marksKey.currentState.popUntil((route) => route.isFirst);
//              },
//              icon: Stack(
//                children: <Widget>[
//                  Container(
//                    width: 64,
//                    height: 64,
//                    decoration: BoxDecoration(
//                      shape: BoxShape.circle,
//                      color: ColorStyle.white2,
//                      boxShadow: [
//                        BoxShadow(
//                          color: Color.fromRGBO(0, 0, 0, 0.05),
//                          blurRadius: 12,
//                        )
//                      ],
//                    ),
//                  ),
//                  Positioned.fill(
//                    child: Center(
//                      child: SvgPicture.asset('assets/vectors/delete.svg'),
//                    ),
//                  ),
//                ],
//              ),
//              iconSize: 64,
//            ),
                ],
              ),
            ),
            body: ListView(
              physics: BouncingScrollPhysics(),
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    // number plate
                    Column(
                      children: <Widget>[
                        AutoSizeText(
                          'Номер машины',
                          maxLines: 1,
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            color: ColorStyle.txt,
                            fontSize: 13,
                          ),
                        ),
                        AutoSizeText(
                          work.carNumber ?? '',
                          maxLines: 1,
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            color: ColorStyle.txt,
                            fontSize: 18,
                          ),
                        ),
                      ],
                    ),
                    // service name
                    Column(
                      children: <Widget>[
                        AutoSizeText(
                          'Сервис каршеринга',
                          maxLines: 1,
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            color: ColorStyle.txt,
                            fontSize: 13,
                          ),
                        ),
                        AutoSizeText(
                          work.agentName ?? '',
                          maxLines: 1,
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            color: ColorStyle.txt,
                            fontSize: 18,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Container(height: 24),
                (work.photoBefore?.isEmpty ?? true)
                    ? Container()
                    : Center(
                  child: AutoSizeText(
                    'Фото до начала работ',
                    maxLines: 1,
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: ColorStyle.txt,
                      fontSize: 13,
                    ),
                  ),
                ),
                Container(height: 12),
                LayoutBuilder(
                  builder: (_, c) {
                    double size = min(c.biggest.width * 0.65, 200);
                    Widget res;
                    if (work.photoBefore?.isNotEmpty ?? false) {
                      res = Center(
                        child: Container(
                          width: size,
                          height: size,
                          decoration: BoxDecoration(
                            color: Colors.transparent,
                            borderRadius: BorderRadius.circular(8),
                            image: DecorationImage(
                                image: CachedNetworkImageProvider(
                                  work.photoBefore,
                                ),
                                fit: BoxFit.cover),
                          ),
                        ),
                      );
                    } else {
                      res = Container(
                        height: size,
                      );
                    }
                    return res;
                  },
                ),
                Container(height: 24),
                (work.photoAfter?.isEmpty ?? true)
                    ? Container()
                    : Center(
                  child: AutoSizeText(
                    'Фото после работ',
                    maxLines: 1,
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: ColorStyle.txt,
                      fontSize: 13,
                    ),
                  ),
                ),
                Container(height: 12),
                (work.photoAfter?.isEmpty ?? true)
                    ? Container()
                    : LayoutBuilder(
                  builder: (_, c) {
                    double size = min(c.biggest.width * 0.65, 200);
                    Widget res;
                    if (work.photoBefore?.isNotEmpty ?? false) {
                      res = Center(
                        child: Container(
                          width: size,
                          height: size,
                          decoration: BoxDecoration(
                            color: Colors.transparent,
                            borderRadius: BorderRadius.circular(8),
                            image: DecorationImage(
                              image: CachedNetworkImageProvider(
                                work.photoAfter,
                              ),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      );
                    } else {
                      res = Container(
                        height: size,
                      );
                    }
                    return Padding(
                      padding: const EdgeInsets.only(
                          bottom: 24.0 + 64.0 + 16.0),
                      child: res,
                    );
                  },
                ),
              ],
            ),
          );
        });
  }

  Future changeCarParams() async {
    await Navigator.of(context).push(CupertinoPageRoute(
        builder: (_) => CarParamsPage(), fullscreenDialog: true));
  }
}
