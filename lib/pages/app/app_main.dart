import 'dart:async';

import 'package:car_sharing_services/api_client/api_client.dart';
import 'package:car_sharing_services/blocs/core.bloc.dart';
import 'package:car_sharing_services/blocs/profile.bloc.dart';
import 'package:car_sharing_services/blocs/services.bloc.dart';
import 'package:car_sharing_services/blocs/storehouse.bloc.dart';
import 'package:car_sharing_services/blocs/works.bloc.dart';
import 'package:car_sharing_services/models/worker.dart';
import 'package:car_sharing_services/page/widget/bottom_bar.dart';
import 'package:car_sharing_services/page/widget/loading_container.dart';
import 'package:car_sharing_services/pages/app/profile/profile.page.dart';
import 'package:car_sharing_services/pages/app/store/inventory.page.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_svg/svg.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

import 'services/agents.page.dart';
import 'works/works.page.dart';

final GlobalKey<NavigatorState> storeKey = GlobalKey();
final GlobalKey<NavigatorState> marksKey = GlobalKey();
final GlobalKey<NavigatorState> listKey = GlobalKey();
final GlobalKey<NavigatorState> profileKey = GlobalKey();

class AppMainPage extends StatefulWidget {
  final Worker worker;
  final String token;
  final String serverAddr;

  const AppMainPage(
    this.token,
    this.worker, {
    //this.userId = 'af376ac4-53d1-40c4-bdf6-3d2909964b56',
    Key key,
    this.serverAddr,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _AppMainPageState();
}

class _AppMainPageState extends State<AppMainPage> {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<ApiClient>(
          create: (_) => ApiClient(
            Client(),
            '',
            defaultPrefix: '/api/v1',
            defaultHost: StaticHost(widget.serverAddr),
            defaultAuth: KeyAuth(widget.token),
            defaultHandler: (r) => throw Exception(),
          ),
          dispose: (context, api) => api.client.close(),
        ),
        ProxyProvider<ApiClient, ProfileBloc>(
          create: (context) => ProfileBloc(context, null, widget.worker),
          update: (context, api, bloc) => bloc
            ..context = context
            ..api = api,
          dispose: (_, bloc) => bloc.dispose(),
        ),
        ProxyProvider2<ApiClient, ProfileBloc, WorksBloc>(
          create: (context) => WorksBloc(context, null, null),
          update: (context, api, profileBloc, bloc) => bloc
            ..context = context
            ..api = api
            ..profileBloc = profileBloc,
          dispose: (_, bloc) => bloc.dispose(),
        ),
        ProxyProvider2<ApiClient, ProfileBloc, ServicesBloc>(
          create: (context) => ServicesBloc(context, null, null),
          update: (context, api, profileBloc, bloc) => bloc
            ..context = context
            ..api = api
            ..profileBloc = profileBloc,
          dispose: (_, bloc) => bloc.dispose(),
        ),
        ProxyProvider2<ApiClient, ProfileBloc, StorehouseBloc>(
          create: (context) => StorehouseBloc(context, null, null),
          update: (context, api, profileBloc, bloc) => bloc
            ..context = context
            ..api = api
            ..profileBloc = profileBloc,
          dispose: (_, bloc) => bloc.dispose(),
        ),
      ],
      child: Builder(
        builder: (context) => _BarAndContentBuilder(),
      ),
    );
  }
}

class _BarAndContentBuilder extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _BarAndContentBuilderState();
}

final RouteObserver<PageRoute> marksRouteObserver = RouteObserver<PageRoute>();
final RouteObserver<PageRoute> listRouteObserver = RouteObserver<PageRoute>();
final RouteObserver<PageRoute> profileRouteObserver =
    RouteObserver<PageRoute>();
final RouteObserver<PageRoute> storeRouteObserver = RouteObserver<PageRoute>();

class _BarAndContentBuilderState extends State<_BarAndContentBuilder>
    with TickerProviderStateMixin {
  Animation<double> animation;
  AnimationController animationController;
  BehaviorSubject<int> barController;
  BehaviorSubject<bool> barVisibilityController;
  BehaviorSubject<bool> keyboardShow;
  PublishSubject<bool> updateAnimation = PublishSubject();
  CoreBloc coreBloc;
  ProfileBloc profileBloc;
  bool isLoading;
  bool isVisible = true;

  StreamSubscription subscription;

  @override
  void initState() {
    super.initState();
    isLoading = false;
    animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    animation =
        Tween<double>(begin: 0, end: 100 + 34.0).animate(animationController)
          ..addListener(() {
            updateAnimation.add(true);
          });
    barVisibilityController = BehaviorSubject.seeded(true);
    barVisibilityController.listen((value) {
      if (!value)
        animationController.forward();
      else
        animationController.reverse();
    });
    keyboardShow = BehaviorSubject.seeded(false);
    subscription = KeyboardVisibility.onChange.listen((event) {
      if (!event) {
        keyboardShow.add(false);
      } else {
        keyboardShow.add(true);
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    barController?.close();
    barVisibilityController?.close();
    updateAnimation?.close();
    subscription?.cancel();
    keyboardShow?.close();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    coreBloc = CoreBloc.of(context);
    profileBloc = ProfileBloc.of(context);
    if ((profileBloc?.worker?.value?.statusId ?? 0) ==
            workerStatusType[WorkerStatus.accepted] ??
        false) {
      barController = BehaviorSubject.seeded(2);
    } else {
      barController = BehaviorSubject.seeded(3);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (isLoading) return Center(child: LoadingContainer());
    return WillPopScope(
        onWillPop: () async {
          int selected = barController.value;
          if (selected == 0) {
            if (storeKey.currentState.canPop()) {
              storeKey.currentState.pop();
              return false;
            } else {
              return true;
            }
          } else if (selected == 1) {
            if (marksKey.currentState.canPop()) {
              marksKey.currentState.pop();
              return false;
            } else {
              return true;
            }
          } else if (selected == 2) {
            if (listKey.currentState.canPop()) {
              listKey.currentState.pop();
              return false;
            } else {
              return true;
            }
          } else {
            if (profileKey.currentState.canPop()) {
              profileKey.currentState.pop();
              return false;
            } else {
              return true;
            }
          }
        },
        child: SafeArea(
          child: Scaffold(
              resizeToAvoidBottomInset: true,
              resizeToAvoidBottomPadding: true,
              backgroundColor: ColorStyle.white,
              body: BottomBarBuilder(
                selected: barController.mergeWith([coreBloc?.barIndex ?? 0]),
                builders: [
                      (_) =>
                          InventoryPage(
                        barVisibilityController: barVisibilityController,
                      ),
                      (_) =>
                      WorksPage(
                        barVisibilityController: barVisibilityController,
                      ),
                      (_) =>
                      AgentsPage(
                        barVisibilityController: barVisibilityController,
                      ),
                      (_) =>
                      ProfilePage(
                        barVisibilityController: barVisibilityController,
                      ),
                ],
                navigatorKeys: [
                  storeKey,
                  marksKey,
                  listKey,
                  profileKey,
                ],
                navigatorObservers: [
                  storeRouteObserver,
                  marksRouteObserver,
                  listRouteObserver,
                  profileRouteObserver
                ],
              ),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerDocked,
              floatingActionButton: StreamBuilder<bool>(
                  stream: keyboardShow.stream,
                  initialData: false,
                  builder: (context, snapshot) {
                    if (snapshot?.data ?? false) return Container();
                    return StreamBuilder<bool>(
                        stream: updateAnimation.stream,
                        initialData: false,
                        builder: (context, visibilitySnapshot) {
                          return Transform.translate(
                              offset: Offset(0, animation.value),
                              child: StreamBuilder<bool>(
                                  stream: profileBloc.approveToWork.stream,
                                  builder: (context, snapshot) {
                                    if (snapshot.data ?? false)
                                      return BottomBar(
                                        selected: barController.sink,
                                        outerSelected: coreBloc.barIndex,
                                        initSelected: barController.value,
                                        activeIcons: [
                                          Icon(
                                            Icons.store_mall_directory,
                                            color: Colors.white,
                                          ),
                                          SvgPicture.asset(
                                              'assets/vectors/marks_selected.svg'),
                                          SvgPicture.asset(
                                              'assets/vectors/list_selected.svg'),
                                          SvgPicture.asset(
                                              'assets/vectors/profile_selected.svg'),
                                        ],
                                        disabledIcons: <Widget>[
                                          Icon(
                                            Icons.store_mall_directory,
                                            color: Colors.green,
                                          ),
                                          SvgPicture.asset(
                                              'assets/vectors/marks.svg'),
                                          SvgPicture.asset(
                                              'assets/vectors/list.svg'),
                                          SvgPicture.asset(
                                              'assets/vectors/profile.svg'),
                                        ],
                                        navigatorKeys: [
                                          storeKey,
                                          marksKey,
                                          listKey,
                                          profileKey,
                                        ],
                                      );
                                    return Container();
                                  }));
                        });
                  })),
        ));
  }
}
