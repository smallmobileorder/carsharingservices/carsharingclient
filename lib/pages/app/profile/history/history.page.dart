import 'package:car_sharing_services/blocs/profile.bloc.dart';
import 'package:car_sharing_services/endpoints/history.dart';
import 'package:car_sharing_services/models/history.dart';
import 'package:car_sharing_services/page/widget/smart_pull_refresh.dart';
import 'package:car_sharing_services/pages/app/profile/history/widgets/month_builder.dart';
import 'package:car_sharing_services/pages/app/profile/history/widgets/today_builder.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:car_sharing_services/util/month_name.dart';
import 'package:car_sharing_services/utils/date_converter.dart';
import 'package:car_sharing_services/utils/weekday_name.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class HistoryPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  RefreshController controller = RefreshController(initialRefresh: true);
  ProfileBloc bloc;
  List<HistoryMonth> months;

  int offset = 0;
  int limit = 5;
  int max = -1;

  @override
  void initState() {
    months = [];
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc = ProfileBloc.of(context);
    getMoreMonths();
//    if (offset == 0) {
//      getMoreMonths();
//    }
  }

  Future getMoreMonths() async {
    WorkerHistoryPage page;
    try {
      page = await bloc.api.request(
        getHistory(bloc.userId, offset: offset, limit: limit),
      );
    } catch (e) {
      // TODO show some error message
      print("CATCHED ERR: $e");
      return;
    }
    max = page.max;
    offset += page.content.length;
    months.addAll(page.content);
    setState(() {});
  }

  bool canGetMoreMonths() {
    if (max == -1) {
      return true;
    }
    if (offset >= max) {
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    var now = DateTime.now();

    var today;
    if (months.length > 0 &&
        months[0].month == now.month &&
        months[0].year == now.year) {
      var todayDate = dateToString(now);
      var yesterdayDate = dateToString(now.subtract(Duration(days: 1)));
      var tiles = months[0].tiles.where((element) {
        var parts = element.time.split(":");
        var hour = int.tryParse(parts[0]) ?? 9;
        return (element.date == todayDate && hour < 9) ||
            (element.date == yesterdayDate && hour >= 9);
      }).toList();
      var earned = tiles
          .where((element) => element.positive && !element.payout)
          .fold(0, (acc, element) => acc += element.amount);
      var payed = tiles
          .where((element) => element.positive && element.payout)
          .fold(0, (acc, element) => acc += element.amount);
      var penalty = tiles
          .where((element) => !element.positive)
          .fold(0, (acc, element) => acc += element.amount);
      today = TodayBuilder(
          HistoryMonth(now.month, WeekdayName.getName(now.weekday), now.year,
              earned, payed, penalty, tiles),
          key: months[0].getKey());
    } else {
      today = MonthBuilder(HistoryMonth(
          now.month, WeekdayName.getName(now.weekday), now.year, 0, 0, 0, []));
    }
    var currentMonth;
    if (months.length > 0 &&
        months[0].month == now.month &&
        months[0].year == now.year) {
      print(months[0].monthName);
      print(months[0].tiles.length);
      currentMonth = MonthBuilder(months[0], key: months[0].getKey());
    } else {
      currentMonth = MonthBuilder(HistoryMonth(
          now.month, MonthName.getName(now.month), now.year, 0, 0, 0, []));
    }

    var prevMonths = [];
    for (int i = 1; i < months.length; i++) {
      if (months[i] != null) {
        prevMonths.add(Container(height: 20));
        prevMonths.add(MonthBuilder(months[i]));
      }
    }

    return Scaffold(
      backgroundColor: ColorStyle.white,
      appBar: AppBar(
        title: Text('История'),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      body: SmartPullRefresh(
        controller: controller,
        onRefresh: () {
          Future.wait([
            bloc.updateOnlyHistoryInfo(),
          ]);
          controller.refreshCompleted();
        },
        child: ListView(
          padding: EdgeInsets.fromLTRB(24, 0, 24, 24),
          physics: BouncingScrollPhysics(),
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Текущая смена - месяц',
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 18,
                  color: ColorStyle.sideColor,
                ),
              ),
            ),
            StreamBuilder<CommonHistoryInfo>(
              stream: bloc.commonHistoryInfo,
              builder: (_, snapshot) {
                if (snapshot.data == null) {
                  return Container();
                }
                // penalty is positive
                num unpaid = snapshot.data.allSalary -
                    snapshot.data.allPayed -
                    snapshot.data.allPenalty;
                if (unpaid == 0) {
                  return Container();
                }
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Вам ${unpaid > 0 ? 'недоплачено' : 'переплачено'} - ${unpaid.abs()}',
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 13,
                      color: ColorStyle.sideColor,
                    ),
                  ),
                );
              },
            ),
            today,
            Container(
              height: 24,
            ),
            currentMonth,
            Container(
              height: 16,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Предыдущие месяцы',
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 18,
                    color: ColorStyle.sideColor),
              ),
            ),
            Container(
              height: 16,
            ),
            ...prevMonths,
//            StreamBuilder<List<MonthHistoryItem>>(
//              stream: bloc.prevMonthsHistory,
//              builder: (_, snapshot) {
//                if (snapshot.data == null || snapshot.data.isEmpty) {
//                  return Container();
//                }
//                List<Widget> res = snapshot.data
//                    .map((i) => MonthHistoryTile(
//                  historyItem: BehaviorSubject.seeded(i),
//                ))
//                    .expand((i) => [i, Container(height: 24)])
//                    .toList();
//                if (res.isNotEmpty) {
//                  res.removeLast();
//                }
//                return Column(children: res);
//              },
//            ),
          ],
        ),
      ),
    );
  }
}
