import 'package:car_sharing_services/models/history.dart';
import 'package:car_sharing_services/page/widget/custom_cupertino_alert_dialog.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TileBuilder extends StatelessWidget {
  final HistoryTile tile;

  const TileBuilder(this.tile, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (tile.comment != null) {
          showComment(context);
        }
      },
      child: Container(
        padding: EdgeInsets.all(4),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  tile.name ?? '',
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    color: tile.positive ? ColorStyle.txt : ColorStyle.wrong,
                  ),
                ),
                Container(
                  height: 8,
                ),
                Text(
                  tile.time,
                  style: TextStyle(
                    fontSize: 11,
                    fontWeight: FontWeight.w500,
                    color: ColorStyle.sideColor,
                  ),
                )
              ],
            ),
            Text(
              tile.amount?.toString() ?? '',
              style: TextStyle(
                fontSize: 13,
                fontWeight: FontWeight.w500,
                color: tile.positive ? ColorStyle.mainColor : ColorStyle.wrong,
              ),
            )
          ],
        ),
      ),
    );
  }

  showComment(BuildContext context) {
    var text = tile.comment;
    if (text == null || text.isEmpty) {
      text = 'Нет комментария';
    }
    showDialog(
      context: context,
      useRootNavigator: true,
      builder: (_) => CustomCupertinoAlertDialog(
        title: Text(tile.name ?? ''),
        content: Text(text),
        actions: <Widget>[
          CustomCupertinoDialogAction(
            isDefaultAction: true,
            child: Text('OK'),
            onPressed: () => Navigator.of(context, rootNavigator: true).pop(),
          )
        ],
      ),
    );
  }
}
