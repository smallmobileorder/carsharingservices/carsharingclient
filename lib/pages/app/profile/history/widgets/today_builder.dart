import 'package:car_sharing_services/models/history.dart';
import 'package:car_sharing_services/page/widget/button.dart';
import 'package:car_sharing_services/pages/app/profile/history/widgets/tile_builder.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:car_sharing_services/utils/weekday_name.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TodayBuilder extends StatefulWidget {
  // TODO maybe make new class for holding today date
  final HistoryMonth month;

  const TodayBuilder(this.month, {Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _TodayBuilderState();
}

class _TodayBuilderState extends State<TodayBuilder> {
  bool extended = false;
  HistoryMonth month;

  @override
  void initState() {
    this.month = widget.month;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var buttonContent = <Widget>[];

    //buttonContent.add(Text(month.monthName));
    buttonContent.add(Text('Текущая смена'));

    if (extended) {
      buttonContent.add(Icon(Icons.arrow_drop_up, color: Colors.black));
    } else {
      buttonContent.add(Icon(Icons.arrow_drop_down, color: Colors.black));
    }

    if ((month.penalty ?? 0) > 0) {
      // add penalty amount
      buttonContent.add(Text(
        month.penalty?.toString() ?? '',
        style: TextStyle(color: ColorStyle.wrong),
      ));
    }

    // make huge space between contents
    buttonContent.add(Expanded(child: Container()));

    buttonContent.add(Text(
      '${month.earned ?? 0}',
      style: TextStyle(color: ColorStyle.mainColor),
    ));

    var content = <Widget>[];

    content.add(Button(
      child: Row(children: buttonContent),
      padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
      color: ColorStyle.white2,
      borderRadius: 8,
      shadow: [BoxShadow(color: Color.fromRGBO(0, 0, 0, 0.05), blurRadius: 12)],
      textStyle: TextStyle(
        color: ColorStyle.txt,
        fontSize: 13,
        fontWeight: FontWeight.w500,
      ),
      onPressed: () => setState(() => extended = !extended),
    ));

    if (extended) {
      content.addAll(buildTiles());
    }

    return AnimatedContainer(
      width: double.infinity,
      duration: Duration(milliseconds: 300),
      child: Column(children: content),
    );
  }

  List<Widget> buildTiles() {
    var res = <Widget>[];
    if ((month.tiles?.length ?? 0) == 0) {
      return res;
    }
    var prevTile = month.tiles[0];
    res.add(getDayTitle(prevTile));
    res.add(TileBuilder(prevTile));
    for (int i = 1; i < month.tiles.length; i++) {
      res.add(Container(height: 20));
      if (month.tiles[i].date != prevTile.date) {
        res.add(getDayTitle(month.tiles[i]));
        prevTile = month.tiles[i];
      }
      res.add(TileBuilder(month.tiles[i]));
    }
    return res;
  }

  Widget getDayTitle(HistoryTile tile) {
    var date = tile.date;
    var weekday = WeekdayName.getName(tile.getDate().weekday);
    return Container(
      alignment: Alignment.topLeft,
      padding: EdgeInsets.fromLTRB(0, 13, 0, 20),
      child: Text(
        '$date - $weekday',
        textAlign: TextAlign.left,
        style: TextStyle(
          color: ColorStyle.sideColor,
          fontSize: 13,
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }
}
