import 'dart:async';
import 'dart:io';

import 'package:car_sharing_services/blocs/profile.bloc.dart';
import 'package:car_sharing_services/pages/photo/photo_review.page.dart';
import 'package:car_sharing_services/pages/photo/questions.page.dart';
import 'package:car_sharing_services/page/widget/loading_container.dart';
import 'package:car_sharing_services/util/photo_maker.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

class PassportSelfieProblemPage extends StatefulWidget {
  PassportSelfieProblemPage({Key key}) : super(key: key);

  @override
  _PassportSelfieProblemPageState createState() =>
      _PassportSelfieProblemPageState();
}

class _PassportSelfieProblemPageState extends State<PassportSelfieProblemPage> {
  File image;
  final BehaviorSubject<bool> imageLoaded = BehaviorSubject();
  final String title = 'Селфи с паспортом';
  final String text =
      'Сфотографируйте себя с паспортом чтобы отчетливо было видно ваше лицо и фото в паспорте';
  final String imageStubPath = 'assets/vectors/selfie.svg';
  final List<String> questionsForReview = [
    'Ваше лицо отчетливо видно?',
    'Ваш документ отчетливо видно?',
    'Отчетливо видно серию и номер паспорта?',
    'Отчетливо видно фамилию, имя и отчество в паспорте?',
  ];

  /// need fpr rebuild widget when use retake func
  String timeKey;

  final BehaviorSubject<bool> isLoadingToServer = BehaviorSubject();

  @override
  void initState() {
    super.initState();
    imageLoaded.add(false);
  }

  @override
  void dispose() {
    imageLoaded?.close();
    isLoadingToServer?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        StreamBuilder<bool>(
            stream: imageLoaded,
            initialData: false,
            builder: (context, snapshot) {
              if (snapshot?.data ?? false)
                return QuestionsPage(
                  key: Key(timeKey ?? 'default'),
                  title: title,
                  image: image,
                  questions: questionsForReview,
                  accept: onAccept,
                  back: onBack,
                  retake: onRetake,
                );
              return PhotoReviewPage(
                title: title,
                text: text,
                imageStubPath: imageStubPath,
                nextFabPressed: nextFabPressed,
              );
            }),
        StreamBuilder<bool>(
          initialData: false,
          stream: isLoadingToServer.stream,
          builder: (context, snapshot) {
            if (snapshot.data ?? false)
              return Center(
                child: LoadingContainer(),
              );
            return Container();
          },
        )
      ],
    );
  }

  FutureOr nextFabPressed() async {
    image = await PhotoMaker.makePhoto(context);
    if (image != null) {
      timeKey = DateTime.now().toIso8601String();
      imageLoaded.add(true);
    }
  }

  onAccept() async {
    isLoadingToServer.add(true);
    await Provider.of<ProfileBloc>(context, listen: false)
        .updatePhoto(image, PhotoType.passportSelfie);
    Navigator.of(context, rootNavigator: false).pop();
    isLoadingToServer.add(false);
    imageLoaded.add(false);
  }

  onBack() {
    if (isLoadingToServer?.value ?? false) return;
    imageLoaded.add(false);
    image = null;
  }

  onRetake() async {
    image = await PhotoMaker.makePhoto(context);
    if (image != null) {
      timeKey = DateTime.now().toIso8601String();
      imageLoaded.add(true);
    }
  }
}
