import 'dart:async';

import 'package:car_sharing_services/blocs/profile.bloc.dart';
import 'package:car_sharing_services/page/widget/custom_text_field.dart';
import 'package:car_sharing_services/page/widget/next_fab.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

class PassportDataProblemPage extends StatefulWidget {
  PassportDataProblemPage({Key key}) : super(key: key);

  @override
  _PassportDataProblemPageState createState() =>
      _PassportDataProblemPageState();
}

class _PassportDataProblemPageState extends State<PassportDataProblemPage> {
  final validateFormKey = GlobalKey<FormState>();
  final BehaviorSubject<bool> blocked = BehaviorSubject();
  final BehaviorSubject<bool> visible = BehaviorSubject();
  final TextEditingController serialController = TextEditingController();
  final TextEditingController numberController = TextEditingController();
  final TextEditingController orgNameController = TextEditingController();
  final TextEditingController dateController = TextEditingController();
  final TextEditingController addressController = TextEditingController();
  final FocusNode serialFocus = FocusNode();
  final FocusNode numberFocus = FocusNode();
  final FocusNode orgNameFocus = FocusNode();
  final FocusNode addressFocus = FocusNode();

  StreamSubscription subscription;

  @override
  void initState() {
    super.initState();
    serialController.addListener(checkPassportSerial);
    numberController.addListener(checkPassportNumber);
    serialController.addListener(checkControllers);
    orgNameController.addListener(checkControllers);
    numberController.addListener(checkControllers);
    dateController.addListener(checkControllers);
    addressController.addListener(checkControllers);

    subscription = KeyboardVisibility.onChange.listen((event) {
      if (!event) {
        visible.add(true);
        serialFocus.unfocus();
        numberFocus.unfocus();
        orgNameFocus.unfocus();
        addressFocus.unfocus();
      } else {
        visible.add(false);
      }
    });
  }

  @override
  void dispose() {
    visible?.close();
    blocked?.close();
    serialController.dispose();
    orgNameController.dispose();
    numberController.dispose();
    addressController.dispose();
    serialFocus
      ..unfocus()
      ..dispose();
    orgNameFocus
      ..unfocus()
      ..dispose();
    numberFocus
      ..unfocus()
      ..dispose();
    addressFocus
      ..unfocus()
      ..dispose();
    subscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
            backgroundColor: ColorStyle.backgroundColor,
            appBar: AppBar(
              centerTitle: true,
              elevation: 0,
              backgroundColor: Colors.transparent,
              title: Text('Паспортные данные'),
            ),
            body: SingleChildScrollView(
              padding: const EdgeInsets.only(left: 36, right: 36, bottom: 70),
              child: Form(
                key: validateFormKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: 24,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: CustomTextField(
                            label: 'Серия',
                            controller: serialController,
                            focusNode: serialFocus,
                            keyboardType: TextInputType.number,
                            validator: passportSerialValidator,
                          ),
                        ),
                        Container(
                          width: 24,
                        ),
                        Expanded(
                          flex: 2,
                          child: CustomTextField(
                            label: 'Номер',
                            controller: numberController,
                            focusNode: numberFocus,
                            keyboardType: TextInputType.number,
                            validator: passportNumberValidator,
                          ),
                        ),
                      ],
                    ),
                    Container(
                      height: 24,
                    ),
                    CustomTextField(
                      label: 'Дата выдачи (день, месяц, год)',
                      controller: dateController,
                      datePickMode: true,
                      onTap: chooseDate,
                    ),
                    Container(
                      height: 24,
                    ),
                    CustomTextField(
                      label: 'Наименование органа, выдавшего паспорт',
                      controller: orgNameController,
                      focusNode: orgNameFocus,
                      capitalization: TextCapitalization.sentences,
                    ),
                    Container(
                      height: 24,
                    ),
                    CustomTextField(
                      label: 'Адрес фактического проживания',
                      controller: addressController,
                      focusNode: addressFocus,
                      capitalization: TextCapitalization.sentences,
                    ),
//                    Container(
//                      height: 90,
//                    ),
                  ],
                ),
              ),
            ),
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerDocked,
            floatingActionButton: StreamBuilder<bool>(
                stream: visible.stream,
                builder: (context, snapshot) {
                  return Visibility(
                    visible: snapshot?.data ?? true,
                    child: NextFab(
                      blockedStream: blocked.stream,
                      text: 'Отправить',
                      nextFabPressed: nextFabPressed,
                    ),
                  );
                })));
  }

  FutureOr nextFabPressed() async {
    if (validateFormKey.currentState.validate()) {
      await Provider.of<ProfileBloc>(context, listen: false).updatePassport(
        serialController.text.trim(),
        numberController.text.trim(),
        dateController.text,
        addressController.text.trim(),
        orgNameController.text.trim(),
      );
      Navigator.of(context, rootNavigator: false).pop();
    }
  }

  void checkControllers() {
    if ((serialController.text?.isEmpty ?? true) ||
        (numberController.text?.isEmpty ?? true) ||
        (orgNameController.text?.isEmpty ?? true) ||
        (addressController.text?.isEmpty ?? true) ||
        (dateController.text?.isEmpty ?? true)) {
      blocked.add(true);
    } else {
      blocked.add(false);
    }
  }

  chooseDate() async {
    var now = DateTime.now();
    DateTime temp = await showDatePicker(
        helpText: "ВЫБЕРИТЕ ДАТУ",
        context: context,
        initialDate: DateTime(now.year, now.month, now.day),
        firstDate: DateTime(now.year - 60, now.month, now.day),
        lastDate: DateTime(now.year, now.month, now.day));
    if (temp != null) {
      await initializeDateFormatting("ru_RU", null);
      dateController.text = DateFormat.yMd('ru').format(temp);
    }
  }

  String passportNumberValidator(String text) {
    if (text?.trim()?.isNotEmpty ?? false) {
      var temp = text.trim();
      if (temp.length == 6 && RegExp(r'\d\d\d\d\d\d').hasMatch(temp)) {
        return null;
      }
    }
    return 'Номер паспорта введен неверно';
  }

  String passportSerialValidator(String text) {
    if (text?.trim()?.isNotEmpty ?? false) {
      var temp = text.trim();
      if (temp.length == 4 && RegExp(r'\d\d\d\d').hasMatch(temp)) {
        return null;
      }
    }
    return 'Серия паспорта введена неверно';
  }

  void checkPassportSerial() {
    if (serialController.text.length > 4) {
      var temp = serialController.selection.extentOffset;
      serialController.value = TextEditingValue(
        text: serialController.text.substring(0, 4),
        selection: TextSelection.fromPosition(
          TextPosition(offset: temp > 4 ? 4 : temp),
        ),
      );
    }
  }

  void checkPassportNumber() {
    if (numberController.text.length > 6) {
      var temp = numberController.selection.extentOffset;
      numberController.value = TextEditingValue(
        text: numberController.text.substring(0, 6),
        selection: TextSelection.fromPosition(
          TextPosition(offset: temp > 6 ? 6 : temp),
        ),
      );
    }
  }
}
