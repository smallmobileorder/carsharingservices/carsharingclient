import 'dart:async';

import 'package:car_sharing_services/blocs/profile.bloc.dart';
import 'package:car_sharing_services/page/widget/custom_text_field.dart';
import 'package:car_sharing_services/page/widget/custom_will_pop_widget.dart';
import 'package:car_sharing_services/page/widget/next_fab.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

class EmailDataProblemPage extends StatefulWidget {
  EmailDataProblemPage({Key key}) : super(key: key);

  @override
  _EmailDataProblemPageState createState() => _EmailDataProblemPageState();
}

class _EmailDataProblemPageState extends State<EmailDataProblemPage> {
  final validateFormKey = GlobalKey<FormState>();
  final BehaviorSubject<bool> blocked = BehaviorSubject();
  final BehaviorSubject<bool> visible = BehaviorSubject();
  final TextEditingController emailController = TextEditingController();
  final FocusNode emailFocus = FocusNode();

  StreamSubscription subscription;

  @override
  void initState() {
    super.initState();
    emailController.addListener(checkControllers);

    subscription = KeyboardVisibility.onChange.listen((event) {
      if (!event) {
        visible.add(true);
        emailFocus?.unfocus();
      } else {
        visible.add(false);
      }
    });
  }

  @override
  void dispose() {
    visible?.close();
    blocked?.close();
    emailController?.dispose();
    subscription?.cancel();
    emailFocus
      ..unfocus()
      ..dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomWillPopWidget(
      dialogContent: 'Введенные данные будут утеряны',
      useRootNavigator: false,
      child: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Scaffold(
              backgroundColor: ColorStyle.backgroundColor,
              appBar: AppBar(
                centerTitle: true,
                elevation: 0,
                backgroundColor: Colors.transparent,
                title: Text('Почта для договора'),
              ),
              body: SingleChildScrollView(
                padding: const EdgeInsets.only(left: 36, right: 36, bottom: 70),
                child: Form(
                  key: validateFormKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 24,
                      ),
                      Container(
                        child: Text(
                          'Введите вашу действующую электронную почту, на которую будет отправлен договор',
                          style: TextStyle(
                              fontSize: 13, color: ColorStyle.sideColor),
                          textAlign: TextAlign.start,
                        ),
                      ),
                      Container(
                        height: 16,
                      ),
                      CustomTextField(
                        label:
                            'Следуйте инструкциям, указанным в письме для подтверждения вашей учетной записи',
                        controller: emailController,
                        focusNode: emailFocus,
                        keyboardType: TextInputType.emailAddress,
                        capitalization: TextCapitalization.none,
                        validator: emailValidator,
                      ),
                    ],
                  ),
                ),
              ),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerDocked,
              floatingActionButton: StreamBuilder<bool>(
                  stream: visible.stream,
                  builder: (context, snapshot) {
                    return Visibility(
                      visible: snapshot?.data ?? true,
                      child: NextFab(
                        blockedStream: blocked.stream,
                        text: 'Отправить',
                        nextFabPressed: nextFabPressed,
                      ),
                    );
                  }))),
    );
  }

  FutureOr nextFabPressed() async {
    if (validateFormKey.currentState.validate()) {
      await Provider.of<ProfileBloc>(context, listen: false)
          .updateEmail(emailController.text.trim());
      Navigator.of(context, rootNavigator: false).pop();
    }
  }

  void checkControllers() {
    if ((emailController.text?.isEmpty ?? true)) {
      blocked.add(true);
    } else {
      blocked.add(false);
    }
  }

  String emailValidator(String text) {
    String validate =
        r'(?:[a-z0-9!#$%&"*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&"*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])';
    RegExp regExp = RegExp(validate);
    if (regExp.hasMatch(text)) return null;
    return 'Почта введена неверно';
  }
}
