import 'package:car_sharing_services/blocs/profile.bloc.dart';
import 'package:car_sharing_services/models/history.dart';
import 'package:car_sharing_services/models/reg_error.dart';
import 'package:car_sharing_services/models/worker.dart';
import 'package:car_sharing_services/page/widget/button.dart';
import 'package:car_sharing_services/page/widget/custom_cupertino_alert_dialog.dart';
import 'package:car_sharing_services/page/widget/smart_pull_refresh.dart';
import 'package:car_sharing_services/pages/app/profile/widgets/history_tile.dart';
import 'package:car_sharing_services/pages/app/profile/widgets/worker_info.dart';
import 'package:car_sharing_services/pages/splash.page.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:rxdart/rxdart.dart';

import '../app_main.dart';
import 'about/about.page.dart';
import 'feedback/feedback.page.dart';
import 'history/history.page.dart';
import 'reg_problem/email_data_problem.page.dart';
import 'reg_problem/license/license_back_photo_problem.page.dart';
import 'reg_problem/license/license_data_problem.page.dart';
import 'reg_problem/license/license_front_photo_problem.page.dart';
import 'reg_problem/passport/passport_data_problem.page.dart';
import 'reg_problem/passport/passport_main_photo_problem.page.dart';
import 'reg_problem/passport/passport_reg_photo_problem.page.dart';
import 'reg_problem/passport/passport_selfie_problem.page.dart';
import 'reg_problem/payout/bank_data_problem.page.dart';
import 'reg_problem/payout/card_data_problem.page.dart';
import 'reg_problem/personal_data_problem.page.dart';

class ProfilePage extends StatefulWidget {
  final BehaviorSubject barVisibilityController;

  const ProfilePage({Key key, @required this.barVisibilityController})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _ProfilePage();
}

class _ProfilePage extends State<ProfilePage> with RouteAware {
  RefreshController controller = RefreshController();
  ProfileBloc bloc;

  String earnedToday = '1160';

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    profileRouteObserver.subscribe(this, ModalRoute.of(context));
    bloc = ProfileBloc.of(context);
    bloc.updateOnlyHistoryInfo();
  }

  @override
  void dispose() {
    profileRouteObserver.unsubscribe(this);
    super.dispose();
  }

  @override
  void didPopNext() {
    super.didPopNext();
    widget.barVisibilityController.add(true);
  }

  @override
  void didPushNext() {
    super.didPushNext();
    widget.barVisibilityController.add(false);
  }

  @override
  Widget build(BuildContext context) {
    Orientation orientation = MediaQuery.of(context).orientation;
    return Scaffold(
      backgroundColor: ColorStyle.white,
      appBar: AppBar(
        title: Text('Профиль'),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      body: SmartPullRefresh(
        controller: controller,
        onRefresh: () async {
          await bloc.updateWorkerInfo();
          controller.refreshCompleted();
        },
        child: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 0 /*24*/),
              child: orientation == Orientation.portrait
                  ? Column(children: buildContent(context, true))
                  : Row(children: buildContent(context, false)),
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> buildContent(BuildContext context, bool portrait) {
    return [
      Expanded(
        flex: 2,
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 5,
              child: Stack(
                children: <Widget>[
                  portrait
                      ? LayoutBuilder(
                          builder: (_, c) {
                            return Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(29),
                                  topRight: Radius.circular(29),
                                ),
                                color: ColorStyle.backgroundColor,
                              ),
                              margin: EdgeInsets.only(top: c.maxHeight / 2),
                              height: c.maxHeight / 2,
                              width: c.maxWidth,
                            );
                          },
                        )
                      : Container(),
                  LayoutBuilder(
                    builder: (_, c) {
                      double size = c.biggest.shortestSide;
                      return Center(
                        child: Container(
                          width: size,
                          height: size,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(size / 2),
                          ),
                          child: Center(
                            child: SvgPicture.asset(
                              'assets/vectors/key.svg',
                              height: size * 0.7,
                              width: size * 0.7,
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
            Container(
              height: 16,
              color: portrait ? ColorStyle.backgroundColor : Colors.transparent,
            ),
            Expanded(
              flex: 4,
              child: Container(
                color:
                    portrait ? ColorStyle.backgroundColor : Colors.transparent,
                child: StreamBuilder<Worker>(
                    stream: bloc.worker,
                    builder: (context, snapshot) {
                      return WorkerInfoWidget(
                        worker: snapshot.data,
                      );
                    }),
              ),
            ),
            Container(height: portrait ? 0 : 60),
          ],
        ),
      ),
      Container(
        width: portrait ? 0 : 32,
        color: portrait ? ColorStyle.backgroundColor : Colors.transparent,
      ),
      Expanded(
        flex: 3,
        child: StreamBuilder<Worker>(
            stream: bloc.worker.stream,
            builder: (context, snapshot) {
              if ((snapshot?.data?.statusId ??
                      workerStatusType[WorkerStatus.needModerate]) ==
                  workerStatusType[WorkerStatus.needModerate])
                return Container(
                  color: ColorStyle.backgroundColor,
                  child: aboutValidation(
                      portrait ? Orientation.portrait : Orientation.landscape),
                );
              if (snapshot?.data?.statusId ==
                  workerStatusType[WorkerStatus.declined])
                return StreamBuilder<List<RegError>>(
                    stream: bloc.regErrors.stream,
                    builder: (context, snapshotError) {
                      return Container(
                        color: ColorStyle.backgroundColor,
                        child: badData(
                            snapshotError?.data?.first,
                            portrait
                                ? Orientation.portrait
                                : Orientation.landscape),
                      );
                    });

              return Container(
                color:
                    portrait ? ColorStyle.backgroundColor : Colors.transparent,
                padding: portrait
                    ? EdgeInsets.fromLTRB(24, 0, 24, 60)
                    : EdgeInsets.only(right: 24, bottom: 60),
                child: ListView(
                  physics: BouncingScrollPhysics(),
                  children: <Widget>[
                    StreamBuilder<CommonHistoryInfo>(
                      stream: bloc.commonHistoryInfo,
                      builder: (_, snapshot) {
                        var earned = 0;
                        if (snapshot.data != null) {
                          earned = snapshot.data.todayEearned ?? 0;
                        }
                        return HistoryTileBuilder(
                          earnedToday: earned?.toString() ?? '',
                          onPressed: () =>
                              Navigator.of(context, rootNavigator: false).push(
                            CupertinoPageRoute(builder: (_) => HistoryPage()),
                          ),
                        );
                      },
                    ),
                    Container(height: 24),
                    _CustomButton(
                      text: 'О приложении',
                      icon: SvgPicture.asset('assets/vectors/info.svg'),
                      onPressed: () => Navigator.of(context).push(
                        CupertinoPageRoute(builder: (_) => AboutPage()),
                      ),
                      transparentIcon: SvgPicture.asset(
                        'assets/vectors/info.svg',
                        color: Colors.transparent,
                      ),
                    ),
                    Container(height: 24),
                    _CustomButton(
                      text: 'Обратная связь',
                      icon: Icon(
                        CupertinoIcons.phone,
                        color: ColorStyle.mainColor,
                        size: 32,
                      ),
                      onPressed: () {
                        Navigator.of(context, rootNavigator: false).push(
                          CupertinoPageRoute(builder: (_) => FeedbackPage()),
                        );
                      },
                      transparentIcon: Icon(
                        CupertinoIcons.phone,
                        color: Colors.transparent,
                        size: 32,
                      ),
                    ),
                    Container(height: 24),
                    _CustomButton(
                      text: 'Выйти из аккаунта',
                      icon: SvgPicture.asset('assets/vectors/exit.svg'),
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (context) => CustomCupertinoAlertDialog(
                            title: Text('Выйти из аккаунта?'),
                            actions: <Widget>[
                              CustomCupertinoDialogAction(
                                child: Text('Да'),
                                onPressed: () =>
                                    Navigator.of(context).pop(true),
                              ),
                              CustomCupertinoDialogAction(
                                child: Text('Нет'),
                                onPressed: () =>
                                    Navigator.of(context).pop(false),
                                isDefaultAction: true,
                              ),
                            ],
                          ),
                        ).then((exit) async {
                          if (exit) {
                            await bloc.logout();
                            Navigator.of(context, rootNavigator: true)
                                .popUntil((r) => r.isFirst);
                            Navigator.of(context, rootNavigator: true)
                                .pushReplacement(CupertinoPageRoute(
                              builder: (_) => SplashPage(),
                            ));
                          }
                        });
                      },
                      transparentIcon: SvgPicture.asset(
                        'assets/vectors/exit.svg',
                        color: Colors.transparent,
                      ),
                    ),
                    Container(height: 24),
                  ],
                ),
              );
            }),
      ),
    ];
  }

  Widget aboutValidation(Orientation orientation) {
    return Padding(
      padding: const EdgeInsets.only(left: 24, right: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            'На ваш e-mail отправлено письмо с договором и дальнейшая инструкция',
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w500,
              color: ColorStyle.waiting,
            ),
            textAlign: TextAlign.center,
          ),
          Container(
            height: orientation == Orientation.landscape ? 5 : 10,
          ),
          Text(
            'Если письмо не пришло, то проверьте правильность введенной почты и попробуйте снова',
            style: TextStyle(
              fontSize: 13,
              color: ColorStyle.sideColor,
            ),
            textAlign: TextAlign.center,
          ),
          Container(
            height: orientation == Orientation.landscape ? 5 : 10,
          ),
          Button(
            onPressed: () => Navigator.of(context, rootNavigator: false).push(
                CupertinoPageRoute(builder: (_) => EmailDataProblemPage())),
            child: Text(
              'Ввести почту заново',
              style: TextStyle(fontSize: 13),
            ),
            shadow: [
              BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.05),
                blurRadius: 12,
              )
            ],
          ),
          Container(height: 16),
          Button(
            child: Text(
              'Выйти из аккаунта',
              style: TextStyle(
                fontSize: 13,
                color: ColorStyle.txt,
              ),
            ),
            shadow: [
              BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.05),
                blurRadius: 12,
              )
            ],
            color: ColorStyle.white2,
            onPressed: () {
              showDialog(
                context: context,
                builder: (context) => CustomCupertinoAlertDialog(
                  title: Text('Выйти из аккаунта?'),
                  actions: <Widget>[
                    CustomCupertinoDialogAction(
                      child: Text('Да'),
                      onPressed: () => Navigator.of(context).pop(true),
                    ),
                    CustomCupertinoDialogAction(
                      child: Text('Нет'),
                      onPressed: () => Navigator.of(context).pop(false),
                      isDefaultAction: true,
                    ),
                  ],
                ),
              ).then((exit) async {
                if (exit) {
                  await bloc.logout();
                  Navigator.of(context, rootNavigator: true)
                      .popUntil((r) => r.isFirst);
                  Navigator.of(context, rootNavigator: true)
                      .pushReplacement(CupertinoPageRoute(
                    builder: (_) => SplashPage(),
                  ));
                }
              });
            },
          ),
        ],
      ),
    );
  }

  Widget badData(RegError regError, Orientation orientation) {
    String buttonText = '';
    String problemText = '';
    String hintText = '';
    Function onPressed;
    if (regError?.id == null) return Container();
    switch (errorType[regError.id]) {
      case BadDataType.badPassportData:
        onPressed = () => Navigator.of(context, rootNavigator: false)
            .push(CupertinoPageRoute(builder: (_) => PassportDataProblemPage()))
            .then((_) => controller.requestRefresh());
        problemText = 'Некорректные паспортные данные';
        buttonText = 'Ввести данные заново';
        hintText =
            'Введите данные еще раз и внимательно проверьте перед отправкой';
        break;
      case BadDataType.badPassportFirstPhoto:
        onPressed = () => Navigator.of(context, rootNavigator: false)
            .push(CupertinoPageRoute(
                builder: (_) => PassportMainPhotoProblemPage()))
            .then((_) => controller.requestRefresh());
        problemText = 'Фотография паспорта не соответствует требованиям';
        buttonText = 'Сделать фото';
        hintText = 'Попробуйте сделать повторный снимок';
        break;
      case BadDataType.badPassportRegPhoto:
        onPressed = () => Navigator.of(context, rootNavigator: false)
            .push(CupertinoPageRoute(
                builder: (_) => PassportRegPhotoProblemPage()))
            .then((_) => controller.requestRefresh());
        problemText = 'Фотография паспорта не соответствует требованиям';
        buttonText = 'Сделать фото';
        hintText = 'Попробуйте сделать повторный снимок';
        break;
      case BadDataType.badPassportSelfie:
        onPressed = () => Navigator.of(context, rootNavigator: false)
            .push(
                CupertinoPageRoute(builder: (_) => PassportSelfieProblemPage()))
            .then((_) => controller.requestRefresh());
        problemText = 'Фотография паспорта не соответствует требованиям';
        buttonText = 'Сделать фото';
        hintText = 'Попробуйте сделать повторный снимок';
        break;
      case BadDataType.badLicenseData:
        onPressed = () => Navigator.of(context, rootNavigator: false)
            .push(CupertinoPageRoute(builder: (_) => LicenseDataProblemPage()))
            .then((_) => controller.requestRefresh());
        problemText = 'Некорректные данные ВУ';
        buttonText = 'Ввести данные заново';
        hintText =
            'Введите данные еще раз и внимательно проверьте перед отправкой';
        break;
      case BadDataType.badLicenseFrontPhoto:
        onPressed = () => Navigator.of(context, rootNavigator: false)
            .push(CupertinoPageRoute(
                builder: (_) => LicenseFrontPhotoProblemPage()))
            .then((_) => controller.requestRefresh());
        problemText = 'Фотография ВУ не соответствует требованиям';
        buttonText = 'Сделать фото';
        hintText = 'Попробуйте сделать повторный снимок';
        break;
      case BadDataType.badLicenseBackPhoto:
        onPressed = () => Navigator.of(context, rootNavigator: false)
            .push(CupertinoPageRoute(
                builder: (_) => LicenseBackPhotoProblemPage()))
            .then((_) => controller.requestRefresh());
        problemText = 'Фотография ВУ не соответствует требованиям';
        buttonText = 'Сделать фото';
        hintText = 'Попробуйте сделать повторный снимок';
        break;
      case BadDataType.badPersonalData:
        onPressed = () => Navigator.of(context, rootNavigator: false)
            .push(CupertinoPageRoute(builder: (_) => PersonalDataProblemPage()))
            .then((_) => controller.requestRefresh());
        problemText = 'Некорректные персональные данные';
        buttonText = 'Ввести данные заново';
        hintText =
            'Введите данные еще раз и внимательно проверьте перед отправкой';
        break;
      case BadDataType.badBankData:
        onPressed = () => Navigator.of(context, rootNavigator: false)
            .push(CupertinoPageRoute(builder: (_) => BankDataProblemPage()))
            .then((_) => controller.requestRefresh());
        problemText = 'Некорректные банковские данные';
        buttonText = 'Ввести данные заново';
        hintText =
            'Введите данные еще раз и внимательно проверьте перед отправкой';
        break;
      case BadDataType.badCardData:
        onPressed = () => Navigator.of(context, rootNavigator: false)
            .push(CupertinoPageRoute(builder: (_) => CardDataProblemPage()))
            .then((_) => controller.requestRefresh());
        problemText = 'Некорректные данные карты';
        buttonText = 'Ввести данные заново';
        hintText =
            'Введите данные еще раз и внимательно проверьте перед отправкой';
        break;
      case BadDataType.badEmailData:
        onPressed = () => Navigator.of(context, rootNavigator: false)
            .push(CupertinoPageRoute(builder: (_) => EmailDataProblemPage()))
            .then((_) => controller.requestRefresh());
        problemText = 'Некорректная почта';
        buttonText = 'Ввести данные заново';
        hintText =
            'Введите данные еще раз и внимательно проверьте перед отправкой';
        break;
    }
    return Padding(
      padding: const EdgeInsets.only(left: 24, right: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            problemText,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w500,
              color: ColorStyle.wrong,
            ),
            textAlign: TextAlign.center,
          ),
          Container(
            height: orientation == Orientation.landscape ? 5 : 10,
          ),
          Text(
            hintText,
            style: TextStyle(
              fontSize: 13,
              color: ColorStyle.sideColor,
            ),
            textAlign: TextAlign.center,
          ),
          Container(
            height: orientation == Orientation.landscape ? 5 : 10,
          ),
          Button(
            onPressed: onPressed,
            child: Text(
              buttonText,
              style: TextStyle(fontSize: 13),
            ),
          ),
          Container(height: 16),
          Button(
            child: Text(
              'Выйти из аккаунта',
              style: TextStyle(
                fontSize: 13,
                color: ColorStyle.txt,
              ),
            ),
            shadow: [
              BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.05),
                blurRadius: 12,
              )
            ],
            color: ColorStyle.white2,
            onPressed: () {
              showDialog(
                context: context,
                builder: (context) => CustomCupertinoAlertDialog(
                  title: Text('Выйти из аккаунта?'),
                  actions: <Widget>[
                    CustomCupertinoDialogAction(
                      child: Text('Да'),
                      onPressed: () => Navigator.of(context).pop(true),
                    ),
                    CustomCupertinoDialogAction(
                      child: Text('Нет'),
                      onPressed: () => Navigator.of(context).pop(false),
                      isDefaultAction: true,
                    ),
                  ],
                ),
              ).then((exit) async {
                if (exit) {
                  await bloc.logout();
                  Navigator.of(context).pushAndRemoveUntil(
                    CupertinoPageRoute(
                      builder: (_) => SplashPage(),
                    ),
                    (route) => route.isFirst,
                  );
                }
              });
            },
          ),
        ],
      ),
    );
  }
}

class _CustomButton extends StatelessWidget {
  final String text;
  final Function onPressed;
  final Widget icon;
  final Widget transparentIcon;

  const _CustomButton(
      {Key key,
      @required this.text,
      this.onPressed,
      @required this.icon,
      @required this.transparentIcon})
      : super(key: key);

  final TextStyle buttonTextStyle = const TextStyle(
    color: ColorStyle.txt,
    fontSize: 13,
    fontWeight: FontWeight.w500,
  );

  @override
  Widget build(BuildContext context) {
    return Button(
      shadow: [
        BoxShadow(
          color: Color.fromRGBO(0, 0, 0, 0.05),
          blurRadius: 12,
        )
      ],
      color: ColorStyle.white2,
      textStyle: buttonTextStyle,
      onPressed: onPressed,
      child: Row(
        children: <Widget>[
          icon,
          Text(text),
          transparentIcon,
        ],
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
      ),
    );
  }
}
