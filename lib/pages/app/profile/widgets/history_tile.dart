import 'package:car_sharing_services/page/widget/button.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HistoryTileBuilder extends StatelessWidget {
  final Function onPressed;
  final String earnedToday;

  final TextStyle buttonTextStyle = const TextStyle(
    color: ColorStyle.txt,
    fontSize: 13,
    fontWeight: FontWeight.w500,
  );

  const HistoryTileBuilder({Key key, this.onPressed, this.earnedToday}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Button(
      
      shadow: [
        BoxShadow(
          color: Color.fromRGBO(0, 0, 0, 0.05),
          blurRadius: 12,
        )
      ],
      color: ColorStyle.white2,
      textStyle: buttonTextStyle,
      onPressed: onPressed,
      padding: EdgeInsets.all(15),
      child: Row(
        children: <Widget>[
          Text('История'),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                'за сегодня - ',
                style: TextStyle(
                    fontSize: 13,
                    color: ColorStyle.sideColor,
                    fontWeight: FontWeight.w500),
              ),
              Text(
                earnedToday ?? '0',
                style: TextStyle(
                    fontSize: 13,
                    color: ColorStyle.mainColor,
                    fontWeight: FontWeight.w500),
              ),
              Container(width: 5),
              Icon(
                Icons.chevron_right,
                color: ColorStyle.mainColor,
              )
            ],
          )
        ],
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
      ),
    );
  }
}
