import 'package:auto_size_text/auto_size_text.dart';
import 'package:car_sharing_services/models/worker.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:car_sharing_services/utils/date_converter.dart';
import 'package:flutter/material.dart';

class WorkerInfoWidget extends StatelessWidget {
  final Worker worker;

  const WorkerInfoWidget({
    @required this.worker,
  });

  final TextStyle _nameStyle = const TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.w600,
    color: ColorStyle.txt,
    height: 1.2,
  );

  final TextStyle _infoStyle = const TextStyle(
    fontSize: 13,
    fontWeight: FontWeight.w500,
    color: ColorStyle.sideColor,
    height: 1.85,
  );

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        AutoSizeText(_getName(), style: _nameStyle, maxLines: 1),
        Container(height: 4),
        AutoSizeText(worker?.phone ?? '', style: _infoStyle, maxLines: 1),
        AutoSizeText(dateToString(worker?.birthday ?? DateTime.now()),
            style: _infoStyle, maxLines: 1),
      ],
    );
  }

  _getName() {
    var name = worker?.name ?? '';
    var surname = worker?.surname ?? '';
    return name + ' ' + surname;
  }


}
