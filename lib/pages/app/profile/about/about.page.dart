import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AboutPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  Text head(String text) {
    return Text(
      text,
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.bold,
        color: Color(0xFF444444),
      ),
    );
  }

  Text body(String text) {
    return Text(
      text,
      style: TextStyle(
        fontSize: 15,
        color: Color(0xFF444444),
      ),
    );
  }

  Widget paragraph(Widget number, Widget text) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        number,
        Container(width: 8),
        Expanded(
          child: text,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ColorStyle.white,
        appBar: AppBar(
          title: Text('О приложении'),
          backgroundColor: Colors.transparent,
          elevation: 0.0,
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 14, 20, 24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  head('TsCars - платформа по управлению задачами\n'),
                  body(
                      '''Платформа TScars была создана для управления внутренними задачами популярного в России Каршеринга Youdrive

TSCARS- это индивидуальный (личный кабинет) каждого сотрудника , позволяющий,  при индивидуальной регистрации формировать отчеты о проделанной работе. 
'''),
                  Container(height: 16),
                  head('Инструкция:\n'),
                  body(
                      'Для того, чтобы начать работу с сервисом TSCARS :  необходимо скачать приложение AppStore или  Google Play, в зависимости от модели Вашего смартфона.\n'),
                  paragraph(
                    body('1.'),
                    body(
                      'После установки приложения - следуйте инструкции по регистрации.',
                    ),
                  ),
                  Container(height: 16),
                  paragraph(
                    body('2.'),
                    body(
                      'Обратите внимание,  чтобы все поля были заполнены, а фотографии были читаемы.',
                    ),
                  ),
                  Container(height: 16),
                  paragraph(
                    body('3.'),
                    body(
                      'Для входа в систему введите номер телефона , указанного при регистрации и дождитесь смс пароля.',
                    ),
                  ),
                  Container(height: 16),
                  paragraph(
                    body('4.'),
                    body(
                      'После входа в систему пройдите по владке—> Youdrive\nДалее выберите задачу из списка',
                    ),
                  ),
                  Container(height: 16),
                  paragraph(
                    body('5.'),
                    body(
                      'Введите номер авто,  по которому необходимо произвести работу —>',
                    ),
                  ),
                  Container(height: 16),
                  paragraph(
                    body('6.'),
                    body(
                      'Фотографируйте автомобиль до начала работ —>',
                    ),
                  ),
                  Container(height: 16),
                  paragraph(
                    body('7.'),
                    body(
                      'Фотографируйте автомобиль после завершения работ —>',
                    ),
                  ),
                  Container(height: 16),
                  paragraph(
                    body('8.'),
                    body(
                      'После завершения задач необходимо подтвердить «завершения задачи» галочкой в правом верхнем углу экрана.',
                    ),
                  ),
                  Container(height: 20),
                  Container(
                    alignment: Alignment.center,
                    child: Text(
                      'TSCARS 2020 Все права защищены!',
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontSize: 14,
                        color: ColorStyle.sideColor,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ));
  }
}
