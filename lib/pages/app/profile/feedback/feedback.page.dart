import 'dart:async';
import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:car_sharing_services/blocs/profile.bloc.dart';
import 'package:car_sharing_services/page/widget/button.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:car_sharing_services/util/photo_maker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_svg/flutter_svg.dart';

class FeedbackPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _FeedbackPageState();
}

class _FeedbackPageState extends State<FeedbackPage> {
  TextEditingController commentController;
  final FocusNode commentFocus = FocusNode();
  bool disableButton = true;
  File photo;
  ProfileBloc bloc;

  //final UserService userService = UserService();
  StreamSubscription subscription;

  @override
  void initState() {
    commentController = TextEditingController();

    subscription = KeyboardVisibility.onChange.listen((event) {
      if (!event) {
        setState(() => disableButton = false);
        commentFocus.unfocus();
      }
    });

    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc = ProfileBloc.of(context);
  }

  @override
  void dispose() {
    subscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorStyle.white,
      appBar: AppBar(
        title: Text('Обратная связь'),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: Visibility(
        visible: !disableButton,
        child: Button(
          disabled: commentController.text.isEmpty,
          margin: EdgeInsets.fromLTRB(16, 0, 16, 16),
          width: double.infinity,
          child: Text('Отправить'),
          onPressed: () async {
            //userService.sendFeedback(commentController.text, photo: photo);
            bloc.sendFeedback(
                feedback: commentController.text.trim(), photo: photo);
            Navigator.of(context).pop();
          },
        ),
      ),
      body: SafeArea(
        child: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            setState(() => disableButton = false);
            FocusScope.of(context).unfocus();
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 36),
            child: ListView(
              physics: BouncingScrollPhysics(),
              children: <Widget>[
                AutoSizeText(
                  'Опишите в деталях вашу проблему и нажмите “отправить”. '
                  'Мы позвоним вам и ответим на интересующий '
                  'вас вопрос в ближайшее время.',
                  maxLines: 4,
                  style: TextStyle(
                    color: ColorStyle.txt,
                    fontSize: 13,
                  ),
                  minFontSize: 9,
                  maxFontSize: 16,
                ),
                Container(height: 24),
                AutoSizeText(
                  'Интересующая проблема',
                  style: TextStyle(
                    fontSize: 13,
                    color: ColorStyle.sideColor,
                  ),
                  minFontSize: 9,
                  maxFontSize: 16,
                  maxLines: 1,
                ),
                Container(height: 8),
                TextField(
                  onTap: () {
                    setState(() => disableButton = true);
                  },
                  onEditingComplete: () {
                    FocusScope.of(context).unfocus();
                    setState(() => disableButton = true);
                  },
                  maxLines: 4,
                  minLines: 1,
                  scrollPhysics: BouncingScrollPhysics(),
                  keyboardType: TextInputType.multiline,
                  controller: commentController,
                  focusNode: commentFocus,
                  onChanged: (_) {},
                  cursorColor: ColorStyle.txt,
                  //autofocus: true,
                  decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: ColorStyle.sideColor,
                      ),
                    ),
                    border: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: ColorStyle.sideColor,
                      ),
                    ),
                  ),
                  style: TextStyle(
                    color: ColorStyle.txt,
                    fontSize: 18.0,
                    fontWeight: FontWeight.normal,
                  ),
                ),
                Container(height: 24),
                AutoSizeText(
                  'Прикрепить фото (не обязательно)',
                  style: TextStyle(
                    fontSize: 13,
                    color: ColorStyle.txt,
                  ),
                  minFontSize: 9,
                  maxFontSize: 16,
                  maxLines: 1,
                ),
                Container(height: 24),
                Container(
                  alignment: Alignment.centerLeft,
                  child: Button(
                    color: ColorStyle.white2,
                    //color: Colors.transparent,
                    borderRadius: 8.0,
                    shadow: [
                      BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.05),
                        blurRadius: 12,
                      )
                    ],
                    padding: EdgeInsets.zero,
                    child: Container(
                      width: 96,
                      height: 96,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        image: photo == null
                            ? null
                            : DecorationImage(
                                image: FileImage(photo),
                                fit: BoxFit.cover,
                              ),
                      ),
                      child: photo == null
                          ? Center(
                              child: SvgPicture.asset(
                                'assets/vectors/add.svg',
                              ),
                            )
                          : Container(),
                    ),
                    onPressed: () async {
                      var pic = await PhotoMaker.makePhoto(context);
                      //var bytes = pic?.readAsBytesSync();
                      if (pic != null) {
                        setState(() => photo = pic);
                      }
                    },
                  ),
                ),
                Container(height: 72),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
