import 'dart:async';
import 'dart:io';

import 'package:car_sharing_services/blocs/registration.bloc.dart';
import 'package:car_sharing_services/pages/photo/photo_review.page.dart';
import 'package:car_sharing_services/pages/photo/questions.page.dart';
import 'package:car_sharing_services/page/widget/custom_will_pop_widget.dart';
import 'package:car_sharing_services/pages/auth/code/registration/license/license_data.page.dart';
import 'package:car_sharing_services/util/photo_maker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class PassportSelfiePage extends StatefulWidget {
  PassportSelfiePage({Key key}) : super(key: key);

  @override
  _PassportSelfiePageState createState() => _PassportSelfiePageState();
}

class _PassportSelfiePageState extends State<PassportSelfiePage> {
  File image;
  final BehaviorSubject<bool> imageLoaded = BehaviorSubject();
  final String title = 'Селфи с паспортом';
  final String text =
      'Сфотографируйте себя с паспортом чтобы отчетливо было видно ваше лицо и фото в паспорте';
  final String imageStubPath = 'assets/vectors/selfie.svg';
  final List<String> questionsForReview = [
    'Ваше лицо отчетливо видно?',
    'Ваш документ отчетливо видно?',
    'Отчетливо видно серию и номер паспорта?',
    'Отчетливо видно фамилию, имя и отчество в паспорте?',
  ];

  /// need fpr rebuild widget when use retake func
  String timeKey;

  RegistrationBloc bloc;

  @override
  void initState() {
    super.initState();
    imageLoaded.add(false);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc = RegistrationBloc.of(context);
  }

  @override
  void dispose() {
    imageLoaded?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
        stream: imageLoaded,
        initialData: false,
        builder: (context, snapshot) {
          if (snapshot?.data ?? false)
            return QuestionsPage(
              key: Key(timeKey ?? 'default'),
              title: title,
              image: image,
              questions: questionsForReview,
              accept: onAccept,
              back: onBack,
              retake: onRetake,
            );
          return CustomWillPopWidget(
            child: PhotoReviewPage(
              title: title,
              text: text,
              imageStubPath: imageStubPath,
              nextFabPressed: nextFabPressed,
            ),
          );
        });
  }

  FutureOr nextFabPressed() async {
    image = await PhotoMaker.makePhoto(context);
    if (image != null) {
      timeKey = DateTime.now().toIso8601String();
      imageLoaded.add(true);
    }
  }

  onAccept() {
    /*bloc.addPhoto(image, PhotoType.passportSelfie);
    customNavigator.push(LicenseDataPage());*/
    bloc.selfWithPassport = image;
    Navigator.of(context).push(CupertinoPageRoute(
      builder: (_) => LicenseDataPage(),
    ));
    imageLoaded.add(false);
  }

  onBack() {
    imageLoaded.add(false);
    image = null;
  }

  onRetake() async {
    image = await PhotoMaker.makePhoto(context);
    if (image != null) {
      timeKey = DateTime.now().toIso8601String();
      imageLoaded.add(true);
    }
  }
}
