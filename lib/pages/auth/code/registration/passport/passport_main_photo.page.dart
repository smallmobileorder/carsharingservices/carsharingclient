import 'dart:async';
import 'dart:io';
import 'package:car_sharing_services/blocs/registration.bloc.dart';
import 'package:car_sharing_services/pages/photo/photo_review.page.dart';
import 'package:car_sharing_services/pages/photo/questions.page.dart';
import 'package:car_sharing_services/page/widget/custom_will_pop_widget.dart';
import 'package:car_sharing_services/util/photo_maker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

import 'passport_registration_photo_page.dart';

class PassportMainPhotoPage extends StatefulWidget {
  const PassportMainPhotoPage({
    Key key,
  }) : super(key: key);

  createState() => _PassportMainPhotoPageState();
}

class _PassportMainPhotoPageState extends State<PassportMainPhotoPage> {
  File image;
  final BehaviorSubject<bool> imageLoaded = BehaviorSubject();
  final String title = 'Фото разворота паспорта';
  final String text = 'Сделайте фото главного разворота паспорта';
  final String imageStubPath = 'assets/vectors/passport.svg';
  final List<String> questionsForReview = [
    'Отчетливо видно фамилию, имя, отчество?',
    'Отчетливо видно дату и место рождения?',
    'Отчетливо видно серию и номер паспорта?',
    'Отчетливо видно дату выдачи и орган, которым он выдан?',
  ];

  /// need fpr rebuild widget when use retake func
  String timeKey;

  RegistrationBloc bloc;

  @override
  void initState() {
    super.initState();
    imageLoaded.add(false);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc = RegistrationBloc.of(context);
  }

  @override
  void dispose() {
    imageLoaded?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
        stream: imageLoaded,
        initialData: false,
        builder: (context, snapshot) {
          if (snapshot?.data ?? false)
            return QuestionsPage(
              key: Key(timeKey ?? 'default'),
              title: title,
              image: image,
              questions: questionsForReview,
              accept: onAccept,
              back: onBack,
              retake: onRetake,
            );
          return CustomWillPopWidget(
            child: PhotoReviewPage(
              title: title,
              text: text,
              imageStubPath: imageStubPath,
              nextFabPressed: nextFabPressed,
            ),
          );
        });
  }

  FutureOr nextFabPressed() async {
    image = await PhotoMaker.makePhoto(context);
    if (image != null) {
      timeKey = DateTime.now().toIso8601String();
      imageLoaded.add(true);
    }
  }

  onAccept() {
    /*bloc.addPhoto(image, PhotoType.passportFirstPage);
    customNavigator.push(PassportRegistrationPhotoPage());*/
    bloc.passportFirstPage = image;
    Navigator.of(context).push(CupertinoPageRoute(
      builder: (_) => PassportRegistrationPhotoPage(),
    ));
    imageLoaded.add(false);
  }

  onBack() {
    imageLoaded.add(false);
    image = null;
  }

  onRetake() async {
    image = await PhotoMaker.makePhoto(context);
    if (image != null) {
      timeKey = DateTime.now().toIso8601String();
      imageLoaded.add(true);
    }
  }
}
