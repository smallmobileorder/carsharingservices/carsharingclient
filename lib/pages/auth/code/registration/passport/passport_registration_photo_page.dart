import 'dart:async';
import 'dart:io';

import 'package:car_sharing_services/blocs/registration.bloc.dart';
import 'package:car_sharing_services/pages/photo/photo_review.page.dart';
import 'package:car_sharing_services/pages/photo/questions.page.dart';
import 'package:car_sharing_services/page/widget/custom_will_pop_widget.dart';
import 'package:car_sharing_services/util/photo_maker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

import 'passport_selfie.page.dart';

class PassportRegistrationPhotoPage extends StatefulWidget {
  PassportRegistrationPhotoPage({Key key}) : super(key: key);

  @override
  _PassportRegistrationPhotoPageState createState() =>
      _PassportRegistrationPhotoPageState();
}

class _PassportRegistrationPhotoPageState
    extends State<PassportRegistrationPhotoPage> {
  File image;
  final BehaviorSubject<bool> imageLoaded = BehaviorSubject();
  final String title = 'Фото адреса регистрации';
  final String text =
      'Сделайте фото разворота паспорта со штампом об актуальной регистрации';
  final String imageStubPath = 'assets/vectors/open_passport.svg';
  final List<String> questionsForReview = [
    'Отчетливо видно серию и номер паспорта?',
    'Отчетливо видно адрес и дату регистрации?',
  ];

  /// need fpr rebuild widget when use retake func
  String timeKey;

  RegistrationBloc bloc;

  @override
  void initState() {
    super.initState();
    imageLoaded.add(false);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc = RegistrationBloc.of(context);
  }

  @override
  void dispose() {
    imageLoaded?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
        stream: imageLoaded,
        initialData: false,
        builder: (context, snapshot) {
          if (snapshot?.data ?? false)
            return QuestionsPage(
              key: Key(timeKey),
              title: title,
              image: image,
              questions: questionsForReview,
              accept: onAccept,
              back: onBack,
              retake: onRetake,
            );
          return CustomWillPopWidget(
            child: PhotoReviewPage(
              title: title,
              text: text,
              imageStubPath: imageStubPath,
              nextFabPressed: nextFabPressed,
            ),
          );
        });
  }

  FutureOr nextFabPressed() async {
    image = await PhotoMaker.makePhoto(context);
    if (image != null) {
      timeKey = DateTime.now().toIso8601String();
      imageLoaded.add(true);
    }
  }

  onAccept() {
    /*bloc.addPhoto(image, PhotoType.passportRegPage);
    customNavigator.push(PassportSelfiePage());*/
    bloc.passportRegistrationPage = image;
    Navigator.of(context).push(CupertinoPageRoute(
      builder: (_) => PassportSelfiePage(),
    ));
    imageLoaded.add(false);
  }

  onBack() {
    imageLoaded.add(false);
    image = null;
  }

  onRetake() async {
    image = await PhotoMaker.makePhoto(context);
    if (image != null) {
      timeKey = DateTime.now().toIso8601String();
      imageLoaded.add(true);
    }
  }
}
