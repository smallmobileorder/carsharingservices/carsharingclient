import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';

import 'custom_checkbox.dart';

class AgreementAccept extends StatefulWidget {
  final ValueChanged<bool> checkAgreement;
  final ValueChanged<bool> checkExperience;
  final ValueChanged<bool> checkPersonal;

  const AgreementAccept(
      {Key key,
      @required this.checkAgreement,
      @required this.checkExperience,
      @required this.checkPersonal})
      : super(key: key);

  createState() => _AgreementAcceptState();
}

class _AgreementAcceptState extends State<AgreementAccept> {
  bool agreementChecked;
  bool experienceChecked;
  bool personalChecked;

  @override
  void initState() {
    agreementChecked = false;
    experienceChecked = false;
    personalChecked = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          border: Border.all(
              color: ColorStyle.mainColor, width: 2, style: BorderStyle.solid)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          CustomCheckbox(
            text: 'Я ознакомился и принимаю условия договора',
            value: agreementChecked,
            onChecked: widget.checkAgreement,
          ),
          Container(
            height: 8,
          ),
          CustomCheckbox(
            text: 'Мой стаж вождения больше 2х лет',
            value: experienceChecked,
            onChecked: widget.checkExperience,
          ),
          Container(
            height: 8,
          ),
          CustomCheckbox(
            text: 'Я согласен на обработку моих персональных данных',
            value: personalChecked,
            onChecked: widget.checkPersonal,
          ),
        ],
      ),
    );
  }
}
