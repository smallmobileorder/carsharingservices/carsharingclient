import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomCheckbox extends StatefulWidget {
  final ValueKey<bool> key;
  final ValueChanged<bool> onChecked;
  final bool value;
  final String text;

  CustomCheckbox({
    this.key,
    @required this.onChecked,
    @required this.value,
    @required this.text,
  });

  @override
  createState() => _CustomCheckboxState();
}

class _CustomCheckboxState extends State<CustomCheckbox> {
  bool _value;

  @override
  void initState() {
    _value = widget.value ?? false;
    super.initState();
  }

  void _check() {
    final newValue = !_value;
    setState(() => _value = newValue);
    widget.onChecked(newValue);
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          height: 16,
          width: 16,
          decoration: BoxDecoration(
              border: Border.all(color: ColorStyle.sideColor, width: 1),
              borderRadius: BorderRadius.circular(2)),
          child: Theme(
            data: ThemeData(
              unselectedWidgetColor: Colors.transparent,
            ),
            child: Checkbox(
              onChanged: (checked) => _check(),
              value: _value,
              checkColor: ColorStyle.mainColor,
              activeColor: Colors.transparent,
            ),
          ),
        ),
        Container(width: 8),
        Expanded(
          child: InkWell(
            onTap: () => _check(),
            child: Container(
              alignment: Alignment.centerLeft,
              height: 16,
              child: Text(
                widget.text,
                textAlign: TextAlign.start,
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 12,
                    color: ColorStyle.txt),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
