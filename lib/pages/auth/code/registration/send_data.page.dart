import 'dart:async';

import 'package:car_sharing_services/blocs/auth.bloc.dart';
import 'package:car_sharing_services/blocs/registration.bloc.dart';
import 'package:car_sharing_services/page/widget/loading_container.dart';
import 'package:car_sharing_services/page/widget/next_fab.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:car_sharing_services/service/storage/storage_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

class SendDataPage extends StatefulWidget {
  SendDataPage({Key key}) : super(key: key);

  @override
  _SendDataPageState createState() => _SendDataPageState();
}

class _SendDataPageState extends State<SendDataPage> {
  final BehaviorSubject<bool> isLoading = BehaviorSubject();
  final BehaviorSubject<bool> isError = BehaviorSubject();

  String error;

  @override
  void initState() {
    super.initState();
    sendData();
  }

  @override
  void dispose() {
    print('disposing SendDataPage');
    isLoading?.close();
    isError?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double containerDimension =
        MediaQuery.of(context).size.height > MediaQuery.of(context).size.width
            ? MediaQuery.of(context).size.width / 4
            : MediaQuery.of(context).size.height / 4;
    return WillPopScope(
      onWillPop: () async {
        if (isLoading?.value ?? true == true) {
          return Future.value(false);
        } else {
          return Future.value(true);
        }
      },
      child: Scaffold(
        backgroundColor: ColorStyle.backgroundColor,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          automaticallyImplyLeading: false,
        ),
        body: Padding(
          padding: const EdgeInsets.only(left: 36, right: 36, bottom: 70),
          child: Center(
            child: StreamBuilder<bool>(
                initialData: true,
                stream: isLoading.stream,
                builder: (context, loading) {
                  return StreamBuilder<bool>(
                      initialData: false,
                      stream: isError.stream,
                      builder: (context, error) {
                        if (!loading.data && !error.data)
                          return successWidget(containerDimension);
                        if (!loading.data && error.data)
                          return errorWidget(containerDimension);
                        return loadingWidget(containerDimension);
                      });
                }),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: StreamBuilder<bool>(
            stream: isError.stream,
            builder: (context, snapshot) {
              if (snapshot?.data ?? false)
                return NextFab(
                  text: 'Повторить запрос',
                  blockedStream: Future.value(false).asStream(),
                  nextFabPressed: sendData,
                );
              return Container();
            }),
      ),
    );
  }

  Widget loadingWidget(double containerDimension) {
    return Hero(
      tag: 'error',
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
                height: containerDimension,
                width: containerDimension,
                child: LoadingContainer(
                  strokeWidth: 4,
                )),
            Container(
              height: 70,
            ),
            Container(
              child: Text(
                'Отправка заявки на регистрацию',
                style: TextStyle(
                    fontSize: 18,
                    color: ColorStyle.txt,
                    fontWeight: FontWeight.w600),
                textAlign: TextAlign.center,
              ),
            ),
          ]),
    );
  }

  Widget successWidget(double containerDimension) {
    return Hero(
      tag: 'error',
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
                height: containerDimension,
                width: containerDimension,
                child: SvgPicture.asset('assets/vectors/check.svg')),
            Container(
              height: 70,
            ),
            Container(
              child: Text(
                'Заявка на регистрацию отправлена!',
                style: TextStyle(
                    fontSize: 18,
                    color: ColorStyle.txt,
                    fontWeight: FontWeight.w600),
                textAlign: TextAlign.center,
              ),
            ),
          ]),
    );
  }

  Widget errorWidget(double containerDimension) {
    return Hero(
      tag: 'error',
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
                height: containerDimension,
                width: containerDimension,
                child: SvgPicture.asset('assets/vectors/error.svg')),
            Container(
              height: 70,
            ),
            Container(
              child: Text(
                error ?? 'Произошла ошибка при отправке',
                style: TextStyle(
                    fontSize: 18,
                    color: ColorStyle.txt,
                    fontWeight: FontWeight.w600),
                textAlign: TextAlign.center,
              ),
            ),
          ]),
    );
  }

  Future<void> sendData() async {
    /*isLoading.add(true);
    isError.add(false);
    regBloc.sendRegistrationForm().then((answer) {
      if (answer == null) {
        isLoading.add(false);
        coreBloc.finishRegistration();
      } else {
        error = answer;
        isError.add(true);
        isLoading.add(false);
      }
    });*/
    print('trying to send data');
    isLoading.add(true);
    isError.add(false);

    try {
      var uid = await StorageService.getFirebaseId();
      var token = await StorageService.getFirebaseToken();

      //await RegistrationBloc.of(context).register(uid, token);
      var bloc = Provider.of<RegistrationBloc>(context, listen: false);
      print(bloc.name);
      print(bloc.surname);
      print(bloc.secondName);
      print(bloc.male);
      print(bloc.mail);
      print(bloc.birthday ?? 'no birthday');
      print(bloc.phone);
      print(bloc.workerAccount?.toJson() ?? 'no account');
      print(bloc.workerCard?.toJson() ?? 'no card');
      print(bloc.workerLicense?.toJson() ?? 'no license');
      print(bloc.workerPassport?.toJson() ?? 'no passport');
      print(bloc.uid);
      await bloc.register(uid, token);

      Provider.of<AuthBloc>(context, listen: false)
          .authState
          .add(AuthState.LOGGED);
      isLoading.add(false);
    } catch (e) {
      print('Error: $e');
      isError.add(true);
      isLoading.add(false);
    }
    /*regBloc.sendRegistrationForm().then((answer) {
      if (answer == null) {
        isLoading.add(false);
        coreBloc.finishRegistration();
      } else {
        error = answer;
        isError.add(true);
        isLoading.add(false);
      }
    });*/
  }
}
