import 'dart:async';

import 'package:car_sharing_services/blocs/auth.bloc.dart';
import 'package:car_sharing_services/page/widget/custom_will_pop_widget.dart';
import 'package:car_sharing_services/page/widget/next_fab.dart';
import 'package:car_sharing_services/pages/splash.page.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

import 'personal_data.page.dart';
import 'widget/agreement_accept.dart';

class AgreementPage extends StatefulWidget {
  createState() => _AgreementPageState();
}

class _AgreementPageState extends State<AgreementPage> {
  bool agreement;
  bool experience;
  bool personal;
  BehaviorSubject<bool> blocked = BehaviorSubject();

  @override
  void initState() {
    agreement = false;
    experience = false;
    personal = false;
    blocked.add(!(agreement && experience && personal));
    super.initState();
  }

  @override
  void dispose() {
    blocked?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomWillPopWidget(
      dialogContent: 'Вам придется заново подтвердить номер телефона',
      navigatorFunction: () {
        Navigator.of(context).pushAndRemoveUntil(
            CupertinoPageRoute(builder: (_) => SplashPage()), (_) => false);
        Provider.of<AuthBloc>(context, listen: false)
            .authState
            .add(AuthState.NEED_AUTH);
      },
      child: Scaffold(
          backgroundColor: ColorStyle.backgroundColor,
          appBar: AppBar(
            centerTitle: true,
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            title: Text('Договор'),
          ),
          body: Padding(
            padding: const EdgeInsets.only(bottom: 65),
            child: SingleChildScrollView(
              padding: EdgeInsets.only(top: 24),
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: 16, right: 16),
                    child: Text(
                        '''Индивидуальный предприниматель Якущенко Яна Михайловна, именуемая в дальнейшем «ЗАКАЗЧИК» и гражданин Российской Федерации Именуемый в дальнейшем «ИСПОЛНИТЕЛЬ», действующий от своего имени с другой стороны, совместно именуемые «Стороны», заключили настоящий Договор о нижеследующем

	•	По настоящему договору Исполнитель обязуется по заданию заказчика оказать следующие услуги – перегнать автомобиль в пределах города Москвы и московской области из одного местонахождения в другое, а Заказчик оплатить эту работу в порядке, предусмотренном настоящим договором.

	•	Исполнитель обязан осуществлять перегон автомобиля в указанное время с момента получения поручения об этом.

	•	За выполнение услуг, указанных в пункте 1.1 настоящего договора, Заказчик обязуется уплатить Исполнителю вознаграждение согласно прайс листа, указанного в дополнительном соглашении за каждый автомобиль.

	•	Исполнителю открывается доступ в личный кабинет в YouDrive (диспетчерская) – система управления процессами (https://control-room.youdrive.today), для осуществления открытия – закрытия автомобиля и его перегона в пункт назначения, передача доступа третьим лицам категорически запрещена, в противном случае настоящий договор расторгается в одностороннем порядке по инициативе Заказчика.

	•	Исполнителю открывается доступ в личный кабинет в приложение TSCars – система управления взаиморасчетами и информированием.   
                                                                                                                                                 
	•	Исполнитель обязуется:
1.6.1 – перегнать автомобиль в оговорённые сроки и места, согласно диспетчерской YouDrive
1.6.2 – информировать Заказчика о возникших во время перегона автомобиля, незапланированных ситуациях, таких, например, как авария, поломка, стихийные бедствия и пр.
1.6.3 – выполнять все необходимые действия (согласно инструкции водителя – перегонщика) для сохранности перегоняемого автомобиля. В случае неисполнения несет материальную ответственность, согласно пункту 1.6.4
1.6.4 - Ведет контроль качества выполненных услуг автомойки от имени заказчика, закрытие задачи грязного автомобиля (кузов, салон) – штраф 2000 руб.
- После завершения задачи проверить двери, окна, световые приборы, оставление открытых дверей, окон, световых приборов – штраф 3000 руб.
- Забытое завершение задачи – штраф 3000 руб.
- Оставление автомобиля за зеленой зоной – штраф 3000 руб.
-Завершение задачи на закрытой территории – штраф 2000 руб. 
- Использование автомобиля в личных целях - штраф 5000 руб., повторное – увольнение.
-Курение в автомобиле – штраф 3000 руб.
- Парковка не в положенном месте + эвакуация + простой автомобиля – штраф 2000 руб. + оплата всех расходов, оплаченных Заказчиком.
- Исполнитель должен оказать услуги, предусмотренные настоящим договором лично, в противном случае настоящий договор расторгается в одностороннем порядке по инициативе Заказчика.
1.7. Исполнитель несет полную материальную ответственность в соответствии с пунктом 1.1., за возникшие по его вине во время перегона автомобиля утрату, порчу или гибель автомобиля.
1.8. Ответственность за все административные штрафы, наложенные на исполнителя во время перегона автомобиля, несет Исполнитель
1.9. Исполнитель в праве отказаться от исполнения обязательств по настоящему договору при условии полного возмещения убытков Заказчику.
1.10. Заказчик может в любое время до сдачи ему работы отказаться от договора подряда, уплатив исполнителю часть установленной цены за работу.
1.11. Договор составлен в 2-х экземплярах, по одному для каждой из сторон, имеет равную юридическую силу, сторонам понятно содержание всех пунктов данного договора и действует с момента подписания по 31.12.2020
1.12. Адреса и реквизиты сторон:

  «ЗАКАЗЧИК»
  Индивидуальный предприниматель         
  Якущенко Яна Михайловна       
  ИНН                           
  261202804730                                                                                                           
  Счёт (₽)                         
  40802810105500005655                                                               
  Банк получателя                                                                         
  ТОЧКА ПАО БАНКА "ФК ОТКРЫТИЕ"
  БИК
  044525999
  Корр. счёт
  30101810845250000999
'''),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: AgreementAccept(
                      checkPersonal: (p) => checkAll(personal: p),
                      checkAgreement: (a) => checkAll(agreement: a),
                      checkExperience: (e) => checkAll(experience: e),
                    ),
                  ),
                ],
              ),
            ),
          ),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
          floatingActionButton: NextFab(
            blockedStream: blocked.stream,
            text: 'Далее',
            nextFabPressed: nextFabPressed,
          )),
    );
  }

  FutureOr nextFabPressed() {
    Navigator.of(context)
        .push(CupertinoPageRoute(builder: (_) => PersonalDataPage()));
  }

  void checkAll({bool agreement, bool experience, bool personal}) {
    if (agreement != null) this.agreement = agreement;
    if (experience != null) this.experience = experience;
    if (personal != null) this.personal = personal;
    blocked.add(!(this.experience && this.personal && this.agreement));
  }
}
