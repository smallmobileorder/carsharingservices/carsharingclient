import 'dart:async';
import 'dart:io';
import 'package:car_sharing_services/blocs/registration.bloc.dart';
import 'package:car_sharing_services/pages/photo/photo_review.page.dart';
import 'package:car_sharing_services/pages/photo/questions.page.dart';
import 'package:car_sharing_services/page/widget/custom_will_pop_widget.dart';
import 'package:car_sharing_services/util/photo_maker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

import 'license_back_photo_page.dart';

class LicenseFrontPhotoPage extends StatefulWidget {
  const LicenseFrontPhotoPage({
    Key key,
  }) : super(key: key);

  createState() => _LicenseFrontPhotoPageState();
}

class _LicenseFrontPhotoPageState extends State<LicenseFrontPhotoPage> {
  File image;
  final BehaviorSubject<bool> imageLoaded = BehaviorSubject();
  final String title = 'Фото лицевой стороны ВУ';
  final String text =
      'Сделайте фото лицевой стороны вашего водительского удостоверения';
  final String imageStubPath = 'assets/vectors/drive_license_front.svg';
  final List<String> questionsForReview = [
    'Отчетливо видно фамилию и имя?',
    'Отчетливо видно дату и место рождения?',
    'Отчетливо видно серию и номер удостоверения?',
    'Отчетливо видно дату выдачи удостоверения?',
  ];

  /// need fpr rebuild widget when use retake func
  String timeKey;

  RegistrationBloc bloc;

  @override
  void initState() {
    super.initState();
    imageLoaded.add(false);
  }

  @override
  void didChangeDependencies() {
    bloc = RegistrationBloc.of(context);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    imageLoaded?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
        stream: imageLoaded,
        initialData: false,
        builder: (context, snapshot) {
          if (snapshot?.data ?? false)
            return QuestionsPage(
              key: Key(timeKey ?? 'default'),
              title: title,
              image: image,
              questions: questionsForReview,
              accept: onAccept,
              back: onBack,
              retake: onRetake,
            );
          return CustomWillPopWidget(
            dialogContent: 'Вернуться к заполнению данных ВУ',
            child: PhotoReviewPage(
              title: title,
              text: text,
              imageStubPath: imageStubPath,
              nextFabPressed: nextFabPressed,
            ),
          );
        });
  }

  FutureOr nextFabPressed() async {
    image = await PhotoMaker.makePhoto(context);
    if (image != null) {
      timeKey = DateTime.now().toIso8601String();
      imageLoaded.add(true);
    }
  }

  onAccept() {
    /*bloc.addPhoto(image, PhotoType.licenseFront);
    customNavigator.push(LicenseBackPhotoPage());*/
    bloc.licenseFront = image;
    Navigator.of(context).push(CupertinoPageRoute(
      builder: (_) => LicenseBackPhotoPage(),
    ));
    imageLoaded.add(false);
  }

  onBack() {
    imageLoaded.add(false);
    image = null;
  }

  onRetake() async {
    image = await PhotoMaker.makePhoto(context);
    if (image != null) {
      timeKey = DateTime.now().toIso8601String();
      imageLoaded.add(true);
    }
  }
}
