import 'dart:async';

import 'package:car_sharing_services/blocs/registration.bloc.dart';
import 'package:car_sharing_services/models/worker.dart';
import 'package:car_sharing_services/page/widget/custom_text_field.dart';
import 'package:car_sharing_services/page/widget/custom_will_pop_widget.dart';
import 'package:car_sharing_services/page/widget/next_fab.dart';
import 'package:car_sharing_services/pages/auth/code/registration/widget/custom_checkbox.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:rxdart/rxdart.dart';

import 'license_front_photo.page.dart';

class LicenseDataPage extends StatefulWidget {
  LicenseDataPage({Key key}) : super(key: key);

  @override
  _LicenseDataPageState createState() => _LicenseDataPageState();
}

class _LicenseDataPageState extends State<LicenseDataPage> {
  final validateFormKey = GlobalKey<FormState>();
  final BehaviorSubject<bool> blocked = BehaviorSubject();
  final BehaviorSubject<bool> visible = BehaviorSubject();
  final TextEditingController serialController = TextEditingController();
  final TextEditingController numberController = TextEditingController();
  final TextEditingController startDateController = TextEditingController();
  final TextEditingController endDateController = TextEditingController();
  final FocusNode serialFocus = FocusNode();
  final FocusNode numberFocus = FocusNode();
  bool twoYearsExperience;

  RegistrationBloc bloc;
  StreamSubscription subscription;

  @override
  void initState() {
    super.initState();
    serialController.addListener(checkLicenseSerial);
    numberController.addListener(checkLicenseNumber);
    serialController.addListener(checkControllers);
    numberController.addListener(checkControllers);
    startDateController.addListener(checkControllers);
    endDateController.addListener(checkControllers);
    twoYearsExperience = false;

    subscription = KeyboardVisibility.onChange.listen((event) {
      if (!event) {
        visible.add(true);
        serialFocus.unfocus();
        numberFocus.unfocus();
      } else {
        visible.add(false);
      }
    });
  }

  @override
  void didChangeDependencies() {
    bloc = RegistrationBloc.of(context);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    visible?.close();
    blocked?.close();
    serialController.dispose();
    numberController.dispose();
    serialFocus
      ..unfocus()
      ..dispose();
    numberFocus
      ..unfocus()
      ..dispose();
    subscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomWillPopWidget(
      child: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Scaffold(
              backgroundColor: ColorStyle.backgroundColor,
              appBar: AppBar(
                centerTitle: true,
                elevation: 0,
                backgroundColor: Colors.transparent,
                title: Text(
                  'Водительское удостоверение',
                  maxLines: 2,
                ),
              ),
              body: SingleChildScrollView(
                padding: const EdgeInsets.only(left: 36, right: 36, bottom: 70),
                child: Form(
                  key: validateFormKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 24,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: CustomTextField(
                              label: 'Серия',
                              controller: serialController,
                              focusNode: serialFocus,
                              keyboardType: TextInputType.multiline,
                              validator: licenseSerialValidator,
                            ),
                          ),
                          Container(
                            width: 24,
                          ),
                          Expanded(
                            flex: 2,
                            child: CustomTextField(
                              label: 'Номер',
                              controller: numberController,
                              focusNode: numberFocus,
                              keyboardType: TextInputType.number,
                              validator: licenseNumberValidator,
                            ),
                          ),
                        ],
                      ),
                      Container(
                        height: 24,
                      ),
                      CustomTextField(
                        label: 'Дата выдачи (день, месяц, год)',
                        controller: startDateController,
                        datePickMode: true,
                        onTap: chooseStartDate,
                      ),
                      Container(
                        height: 24,
                      ),
                      CustomTextField(
                        label: 'Действительно до (день, месяц, год)',
                        controller: endDateController,
                        datePickMode: true,
                        onTap: chooseEndDate,
                      ),
                      Container(
                        height: 24,
                      ),
                      CustomCheckbox(
                        text: 'Категория В больше 2х лет',
                        value: twoYearsExperience,
                        onChecked: (v) {
                          twoYearsExperience = v;
                          checkControllers();
                        },
                      ),
                    ],
                  ),
                ),
              ),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerDocked,
              floatingActionButton: StreamBuilder<bool>(
                  stream: visible.stream,
                  builder: (context, snapshot) {
                    return Visibility(
                      visible: snapshot?.data ?? true,
                      child: NextFab(
                        blockedStream: blocked.stream,
                        text: 'Все заполнено',
                        nextFabPressed: nextFabPressed,
                      ),
                    );
                  }))),
    );
  }

  FutureOr nextFabPressed() {
    if (validateFormKey.currentState.validate()) {
      /*logger.i('good data');
      bloc.addLicenseData(LicenseData(
        serial: serialController.text.trim(),
        number: numberController.text.trim(),
        startDate: startDateController.text,
        endDate: endDateController.text,
      ));
      customNavigator.push(LicenseFrontPhotoPage());*/
      bloc.workerLicense = WorkerLicense(
        serialController.text.trim(),
        numberController.text.trim(),
        startDateController.text,
        endDateController.text.trim(),
      );
      Navigator.of(context).push(CupertinoPageRoute(
        builder: (_) => LicenseFrontPhotoPage(),
      ));
    }
  }

  void checkControllers() {
    if ((serialController.text?.isEmpty ?? true) ||
        (numberController.text?.isEmpty ?? true) ||
        (endDateController.text?.isEmpty ?? true) ||
        (startDateController.text?.isEmpty ?? true) ||
        !(twoYearsExperience ?? false)) {
      blocked.add(true);
    } else {
      blocked.add(false);
    }
  }

  chooseStartDate() async {
    var now = DateTime.now();
    DateTime temp = await showDatePicker(
        helpText: "ВЫБЕРИТЕ ДАТУ",
        context: context,
        initialDate: DateTime(now.year, now.month, now.day),
        firstDate: DateTime(now.year - 10, now.month, now.day),
        lastDate: DateTime(now.year, now.month, now.day));
    if (temp != null) {
      await initializeDateFormatting("ru_RU", null);
      startDateController.text = DateFormat.yMd('ru').format(temp);
    }
  }

  chooseEndDate() async {
    var now = DateTime.now();
    DateTime temp = await showDatePicker(
        helpText: "ВЫБЕРИТЕ ДАТУ",
        context: context,
        initialDate: DateTime(now.year, now.month, now.day + 7),
        firstDate: DateTime(now.year, now.month, now.day + 7),
        lastDate: DateTime(now.year + 10, now.month, now.day));
    if (temp != null) {
      await initializeDateFormatting("ru_RU", null);
      endDateController.text = DateFormat.yMd('ru').format(temp);
    }
  }

  String licenseNumberValidator(String text) {
    if (text?.trim()?.isNotEmpty ?? false) {
      var temp = text.trim();
      if (temp.length == 6 && RegExp(r'\d\d\d\d\d\d').hasMatch(temp)) {
        return null;
      }
    }
    return 'Номер ВУ введен неверно';
  }

  String licenseSerialValidator(String text) {
    if (text?.trim()?.isNotEmpty ?? false) {
      var temp = text.trim();
      if (temp.length == 4 && RegExp(r'\d\d[а-яА-Я0-9]{2}').hasMatch(temp)) {
        return null;
      }
    }
    return 'Серия ВУ введена неверно';
  }

  void checkLicenseSerial() {
    if (serialController.text.length > 4) {
      var temp = serialController.selection.extentOffset;
      serialController.value = TextEditingValue(
        text: serialController.text.substring(0, 4),
        selection: TextSelection.fromPosition(
          TextPosition(offset: temp > 4 ? 4 : temp),
        ),
      );
    }
  }

  void checkLicenseNumber() {
    if (numberController.text.length > 6) {
      var temp = numberController.selection.extentOffset;
      numberController.value = TextEditingValue(
        text: numberController.text.substring(0, 6),
        selection: TextSelection.fromPosition(
          TextPosition(offset: temp > 6 ? 6 : temp),
        ),
      );
    }
  }
}
