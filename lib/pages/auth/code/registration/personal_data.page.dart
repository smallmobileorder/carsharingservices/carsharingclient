import 'dart:async';

import 'package:car_sharing_services/blocs/auth.bloc.dart';
import 'package:car_sharing_services/blocs/registration.bloc.dart';
import 'package:car_sharing_services/page/widget/custom_radio.dart';
import 'package:car_sharing_services/page/widget/custom_text_field.dart';
import 'package:car_sharing_services/page/widget/custom_will_pop_widget.dart';
import 'package:car_sharing_services/page/widget/next_fab.dart';
import 'package:car_sharing_services/pages/auth/code/registration/passport/passport_data.page.dart';
import 'package:car_sharing_services/pages/splash.page.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

enum Gender { male, female }

class PersonalDataPage extends StatefulWidget {
  createState() => _PersonalDataPageState();
}

class _PersonalDataPageState extends State<PersonalDataPage> {
  final validateFormKey = GlobalKey<FormState>();
  final BehaviorSubject<bool> blocked = BehaviorSubject();
  final BehaviorSubject<bool> visible = BehaviorSubject();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController surnameController = TextEditingController();
  final TextEditingController secondNameController = TextEditingController();
  final TextEditingController dateController = TextEditingController();
  final FocusNode nameFocus = FocusNode();
  final FocusNode surnameFocus = FocusNode();
  final FocusNode secondNameFocus = FocusNode();

  final BehaviorSubject<Gender> gender = BehaviorSubject();
  StreamSubscription subscription;
  RegistrationBloc bloc;
  StreamSubscription keyboardSubscription;

  @override
  void initState() {
    super.initState();
    nameController.addListener(checkControllers);
    secondNameController.addListener(checkControllers);
    surnameController.addListener(checkControllers);
    dateController.addListener(checkControllers);
    subscription = gender.listen((_) => checkControllers());

    keyboardSubscription = KeyboardVisibility.onChange.listen((event) {
      if (!event) {
        visible.add(true);
        nameFocus.unfocus();
        surnameFocus.unfocus();
        secondNameFocus.unfocus();
      } else {
        visible.add(false);
      }
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc = RegistrationBloc.of(context);
  }

  @override
  void dispose() {
    visible?.close();
    blocked?.close();
    gender?.close();
    nameController.dispose();
    secondNameController.dispose();
    surnameController.dispose();
    nameFocus.dispose();
    secondNameFocus.dispose();
    surnameFocus.dispose();
    subscription?.cancel();
    keyboardSubscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomWillPopWidget(
      dialogContent: 'Вам придется заново подтвердить номер телефона',
      navigatorFunction: () {
        Navigator.of(context).pushAndRemoveUntil(
          CupertinoPageRoute(
            builder: (_) => SplashPage(),
          ),
          (_) => false,
        );
        Provider.of<AuthBloc>(context, listen: false)
            .authState
            .add(AuthState.NEED_AUTH);
      },
      child: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Scaffold(
              backgroundColor: ColorStyle.backgroundColor,
              appBar: AppBar(
                centerTitle: true,
                elevation: 0,
                backgroundColor: Colors.transparent,
                title: Text('Персональные данные'),
              ),
              body: SingleChildScrollView(
                padding: const EdgeInsets.only(left: 36, right: 36, bottom: 70),
                child: Form(
                  key: validateFormKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 24,
                      ),
                      CustomTextField(
                        label: 'Имя',
                        controller: nameController,
                        focusNode: nameFocus,
                      ),
                      Container(
                        height: 24,
                      ),
                      CustomTextField(
                        label: 'Фамилия',
                        controller: surnameController,
                        focusNode: surnameFocus,
                      ),
                      Container(
                        height: 24,
                      ),
                      CustomTextField(
                        label: 'Отчество (если есть)',
                        controller: secondNameController,
                        focusNode: secondNameFocus,
                        validator: (f) {
                          return null;
                        },
                      ),
                      Container(
                        height: 24,
                      ),
                      CustomTextField(
                        label: 'Дата рождения (день, месяц, год)',
                        controller: dateController,
                        datePickMode: true,
                        onTap: () => chooseDate(context),
                      ),
                      Container(
                        height: 24,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text('Пол',
                              style: TextStyle(
                                  color: ColorStyle.txt, fontSize: 13)),
                          StreamBuilder<Gender>(
                              stream: gender.stream,
                              builder: (context, snapshot) {
                                return Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Expanded(
                                      child: InkWell(
                                        onTap: () => gender.add(Gender.male),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: <Widget>[
                                            CustomRadio<Gender>(
                                                materialTapTargetSize:
                                                    MaterialTapTargetSize
                                                        .shrinkWrap,
                                                activeColor:
                                                    ColorStyle.mainColor,
                                                value: Gender.male,
                                                groupValue: snapshot?.data,
                                                onChanged: (value) =>
                                                    gender.add(value)),
                                            Container(
                                              width: 8,
                                            ),
                                            Text(
                                              'Мужской',
                                              style: TextStyle(
                                                  color: ColorStyle.txt,
                                                  fontSize: 13),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: InkWell(
                                        onTap: () => gender.add(Gender.female),
                                        child: Row(
                                          children: <Widget>[
                                            CustomRadio<Gender>(
                                                materialTapTargetSize:
                                                    MaterialTapTargetSize
                                                        .shrinkWrap,
                                                activeColor:
                                                    ColorStyle.mainColor,
                                                value: Gender.female,
                                                groupValue: snapshot?.data,
                                                onChanged: (value) =>
                                                    gender.add(value)),
                                            Container(
                                              width: 8,
                                            ),
                                            Text('Женский',
                                                style: TextStyle(
                                                    color: ColorStyle.txt,
                                                    fontSize: 13)),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                );
                              })
                        ],
                      ),
                      Container(
                        color: Colors.transparent,
                      ),
                    ],
                  ),
                ),
              ),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerDocked,
              floatingActionButton: StreamBuilder<bool>(
                  stream: visible.stream,
                  builder: (context, snapshot) {
                    return Visibility(
                      visible: snapshot?.data ?? true,
                      child: NextFab(
                        blockedStream: blocked.stream,
                        text: 'Далее',
                        nextFabPressed: nextFabPressed,
                      ),
                    );
                  }))),
    );
  }

  FutureOr nextFabPressed() {
    if (validateFormKey.currentState.validate()) {
      /* bloc.addPersonalData(PersonalData(
          name: nameController.text.trim(),
          surname: surnameController.text.trim(),
          secondName: secondNameController?.text?.trim(),
          date: dateController.text,
          gender: gender.value.toString()));*/
      //customNavigator.push(PassportDataPage());
      bloc.name = nameController.text.trim();
      bloc.surname = surnameController.text.trim();
      bloc.secondName = secondNameController.text.trim();
      bloc.birthday = dateController.text;
      bloc.male = gender.value == Gender.male;

      Navigator.of(context).push(CupertinoPageRoute(
        builder: (_) => PassportDataPage(),
      ));
    }
  }

  void checkControllers() {
    if ((nameController.text?.isEmpty ?? true) ||
        (surnameController.text?.isEmpty ?? true) ||
        (dateController.text?.isEmpty ?? true) ||
        (!(gender?.hasValue ?? false))) {
      blocked.add(true);
    } else {
      blocked.add(false);
    }
  }

  chooseDate(BuildContext context) async {
    var now = DateTime.now();
    DateTime temp = await showDatePicker(
      helpText: "ВЫБЕРИТЕ ДАТУ",
      context: context,
      initialDate: DateTime(now.year - 20, now.month, now.day),
      firstDate: DateTime(now.year - 100, now.month, now.day),
      lastDate: DateTime(now.year - 20, now.month, now.day),
    );
    if (temp != null) {
      await initializeDateFormatting("ru_Ru", null);
      dateController.text = DateFormat.yMd('ru').format(temp);
    }
  }
}
