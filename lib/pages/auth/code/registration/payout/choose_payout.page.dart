import 'dart:async';

import 'package:car_sharing_services/page/widget/custom_radio.dart';
import 'package:car_sharing_services/page/widget/custom_will_pop_widget.dart';
import 'package:car_sharing_services/page/widget/loading_image_widget.dart';
import 'package:car_sharing_services/page/widget/next_fab.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

import 'bank_data.page.dart';
import 'card_data.page.dart';

class ChoosePayoutPage extends StatefulWidget {
  createState() => _ChoosePayoutPageState();
}

class _ChoosePayoutPageState extends State<ChoosePayoutPage> {
  final BehaviorSubject<bool> payoutToCard = BehaviorSubject();

  @override
  void initState() {
    super.initState();
    payoutToCard.add(true);
  }

  @override
  void dispose() {
    payoutToCard?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    bool horizontalOrientation = height / width < 1.0;
    return CustomWillPopWidget(
      child: Scaffold(
          backgroundColor: ColorStyle.backgroundColor,
          appBar: AppBar(
            centerTitle: true,
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            title: Text('Реквизиты'),
          ),
          body: SingleChildScrollView(
            padding: EdgeInsets.only(left: 36, right: 36, ),
            child: Column(
              children: <Widget>[
                Container(
                    height: (height - 70) / 3,
                    child: LoadingImageWidget(
                      path: 'assets/vectors/card.svg',
                    )),
                Container(
                  height: horizontalOrientation? 5: 10,
                ),
                Container(
                  child: Text(
                    'Укажите банковские реквизиты для получения зарплаты',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 18,
                        color: ColorStyle.txt,
                        fontWeight: FontWeight.w600),
                  ),
                ),
                Container(
                  height: horizontalOrientation? 5:20,
                ),
                StreamBuilder<bool>(
                    stream: payoutToCard.stream,
                    builder: (context, snapshot) {
                      return Padding(
                        padding: const EdgeInsets.only(left: 40, right: 40),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            InkWell(
                              onTap: () => payoutToCard.add(true),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    'По карте',
                                    style: TextStyle(
                                        color: ColorStyle.txt, fontSize: 13),
                                  ),
                                  Container(
                                    width: 8,
                                  ),
                                  CustomRadio<bool>(
                                      materialTapTargetSize:
                                          MaterialTapTargetSize.shrinkWrap,
                                      activeColor: ColorStyle.mainColor,
                                      value: true,
                                      groupValue: snapshot?.data,
                                      onChanged: (value) =>
                                          payoutToCard.add(value)),
                                ],
                              ),
                            ),
                            InkWell(
                              onTap: () => payoutToCard.add(false),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text('Счет в банке',
                                      style: TextStyle(
                                          color: ColorStyle.txt, fontSize: 13)),
                                  Container(
                                    width: 8,
                                  ),
                                  CustomRadio<bool>(
                                      materialTapTargetSize:
                                          MaterialTapTargetSize.shrinkWrap,
                                      activeColor: ColorStyle.mainColor,
                                      value: false,
                                      groupValue: snapshot?.data,
                                      onChanged: (value) =>
                                          payoutToCard.add(value)),
                                ],
                              ),
                            ),
                          ],
                        ),
                      );
                    })
              ],
            ),
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
          floatingActionButton: NextFab(
            text: 'Далее',
            blockedStream: Future.value(false).asStream(),
            nextFabPressed: nextFabPressed,
          )),
    );
  }

  nextFabPressed() {
    if(payoutToCard?.value ?? true) {
      Navigator.of(context).push(CupertinoPageRoute(
        builder: (_) => CardDataPage(),
      ));
    } else {
      Navigator.of(context).push(CupertinoPageRoute(
        builder: (_) => BankDataPage(),
      ));
    }
   /* if(payoutToCard?.value??true){
      customNavigator.push(CardDataPage());
    } else {
      customNavigator.push(BankDataPage());
    }*/
  }
}
