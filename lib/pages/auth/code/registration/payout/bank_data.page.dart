import 'dart:async';

import 'package:car_sharing_services/blocs/registration.bloc.dart';
import 'package:car_sharing_services/models/worker.dart';
import 'package:car_sharing_services/page/widget/custom_text_field.dart';
import 'package:car_sharing_services/page/widget/custom_will_pop_widget.dart';
import 'package:car_sharing_services/page/widget/next_fab.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:rxdart/rxdart.dart';

import '../email_data.page.dart';

class BankDataPage extends StatefulWidget {
  BankDataPage({Key key}) : super(key: key);

  @override
  _BankDataPageState createState() => _BankDataPageState();
}

class _BankDataPageState extends State<BankDataPage> {
  final validateFormKey = GlobalKey<FormState>();
  final BehaviorSubject<bool> blocked = BehaviorSubject();
  final BehaviorSubject<bool> visible = BehaviorSubject();
  final TextEditingController numberController = TextEditingController();
  final TextEditingController userNameController = TextEditingController();
  final TextEditingController bankNameController = TextEditingController();
  final TextEditingController bicController = TextEditingController();
  final TextEditingController billNumberController = TextEditingController();
  final FocusNode numberFocus = FocusNode();
  final FocusNode userNameFocus = FocusNode();
  final FocusNode bankNameFocus = FocusNode();
  final FocusNode bicFocus = FocusNode();
  final FocusNode billNumberFocus = FocusNode();

  var numberFormatter = MaskTextInputFormatter(
      mask: '#### #### #### #### ####', filter: {"#": RegExp(r'[0-9]')});
  var billNumberFormatter = MaskTextInputFormatter(
      mask: '#### #### #### #### ####', filter: {"#": RegExp(r'[0-9]')});
  var bicFormatter = MaskTextInputFormatter(
      mask: '#########', filter: {"#": RegExp(r'[0-9]')});

  RegistrationBloc bloc;
  StreamSubscription subscription;

  @override
  void initState() {
    super.initState();
    numberController.addListener(checkNumber);
    bicController.addListener(checkBic);
    billNumberController.addListener(checkBill);

    userNameController.addListener(checkControllers);
    numberController.addListener(checkControllers);
    bankNameController.addListener(checkControllers);
    bicController.addListener(checkControllers);
    billNumberController.addListener(checkControllers);

    subscription = KeyboardVisibility.onChange.listen((event) {
      if (!event) {
        visible.add(true);
        numberFocus?.unfocus();
        userNameFocus?.unfocus();
        bicFocus?.unfocus();
        bankNameFocus?.unfocus();
        billNumberFocus?.unfocus();
      } else {
        visible.add(false);
      }
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc = RegistrationBloc.of(context);
  }

  @override
  void dispose() {
    visible?.close();
    blocked?.close();
    userNameController.dispose();
    numberController.dispose();
    bankNameController.dispose();
    bicController.dispose();
    billNumberController.dispose();
    userNameFocus
      ..unfocus()
      ..dispose();
    numberFocus
      ..unfocus()
      ..dispose();
    bicFocus
      ..unfocus()
      ..dispose();
    bankNameFocus
      ..unfocus()
      ..dispose();
    bicFocus
      ..unfocus()
      ..dispose();
    subscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomWillPopWidget(
      dialogContent: 'Введенные данные будут утеряны',
      child: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Scaffold(
              backgroundColor: ColorStyle.backgroundColor,
              appBar: AppBar(
                centerTitle: true,
                elevation: 0,
                backgroundColor: Colors.transparent,
                title: Text('Счет в банке'),
              ),
              body: SingleChildScrollView(
                padding: const EdgeInsets.only(left: 36, right: 36, bottom: 70),
                child: Form(
                  key: validateFormKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 24,
                      ),
                      CustomTextField(
                        label: 'Номер счета',
                        controller: numberController,
                        focusNode: numberFocus,
                        keyboardType: TextInputType.number,
                        validator: numberValidator,
                        formatters: [numberFormatter],
                      ),
                      Container(
                        height: 24,
                      ),
                      CustomTextField(
                        label: 'ФИО получателя',
                        controller: userNameController,
                        focusNode: userNameFocus,
                        capitalization: TextCapitalization.words,
                      ),
                      Container(
                        height: 24,
                      ),
                      CustomTextField(
                        label: 'Название банка',
                        controller: bankNameController,
                        focusNode: bankNameFocus,
                        capitalization: TextCapitalization.words,
                      ),
                      Container(
                        height: 24,
                      ),
                      CustomTextField(
                        label: 'Банковский идентификационный код',
                        controller: bicController,
                        focusNode: bicFocus,
                        keyboardType: TextInputType.number,
                        validator: bicValidator,
                        formatters: [bicFormatter],
                      ),
                      Container(
                        height: 24,
                      ),
                      CustomTextField(
                        label: 'Корреспондентский счет',
                        controller: billNumberController,
                        focusNode: billNumberFocus,
                        keyboardType: TextInputType.number,
                        validator: billNumberValidator,
                        formatters: [billNumberFormatter],
                      ),
                    ],
                  ),
                ),
              ),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerDocked,
              floatingActionButton: StreamBuilder<bool>(
                  stream: visible.stream,
                  builder: (context, snapshot) {
                    return Visibility(
                      visible: snapshot?.data ?? true,
                      child: NextFab(
                        blockedStream: blocked.stream,
                        text: 'Все заполнено',
                        nextFabPressed: nextFabPressed,
                      ),
                    );
                  }))),
    );
  }

  FutureOr nextFabPressed() {
    if (validateFormKey.currentState.validate()) {
      /*logger.i('good data');
      bloc.addBankData(BankData(
        number: numberFormatter.getUnmaskedText(),
        bic: bicController.text.trim(),
        bankName: bankNameController.text.trim(),
        korBill: billNumberFormatter.getUnmaskedText(),
        name: userNameController.text.trim(),
      ));
      customNavigator.push(EmailDataPage());*/
      bloc.workerAccount = WorkerAccount(
        numberFormatter.getUnmaskedText(),
        userNameController.text.trim(),
        bankNameController.text.trim(),
        bicController.text.trim(),
        billNumberFormatter.getUnmaskedText(),
      );
      Navigator.of(context).push(CupertinoPageRoute(
        builder: (_) => EmailDataPage(),
      ));
    }
  }

  void checkControllers() {
    if ((numberController.text?.isEmpty ?? true) ||
        (userNameController.text?.isEmpty ?? true) ||
        (bankNameController.text?.isEmpty ?? true) ||
        (bicController.text?.isEmpty ?? true) ||
        (billNumberController.text?.isEmpty ?? true)) {
      blocked.add(true);
    } else {
      blocked.add(false);
    }
  }

  String numberValidator(String text) {
    if (text?.trim()?.isNotEmpty ?? false) {
      var temp = text.trim();
      if (temp.length == 24) {
        return null;
      }
    }
    return 'Номер счета введен неверно';
  }

  void checkNumber() {
    if (numberController.text.length > 24) {
      var temp = numberController.selection.extentOffset;
      numberController.value = TextEditingValue(
        text: numberController.text.substring(0, 24),
        selection: TextSelection.fromPosition(
          TextPosition(offset: temp > 24 ? 24 : temp),
        ),
      );
    }
  }

  String bicValidator(String text) {
    if (text?.trim()?.isNotEmpty ?? false) {
      var temp = text.trim();
      if (temp.length == 9) {
        return null;
      }
    }
    return 'БИК введен неверно';
  }

  void checkBic() {
    if (bicController.text.length > 9) {
      var temp = bicController.selection.extentOffset;
      bicController.value = TextEditingValue(
        text: bicController.text.substring(0, 9),
        selection: TextSelection.fromPosition(
          TextPosition(offset: temp > 9 ? 9 : temp),
        ),
      );
    }
  }

  String billNumberValidator(String text) {
    if (text?.trim()?.isNotEmpty ?? false) {
      var temp = text.trim();
      if (temp.length == 24) {
        return null;
      }
    }
    return 'Кор. счет введен неверно';
  }

  void checkBill() {
    if (billNumberController.text.length > 24) {
      var temp = billNumberController.selection.extentOffset;
      billNumberController.value = TextEditingValue(
        text: billNumberController.text.substring(0, 24),
        selection: TextSelection.fromPosition(
          TextPosition(offset: temp > 24 ? 24 : temp),
        ),
      );
    }
  }
}
