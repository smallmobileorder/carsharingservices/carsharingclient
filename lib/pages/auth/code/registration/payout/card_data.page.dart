import 'dart:async';

import 'package:car_sharing_services/blocs/registration.bloc.dart';
import 'package:car_sharing_services/models/worker.dart';
import 'package:car_sharing_services/page/widget/custom_text_field.dart';
import 'package:car_sharing_services/page/widget/custom_will_pop_widget.dart';
import 'package:car_sharing_services/page/widget/next_fab.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:rxdart/rxdart.dart';

import '../email_data.page.dart';

class CardDataPage extends StatefulWidget {
  CardDataPage({Key key}) : super(key: key);

  @override
  _CardDataPageState createState() => _CardDataPageState();
}

class _CardDataPageState extends State<CardDataPage> {
  final validateFormKey = GlobalKey<FormState>();
  final BehaviorSubject<bool> blocked = BehaviorSubject();
  final BehaviorSubject<bool> visible = BehaviorSubject();
  final TextEditingController numberController = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  final FocusNode numberFocus = FocusNode();
  final FocusNode nameFocus = FocusNode();
  var cardFormatter = MaskTextInputFormatter(
      mask: '#### #### #### ####', filter: {"#": RegExp(r'[0-9]')});

  RegistrationBloc bloc;
  StreamSubscription subscription;

  @override
  void initState() {
    super.initState();
    numberController.addListener(checkPassportNumber);
    nameController.addListener(checkControllers);
    numberController.addListener(checkControllers);

    subscription = KeyboardVisibility.onChange.listen((event) {
      if (!event) {
        visible.add(true);
        numberFocus?.unfocus();
        nameFocus?.unfocus();
      } else {
        visible.add(false);
      }
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc = RegistrationBloc.of(context);
  }

  @override
  void dispose() {
    visible?.close();
    blocked?.close();
    nameController.dispose();
    numberController.dispose();
    nameFocus
      ..unfocus()
      ..dispose();
    numberFocus
      ..unfocus()
      ..dispose();
    subscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomWillPopWidget(
      dialogContent: 'Введенные данные будут утеряны',
      child: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Scaffold(
              backgroundColor: ColorStyle.backgroundColor,
              appBar: AppBar(
                centerTitle: true,
                elevation: 0,
                backgroundColor: Colors.transparent,
                title: Text('По карте'),
              ),
              body: SingleChildScrollView(
                padding: const EdgeInsets.only(left: 36, right: 36, bottom: 70),
                child: Form(
                  key: validateFormKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 24,
                      ),
                      CustomTextField(
                        label: 'Номер карты',
                        controller: numberController,
                        focusNode: numberFocus,
                        keyboardType: TextInputType.number,
                        validator: passportNumberValidator,
                        formatters: [cardFormatter],
                      ),
                      Container(
                        height: 24,
                      ),
                      CustomTextField(
                        label: 'Владелец карты',
                        controller: nameController,
                        focusNode: nameFocus,
                        capitalization: TextCapitalization.words,
                      ),
                    ],
                  ),
                ),
              ),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerDocked,
              floatingActionButton: StreamBuilder<bool>(
                  stream: visible.stream,
                  builder: (context, snapshot) {
                    return Visibility(
                      visible: snapshot?.data ?? true,
                      child: NextFab(
                        blockedStream: blocked.stream,
                        text: 'Все заполнено',
                        nextFabPressed: nextFabPressed,
                      ),
                    );
                  }))),
    );
  }

  FutureOr nextFabPressed() {
    if (validateFormKey.currentState.validate()) {
      /*logger.i('good data');
      bloc.addCardData(CardData(
        number: cardFormatter.getUnmaskedText(),
        name: nameController.text.trim(),
      ));
      customNavigator.push(EmailDataPage());*/
      bloc.workerCard = WorkerCard(
        nameController.text.trim(),
        cardFormatter.getUnmaskedText(),
      );
      Navigator.of(context).push(CupertinoPageRoute(
        builder: (_) => EmailDataPage(),
      ));
    }
  }

  void checkControllers() {
    if ((numberController.text?.isEmpty ?? true) ||
        (nameController.text?.isEmpty ?? true)) {
      blocked.add(true);
    } else {
      blocked.add(false);
    }
  }

  String passportNumberValidator(String text) {
    if (text?.trim()?.isNotEmpty ?? false) {
      var temp = text.trim();
      if (temp.length == 19) {
        return null;
      }
    }
    return 'Номер карты введен неверно';
  }

  void checkPassportNumber() {
    if (numberController.text.length > 19) {
      var temp = numberController.selection.extentOffset;
      numberController.value = TextEditingValue(
        text: numberController.text.substring(0, 19),
        selection: TextSelection.fromPosition(
          TextPosition(offset: temp > 19 ? 19 : temp),
        ),
      );
    }
  }
}
