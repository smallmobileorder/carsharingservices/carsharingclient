import 'dart:async';

import 'package:car_sharing_services/blocs/auth.bloc.dart';
import 'package:car_sharing_services/blocs/core.bloc.dart';
import 'package:car_sharing_services/page/widget/loading_line.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:car_sharing_services/service/firebase/auth/auth_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';
import 'package:rxdart/rxdart.dart';

class CodePage extends StatefulWidget {
  final String number;
  final int forceResendingToken;
  final String verificationId;

  const CodePage(
    this.forceResendingToken,
    this.verificationId, {
    Key key,
    this.number,
  }) : super(key: key);

  @override
  createState() => _CodePageState();
}

class _CodePageState extends State<CodePage> {
  CoreBloc coreBloc;
  AuthBloc authBloc;

  final codeController = TextEditingController();

  final FocusNode codeFocus = FocusNode();

  BehaviorSubject<int> time = BehaviorSubject();

  final AuthService authService = AuthService();

  bool visible;

  int pinLength;

  bool hasError;

  bool isLoading;

  double pinWidth;

  double width;

  double height;

  bool horizontalOrientation;

  Timer resendTimer;

  int lostSeconds;

  int forceResendingToken;
  String verificationId;
  StreamSubscription subscription;

  @override
  void initState() {
    this.forceResendingToken = widget.forceResendingToken;
    this.verificationId = widget.verificationId;
    lostSeconds = -1;
    pinLength = 6;
    hasError = false;
    isLoading = false;
    pinWidth = 50;

    subscription = KeyboardVisibility.onChange.listen((event) {
      if (!event) {
        codeFocus.unfocus();
      }
    });

    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    coreBloc = CoreBloc.of(context);
    authBloc = AuthBloc.of(context);
  }

  @override
  void dispose() {
    subscription?.cancel();
    codeController.dispose();
    codeFocus.dispose();
    time.close();
    resendTimer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    horizontalOrientation = height / width < 1.0;
    pinWidth = (horizontalOrientation
            ? MediaQuery.of(context).size.height
            : MediaQuery.of(context).size.width - 50) /
        6;
    return SafeArea(
      bottom: false,
      child: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          backgroundColor: ColorStyle.backgroundColor,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0.0,
          ),
          body: page(context),
        ),
      ),
    );
  }

  page(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.fromLTRB(0, horizontalOrientation ? 0 : 10, 0, 0),
      child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: <
          Widget>[
        Text(
          'Подтвердите\nкод из SMS',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 18, color: ColorStyle.txt, fontWeight: FontWeight.w600),
        ),
        Container(
          height: horizontalOrientation ? 5 : 33,
        ),
        PinCodeTextField(
          autofocus: true,
          controller: codeController,
          hideCharacter: false,
          highlight: false,
          highlightColor: Colors.transparent,
          defaultBorderColor: ColorStyle.sideColor,
          hasTextBorderColor: Colors.transparent,
          errorBorderColor: Colors.transparent,
          maxLength: pinLength,
          maskCharacter: '○',
          //'●' :,
          onTextChanged: (text) {
            setState(() {
//                hasError = false;
            });
          },
          onDone: onDone,
          wrapAlignment: WrapAlignment.center,
          pinBoxDecoration: ProvidedPinBoxDecoration.roundedPinBoxDecoration,
          pinTextStyle:
              TextStyle(fontSize: pinWidth / 2.3, color: ColorStyle.mainColor),
          pinBoxHeight: pinWidth / 2,
          pinBoxWidth: pinWidth / 2,
          pinTextAnimatedSwitcherTransition:
              ProvidedPinBoxTextAnimation.scalingTransition,
          pinTextAnimatedSwitcherDuration: Duration(milliseconds: 300),
        ),
        Container(
          height: horizontalOrientation ? 0 : 10,
        ),
        Visibility(
          child: Text(
            "Неверный код",
            style: TextStyle(fontSize: 13, color: ColorStyle.wrong),
          ),
          visible: !isLoading && hasError,
        ),
        Container(
          height: horizontalOrientation ? 5 : 12,
        ),
        StreamBuilder<int>(
            stream: time,
            builder: (context, snapshot) {
              if (snapshot.hasData && snapshot.data >= 0)
                return RichText(
                  text: TextSpan(
                      style:
                          TextStyle(color: ColorStyle.sideColor, fontSize: 13),
                      children: [
                        TextSpan(text: 'Повторная отправка через '),
                        TextSpan(
                          text: '${snapshot?.data ?? 0} ',
                          style: TextStyle(color: ColorStyle.mainColor),
                        ),
                        TextSpan(
                          text: 'сек',
                        ),
                      ]),
                );
              return RichText(
                text: TextSpan(
                    style: TextStyle(color: ColorStyle.sideColor, fontSize: 13),
                    children: [
                      TextSpan(text: 'Не пришел код? '),
                      TextSpan(
                          text: 'Получить новый',
                          style: TextStyle(color: ColorStyle.mainColor),
                          recognizer: TapGestureRecognizer()
                            ..onTap = getNewCode)
                    ]),
              );
            }),
        Container(
          height: horizontalOrientation ? 20 : 40,
        ),
        if (isLoading) LoadingLine()
      ]),
    );
  }

  getNewCode() {
    print('get new verification code');
    codeController.clear();
    initTimerForResend();
    setState(() {
      isLoading = false;
      hasError = false;
    });
    authBloc.verifyNumber(widget.number, (error) {
      print('FAILED TO VERIFY NUMBER: $error');
    }, (verificationId, [forceResendingToken]) {
      this.forceResendingToken = forceResendingToken;
      this.verificationId = verificationId;
    }, (String verificationId) {
      this.verificationId = verificationId;
    });
    /*authService.auth(
      widget.number,
      (user) {
        print('uid:${user.uid}, number ${user.phoneNumber}');
        bloc.userVerification(user.phoneNumber);
      },
      () => print('new code sended'),
    );*/
  }

  initTimerForResend() {
    lostSeconds = 30;
    resendTimer?.cancel();
    time.add(lostSeconds);
    resendTimer = Timer.periodic(Duration(seconds: 1), (t) {
      lostSeconds--;
      time.add(lostSeconds);
      if (lostSeconds < 0) resendTimer.cancel();
    });
  }

  onDone(String code) async {
    print('send code $code');
    setState(() {
      isLoading = true;
      hasError = false;
    });
    try {
      await authBloc.checkCode(code, verificationId);
    } catch (e) {
      print('error catched $e');
      if (e.toString().contains('code')) {
        codeController.clear();
        setState(() {
          isLoading = false;
          hasError = true;
        });
      }
    }
    /* authService.verify(code).then((user) {
      hasError = user == null;
      if (hasError) {
        codeController.clear();
        setState(() => isLoading = false);
      } else {
        print('uid:${user.uid}, number ${user.phoneNumber}');
        bloc.userVerification(user.phoneNumber);
      }
    });*/
  }
}
