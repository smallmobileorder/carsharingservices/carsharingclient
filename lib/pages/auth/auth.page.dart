import 'dart:async';

import 'package:car_sharing_services/blocs/auth.bloc.dart';
import 'package:car_sharing_services/blocs/core.bloc.dart';
import 'package:car_sharing_services/blocs/registration.bloc.dart';
import 'package:car_sharing_services/page/widget/next_fab.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

import 'code/code.page.dart';

class AuthPage extends StatefulWidget {
  const AuthPage({
    Key key,
  }) : super(key: key);

  @override
  createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage> {
  AuthBloc authBloc;
  CoreBloc coreBloc;
  final numberController = TextEditingController();

  var maskFormatter = MaskTextInputFormatter(
    mask: '(###) ###-##-##',
    filter: {"#": RegExp(r'[0-9]')},
  );

  final FocusNode numberFocus = FocusNode();

  BehaviorSubject<bool> blockedStream = BehaviorSubject();

  bool visible;
  double width;
  double height;
  bool horizontalOrientation;
  StreamSubscription subscription;

  @override
  void initState() {
    super.initState();
    visible = true;
    blockedStream.add(true);

    subscription = KeyboardVisibility.onChange.listen((event) {
      if (!event) {
        numberFocus.unfocus();
      }
    });

    numberFocus.addListener(onFocusChange);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    authBloc = AuthBloc.of(context);
    coreBloc = CoreBloc.of(context);
  }

  @override
  void dispose() {
    numberController.dispose();
    numberFocus.dispose();
    blockedStream.close();
    subscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    horizontalOrientation = height / width < 1.0;
    return WillPopScope(
      onWillPop: () async {
        SystemNavigator.pop();
        return false;
      },
      child: SafeArea(
        child: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Scaffold(
            backgroundColor: ColorStyle.backgroundColor,
            body: page(context),
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerFloat,
            floatingActionButton: Visibility(
              visible: visible,
              child: StreamBuilder<bool>(
                stream: coreBloc.connection,
                builder: (context, snapshot) {
                  return NextFab(
                    buttonContainerColor: Colors.transparent,
                    padding: const EdgeInsets.only(left: 16, right: 16),
                    nextFabPressed: nextFabPressed,
                    blockedStream: blockedStream.stream
                        .mergeWith([coreBloc.connection.map((v) => !v)]),
                    text: 'Отправить код',
                  );
                },
              ),
            ),
          ),
        ),
      ),
    );
  }

  page(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.fromLTRB(25, horizontalOrientation ? 5 : 70, 25, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            'Введите ваш\nномер телефона',
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 18,
                color: ColorStyle.txt,
                fontWeight: FontWeight.w600),
          ),
          Container(
            height: horizontalOrientation ? 0 : 32,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 0.0, right: 5),
                child: Container(
                  height: 40,
                  alignment: Alignment.center,
                  child: Text(
                    '+7',
                    style: TextStyle(
                      color: ColorStyle.mainColor,
                      fontSize: 24.0,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ),
              Container(
                width: 200,
                child: TextField(
                  keyboardType: TextInputType.number,
                  controller: numberController,
                  focusNode: numberFocus,
                  inputFormatters: [maskFormatter],
                  onChanged: (_) => onChanged(),
                  cursorColor: ColorStyle.txt,
                  autofocus: true,
                  decoration: InputDecoration(border: InputBorder.none),
                  style: TextStyle(
                      color: ColorStyle.txt,
                      fontSize: 24.0,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ],
          ),
          StreamBuilder<bool>(
            stream: coreBloc.connection,
            builder: (context, snapshot) {
              if (snapshot?.data ?? false) return Container();
              return Container(
                child: Text(
                  'Отсутствует подключение к интернету',
                  style: TextStyle(fontSize: 13, color: ColorStyle.wrong),
                  textAlign: TextAlign.center,
                ),
              );
            },
          )
        ],
      ),
    );
  }

  void nextFabPressed() async {
    await authBloc.verifyNumber('+7 ${numberController.text}', (error) {
      print('FAILED TO VERIFY NUMBER: ${error.code} ${error.message}');
    }, (verificationId, [forceResendingToken]) {
      Provider.of<RegistrationBloc>(context, listen: false).phone =
          '+7 ${numberController.text}';
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) => CodePage(
            forceResendingToken,
            verificationId,
            number: '+7 ${numberController.text}',
          ),
        ),
      );
    }, (verificationId) => print(verificationId));
  }

  onChanged() {
    blockedStream.add(!((maskFormatter?.getUnmaskedText()?.length ?? 0) == 10));
    onFocusChange();
  }

  void onFocusChange() {
    if (numberFocus.hasFocus) {
      if (visible == true && blockedStream.value)
        setState(() => visible = false);
      if (blockedStream.value == false && visible == false)
        setState(() => visible = true);
    } else {
      if (blockedStream.value == false && visible == false)
        setState(() => visible = true);
    }
  }
}
