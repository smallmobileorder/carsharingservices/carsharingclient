import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:car_sharing_services/application.dart';
import 'package:car_sharing_services/page/widget/dots_indicator.dart';
import 'package:car_sharing_services/page/widget/loading_container.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class QuestionsPage extends StatefulWidget {
  final List<String> questions;
  final String title;
  final File image;
  final Function back;
  final Function accept;
  final Function retake;
  final Stream<bool> loading;

  QuestionsPage({
    Key key,
    @required this.questions,
    @required this.title,
    @required this.image,
    @required this.back,
    @required this.accept,
    @required this.retake,
    this.loading,
  }) : super(key: key);

  @override
  _QuestionsPageState createState() => _QuestionsPageState();
}

class _QuestionsPageState extends State<QuestionsPage> {
  final PageController controller = PageController();
  bool landscape;
  bool pressed = false;

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    landscape = width > height;
    return WillPopScope(
      onWillPop: () {
        widget.back();
        return Future.value(false);
      },
      child: Scaffold(
        backgroundColor: ColorStyle.txt,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          centerTitle: true,
          elevation: 0,
          title: Text(
            widget.title ?? '',
            style: TextStyle(color: ColorStyle.white),
          ),
          iconTheme: IconThemeData(color: ColorStyle.white),
        ),
        body: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  height: landscape ? 4 : 24,
                ),
                Expanded(
                  flex: 5,
                  child: Container(
                    color: Color(0xFF393939),
                    height: double.infinity,
                    width: double.infinity,
                    child: Image.file(
                      ///add stub for null exception
                      widget.image,
                      fit: BoxFit.scaleDown,
                    ),
                  ),
                ),
                Expanded(
                  flex: 4,
                  child: Column(
                    children: <Widget>[
                      Expanded(
                        child: PageView(
                            physics: NeverScrollableScrollPhysics(),
                            controller: controller,
                            children: List.generate(
                              widget?.questions?.length ?? 0,
                              (index) => Padding(
                                padding: EdgeInsets.only(
                                  top: landscape ? 4 : 24,
                                  left: 36,
                                  right: 36,
                                ),
                                child: AutoSizeText(
                                  widget?.questions[index] ?? '',
                                  maxLines: 3,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    color: ColorStyle.white,
                                    fontSize: 18,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            )),
                      ),
                      if ((widget?.questions?.length ?? 0) > 1)
                        DotsIndicator(
                          controller: controller,
                          itemCount: widget?.questions?.length ?? 0,
                        ),
                      ButtonBar(
                        alignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          //TODO use Button widget after upgrade
                          FlatButton(
                            materialTapTargetSize:
                                MaterialTapTargetSize.shrinkWrap,
                            child: AutoSizeText(
                              'Нет, переснять',
                              style: TextStyle(
                                  //fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                  color: ColorStyle.white),
                            ),
                            color: Colors.transparent,
                            onPressed: () async {
                              logger.i('Нет, переснять');
                              await widget.retake();
                            },
                          ),
                          FlatButton(
                            materialTapTargetSize:
                                MaterialTapTargetSize.shrinkWrap,
                            child: AutoSizeText('Да, подтверждаю',
                                style: TextStyle(
                                    //fontSize: 18,
                                    fontWeight: FontWeight.w500,
                                    color: ColorStyle.mainColor)),
                            color: Colors.transparent,
                            onPressed: () async {
                              if (!pressed) {
                                pressed = true;
                                logger.i('Да, подтверждаю');
                                if (controller.page !=
                                    (widget?.questions?.length ?? 1) - 1) {
                                  controller?.nextPage(
                                      duration: Duration(milliseconds: 300),
                                      curve: Curves.easeInOut);
                                } else {
                                  await widget.accept();
                                }
                                pressed = false;
                              }
                            },
                          ),
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
            widget.loading != null
                ? StreamBuilder<bool>(
                    stream: widget.loading,
                    initialData: false,
                    builder: (_, snapshot) => snapshot.data == true
                        ? Center(child: LoadingContainer())
                        : Container(),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}
