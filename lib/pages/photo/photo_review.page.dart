import 'dart:async';

import 'package:car_sharing_services/page/widget/loading_image_widget.dart';
import 'package:car_sharing_services/page/widget/next_fab.dart';
import 'package:car_sharing_services/pages/style/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PhotoReviewPage extends StatefulWidget {
  final String title;
  final String imageStubPath;
  final String text;

  final Function nextFabPressed;

  const PhotoReviewPage({
    Key key,
    @required this.title,
    this.imageStubPath,
    @required this.text,
    this.nextFabPressed,
  }) : super(key: key);

  createState() => _PhotoReviewPageState();
}

class _PhotoReviewPageState extends State<PhotoReviewPage> {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery
      .of(context)
      .size
      .height;
    return Scaffold(
      backgroundColor: ColorStyle.backgroundColor,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        title: Text(widget.title ?? ''),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(left: 36, right: 36),
        child: Column(
          children: <Widget>[
            Container(
              height: height / 3,
              child: LoadingImageWidget(
                path: widget.imageStubPath,
              )),
            Container(
              height: 10,
            ),
            Container(
              child: Text(
                widget.text ?? '',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 18,
                  color: ColorStyle.txt,
                  fontWeight: FontWeight.w600),
              ),
            )
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: NextFab(
        text: 'Сделать фото',
        blockedStream: Future.value(false).asStream(),
        nextFabPressed: widget?.nextFabPressed,
      ));
  }
}
