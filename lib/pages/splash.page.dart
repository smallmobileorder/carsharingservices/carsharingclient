import 'dart:async';

import 'package:car_sharing_services/blocs/auth.bloc.dart';
import 'package:car_sharing_services/endpoints/worker.dart';
import 'package:car_sharing_services/page/widget/loading_container.dart';
import 'package:car_sharing_services/page/widget/update_app_dialog.dart';
import 'package:car_sharing_services/pages/app/app_main.dart';
import 'package:car_sharing_services/pages/auth/auth.page.dart';
import 'package:car_sharing_services/pages/auth/code/registration/agreement.page.dart';
import 'package:car_sharing_services/service/firebase/remote_config/remote_config.dart';
import 'package:car_sharing_services/service/storage/storage_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:version/version.dart';

/// Page for displaying loading indicator while [AuthBloc] checks
/// user status.
///
/// If user is logged then go to [AppMainPage] and remove other pages from tree.
/// If user is not logged then go to [AuthPage]
class SplashPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  AuthBloc bloc;
  StreamSubscription loggedSub;
  bool checkVersionOpened;

  @override
  void didChangeDependencies() {
    print('DID CHANGE DEP SPLASH');
    super.didChangeDependencies();
    bloc = AuthBloc.of(context);

    // subscribe to logged event
    loggedSub = bloc.authState.listen((state) async {
      if (state == null) {
        return;
      }

      ///USE FOR FAST TESTING WORKFLOW
      ///state = AuthState.LOGGED;
      switch (state) {
        case AuthState.LOGGED:
          Navigator.of(context).popUntil((route) => route.isFirst);
          print("AuthState: LOGGED");
          // if user is logged then get worker and open main screen
          ///USE FOR FAST TESTING WORKFLOW
          ///var token = "worker-key";
          ///var workerId = "32d05304-064b-4280-bf9c-1cf9dd3e6080"; //await StorageService.getWorkerId();
          var token = await StorageService.getFirebaseToken();
          var workerId = await StorageService.getWorkerId();
          // get worker info
          var worker = await bloc.api.request(getWorkerById(workerId, token));
          var serverAddr = await RemoteConfigService.getServerAddress();
          Navigator.of(context, rootNavigator: true).pushAndRemoveUntil(
            CupertinoPageRoute(
                builder: (_) =>
                    AppMainPage(token, worker, serverAddr: serverAddr),
                settings: RouteSettings(name: 'AppMain')),
            (route) => false,
          );
          break;
        case AuthState.FAILED:
          print("AuthState: FAILED");
          // TODO show error dialog
          break;
        case AuthState.NEED_REGISTRATION:
          // Navigator.of(context).popUntil((route) => route.isFirst);
          print("AuthState: NEED_REGISTRATION");
          Navigator.of(context).push(CupertinoPageRoute(
            builder: (_) => AgreementPage(),
          ));
          /*Navigator.of(context, rootNavigator: true).pushAndRemoveUntil(
            CupertinoPageRoute(
              builder: (_) => AgreementPage(),
            ),
            (route) => false,
          );*/
          break;
        case AuthState.NEED_AUTH:
          print("AuthState: NEED_AUTH");
          // Navigator.of(context).popUntil((route) => route.isFirst);
          // go to authorization page
//          Navigator.of(context, rootNavigator: true).pushAndRemoveUntil(
//              CupertinoPageRoute(
//                builder: (_) => AuthPage(),
//              ),
//              (_) => false);
          Navigator.of(context).push(CupertinoPageRoute(
            builder: (_) => AuthPage(),
          ));
          break;
      }
      checkVersion();
    });
  }

  @override
  void initState() {
    print('INIT SPLASH');
    checkVersionOpened = false;
    super.initState();
  }

  @override
  void dispose() {
    print('DISPOSE SPLASH');
    super.dispose();
    loggedSub?.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Center(child: LoadingContainer()));
  }

  void checkVersion() async {
    var versions = await RemoteConfigService.getVersions();
    var storeLink = await RemoteConfigService.getStoreLink();
    var info = await PackageInfo.fromPlatform();
    var appVersion = info.version;
    print(versions);
    print(appVersion);

    Version currentVersion = Version.parse(appVersion);
    Version minimalVersion = Version.parse(versions[0]);
    Version latestVersion = Version.parse(versions[1]);
    if (checkVersionOpened) return;
    if (minimalVersion > currentVersion) {
      print('need update');
      checkVersionOpened = true;
      await showDialog(
          context: context,
          barrierDismissible: false,
          builder: (_) => UpdateAppDialog(
                storeLink,
                forced: true,
              ));
    } else if (latestVersion > currentVersion) {
      print('maybe update');
      checkVersionOpened = true;
      await showDialog(
          context: context,
          barrierDismissible: false,
          builder: (_) => UpdateAppDialog(storeLink));
    }
  }
}
