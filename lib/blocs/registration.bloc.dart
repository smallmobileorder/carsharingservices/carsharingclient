import 'dart:io';

import 'package:car_sharing_services/api_client/api_client.dart';
import 'package:car_sharing_services/endpoints/registration.dart';
import 'package:car_sharing_services/models/worker.dart';
import 'package:car_sharing_services/service/storage/storage_service.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

/// Bloc for performing registration
///
/// Collects all data about worker and then makes single request. Also
/// worker can resend data with this bloc
class RegistrationBloc {
  String uid;
  String name;
  String surname;
  String secondName;

  bool male;
  String mail;
  String phone;
  String birthday;
  WorkerAccount workerAccount;
  WorkerCard workerCard;
  WorkerLicense workerLicense;
  WorkerPassport workerPassport;

  File passportFirstPage;
  File passportRegistrationPage;
  File selfWithPassport;
  File licenseFront;
  File licenseBack;

  BuildContext context;
  ApiClient api;

  RegistrationBloc(this.context, this.api);

  /// Save all photos to store and then send all data to server
  Future<void> register(String uid, String token) async {
    if (uid == null || uid == "") {
      throw Exception("Empty uid");
    }
    this.uid = uid;
    // check if all data exists
    if (!_isDataValid()) {
      throw Exception("Some data is empty");
    }
    // send all photos to store
    workerPassport.photoMainLink = await uploadTo(
      passportFirstPage,
      '/photos/$uid/passport_main',
    );
    workerPassport.photoRegistrationLink = await uploadTo(
      passportRegistrationPage,
      '/photos/$uid/passport_reg',
    );
    workerPassport.photoSelfLink = await uploadTo(
      selfWithPassport,
      '/photos/$uid/passport_self',
    );
    workerLicense.photoFrontLink = await uploadTo(
      licenseFront,
      '/photos/$uid/license_front',
    );
    workerLicense.photoBackLink = await uploadTo(
      licenseFront,
      '/photos/$uid/license_back',
    );
    // send data to server
    var id = await api.request(registerWorker(
      uid,
      token,
      WorkerInfo(uid, name, surname, secondName, male, mail, phone, birthday),
      workerPassport,
      workerLicense,
      workerCard,
      workerAccount,
    ));
    await StorageService.saveWorkerId(id);
  }

  Future<String> uploadTo(File photo, String path) async {
    StorageTaskSnapshot snapshot = await FirebaseStorage.instance
        .ref()
        .child(path)
        .putData(photo.readAsBytesSync())
        .onComplete;
    return snapshot.ref.getDownloadURL().then((value) => value as String);
  }

  bool _isDataValid() {
    return name != null &&
        surname != null &&
        secondName != null &&
        birthday != null &&
        male != null &&
        mail != null &&
        phone != null &&
        workerPassport != null &&
        workerLicense != null &&
        (workerCard != null || workerAccount != null);
  }

  void dispose() {}

  static RegistrationBloc of(BuildContext context) {
    return Provider.of<RegistrationBloc>(context);
  }
}
