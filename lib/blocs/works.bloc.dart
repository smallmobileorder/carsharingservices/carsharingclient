import 'dart:typed_data';

import 'package:car_sharing_services/api_client/api_client.dart';
import 'package:car_sharing_services/blocs/profile.bloc.dart';
import 'package:car_sharing_services/endpoints/cars_params.dart';
import 'package:car_sharing_services/endpoints/works.dart';
import 'package:car_sharing_services/models/store_item.dart';
import 'package:car_sharing_services/models/work_adapter.dart';
import 'package:car_sharing_services/util/error_handler.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

class WorksBloc {
  BehaviorSubject<List<WorkAdapter>> currentWorks = BehaviorSubject.seeded([]);
  BehaviorSubject<List<WorkAdapter>> historyWorks = BehaviorSubject.seeded([]);
  BehaviorSubject<WorkAdapter> selectedWork = BehaviorSubject();

  BuildContext context;
  ApiClient api;
  ProfileBloc profileBloc;

  WorksBloc(this.context, this.api, this.profileBloc);

  Future<void> getAllCurrentWorks() async {
    await api.request(getCurrentWorks(profileBloc.userId)).then((res) {
      if (res == null) {
        currentWorks.add([]);
      } else {
        currentWorks.add(res);
      }
    });
  }

  Future<void> getAllHistoryWorks() async {
    await api.request(getHistoryWorks(profileBloc.userId)).then((value) {
      if (value == null) {
        historyWorks.add([]);
      } else {
        historyWorks.add(value);
      }
    });
  }

  Future startNewWork(
    String serviceId,
    String carId,
  ) async {
    //try {
    await api
        .request(startWork(profileBloc.userId, serviceId, carId))
        .then((value) => selectedWork.add(value))
        .whenComplete(() => getAllCurrentWorks().whenComplete(() =>
            selectedWork.add(currentWorks.value.singleWhere(
                (element) => element.id == selectedWork.value.id))))
        .catchError((error) => print("Can't create work $error"));
    //} catch (e) {
    //return false;
    //}
  }

  Future<void> updatePhotoBefore(Uint8List imageBytes) async {
    String randomKey = FirebaseDatabase.instance.reference().push().key;
    StorageTaskSnapshot snapshot = await FirebaseStorage.instance
        .ref()
        .child('photos/works/${selectedWork.value.id}/$randomKey')
        .putData(imageBytes)
        .onComplete;
    var photoBeforeLink = await snapshot.ref.getDownloadURL();

    api
        .request(addPhotoBeforeToWork(
            profileBloc.userId, selectedWork.value.id, photoBeforeLink))
        .catchError((error) =>
            print('error occured then try to load photo before $error'))
        .whenComplete(() async {
      getAllCurrentWorks().whenComplete(() {
        var work = currentWorks.value.firstWhere(
            (element) => element.id == selectedWork.value.id,
            orElse: () => null);
        print(work.photoBefore);
        if (work != null) selectedWork.add(work);
      });
    });
  }

  Future<void> updatePhotoAfter(Uint8List imageBytes) async {
    String randomKey = FirebaseDatabase.instance.reference().push().key;
    StorageTaskSnapshot snapshot = await FirebaseStorage.instance
        .ref()
        .child('photos/works/${selectedWork.value.id}/$randomKey')
        .putData(imageBytes)
        .onComplete;
    var photoAfterLink = await snapshot.ref.getDownloadURL();

    api
        .request(addPhotoAfterToWork(
            profileBloc.userId, selectedWork.value.id, photoAfterLink))
        .catchError((error) =>
            print('error occured then try to load photo before $error'))
        .whenComplete(() async {
      getAllCurrentWorks().whenComplete(() {
        var work = currentWorks.value.firstWhere(
            (element) => element.id == selectedWork.value.id,
            orElse: () => null);
        if (work != null) selectedWork.add(work);
      });
    });
  }

  Future<bool> closeWork() async {
    try {
      await api.request(finishWork(profileBloc.userId, selectedWork.value.id));
    } catch (e) {
      return false;
    }
    getAllHistoryWorks();
    return true;
  }

  void dispose() {
    currentWorks.close();
    historyWorks.close();
    selectedWork.close();
  }

  factory WorksBloc.of(BuildContext context) {
    return Provider.of<WorksBloc>(context);
  }

  Future<List<StoreItem>> loadCarParams(String carId) async {
    return await errorHandler(api.request(getCarParams(carId)), errorValue: []);
  }

  Future<bool> addCarParams(
      String carId, String storeItemId, int amount) async {
    return await api.request(
        addToCarParams(profileBloc.userId, carId, storeItemId, amount));
  }

  Future<bool> removeCarParams(
      String carId, String storeItemId, int amount) async {
    return await api.request(
        removeFromCarParams(profileBloc.userId, carId, storeItemId, amount));
  }
}
