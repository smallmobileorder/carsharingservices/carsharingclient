import 'package:car_sharing_services/api_client/api_client.dart';
import 'package:car_sharing_services/application.dart';
import 'package:car_sharing_services/blocs/profile.bloc.dart';
import 'package:car_sharing_services/endpoints/store.dart';
import 'package:car_sharing_services/models/store_item.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

class StorehouseBloc {
  BehaviorSubject<List<StoreItem>> inventory = BehaviorSubject.seeded([]);
  BehaviorSubject<List<StorehouseWithItems>> storehouses =
      BehaviorSubject.seeded([]);

  BuildContext context;
  ApiClient api;
  ProfileBloc profileBloc;

  static StorehouseBloc of(BuildContext context) {
    return Provider.of<StorehouseBloc>(context);
  }

  StorehouseBloc(this.context, this.api, this.profileBloc) {
    //updateStorehouses();
    //fetchInventory();
  }

//  Future<void> _loadStorehouseItems() async {
//    if (storehouses?.value != null && storehouses?.value != {}) {
//      List<Future<List<StoreItem>>> loadList = [];
//      for (var storehouse in storehouses.value.keys) {
//        loadList.add(api.request(getStorehouseItems(storehouse.id)));
//      }
//      Future.wait(loadList).then((itemsLists) {
//        storehouses.add(Map.fromIterables(storehouses.value.keys, itemsLists));
//      });
//    }
//  }

//  Future<void> updateStorehouses() async {
//    return api.request(getStorehouses()).then((value) {
//      if (value != null) {
//        Map temp = <Storehouse, List<StoreItem>>{};
//        //value.map((e) => temp.addAll({e: <StoreItem>[]}));
//        value.map((e) => temp[e] = <StoreItem>[]);
//        storehouses.add(temp);
//        _loadStorehouseItems();
//      }
//    }).catchError((err) => logger.w(err.toString()));
//  }

  Future<void> updateStorehouses() async {
    return api.request(getStorehousesWithItems()).then((value) async {
      if (value != null) {
        if (value.isNotEmpty) {
          storehouses.add(value);
        } else {
          var stores = await api.request(getStorehouses());
          // print(stores.first.toJson());
          if (stores != null) {
            storehouses.add(stores
                .map((e) => StorehouseWithItems.fromStorehouse(e))
                .toList());
          } else {
            storehouses.add([]);
          }
        }
      } else {
        storehouses.add([]);
      }
    }).catchError((err) => logger.w(err.toString()));
  }

  Future fetchInventory() {
    return api.request(getInventory(profileBloc.userId)).then((value) {
      if (value != null) {
        inventory.add(value);
      }
    }).catchError((err) => logger.w(err.toString()));
  }

  Future<bool> putToInventory(
      {@required String storehouseId,
      @required String itemId,
      @required int amount}) {
    return api.request(
        addToInventory(profileBloc.userId, storehouseId, itemId, amount));
  }

  Future<bool> deleteFromInventory(
      {@required String storehouseId,
      @required String itemId,
      @required int amount}) {
    // print(storehouseId);
    // print(itemId);
    // print(amount);
    return api.request(
        removeFromInventory(profileBloc.userId, storehouseId, itemId, amount));
  }

  void dispose() {
    storehouses.close();
    inventory.close();
  }
}
