import 'package:car_sharing_services/api_client/api_client.dart';
import 'package:car_sharing_services/endpoints/worker.dart';
import 'package:car_sharing_services/service/storage/storage_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

/// Bloc for managing authorization and registration process
class AuthBloc {
  BehaviorSubject<AuthState> authState = BehaviorSubject();

  BuildContext context;
  ApiClient api;

  AuthBloc(this.context, this.api) {
    Firebase.initializeApp();
    // get firebase token from storage
    StorageService.getFirebaseToken().then((token) async {
      if (token == null ?? token.isEmpty) {
        authState.add(AuthState.NEED_AUTH);
        return;
      }
      // get user id
      var id = await api.request(getIdByToken(token));
      if (id == null) {
        authState.add(AuthState.NEED_AUTH);
        return;
      }
      // get user status
      var status = await api.request(getWorkerStatus(token, id));
      if (status == null) {
        authState.add(AuthState.NEED_AUTH);
        return;
      }
      // save it to store
      await StorageService.saveWorkerId(id);
      // if status is not null then user is already created
      authState.add(AuthState.LOGGED);
    });
  }

  Future<void> verifyNumber(
    String number,
    PhoneVerificationFailed verificationFailed,
    PhoneCodeSent codeSent,
    PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout, {
    int forceResendingToken,
  }) async {
    // start authorization with phone
    await FirebaseAuth.instance.verifyPhoneNumber(
      phoneNumber: number,
      timeout: Duration(seconds: 25),
      verificationCompleted: (credential) {
        loginWithCredential(credential);
      },
      verificationFailed: verificationFailed,
      codeSent: codeSent,
      forceResendingToken: forceResendingToken,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout
    );
  }

  Future<void> checkCode(String smsCode, String verificationId) async {
    var credential = PhoneAuthProvider.credential(
      verificationId: verificationId,
      smsCode: smsCode,
    );
    await loginWithCredential(credential);
  }

  Future<void> loginWithCredential(AuthCredential credential) async {
    // get token from firebase
    print('CREDS: ${credential.toString()} ${credential.providerId}');
    print('INSTANCE: ${FirebaseAuth.instance}');
    var res = await FirebaseAuth.instance
        .signInWithCredential(credential)
        .catchError((e) {
      print('loginWithCredential: ERROR Occurred - $e');
      return null;
    });
    if (res == null) {
      throw Exception('Bad code');
    }
    print('USER ID: ${res?.user?.uid}');
    await StorageService.saveFirebaseId(res.user.uid);
    var tokenData = await res.user.getIdToken(true);
    // save it to store
    await StorageService.saveFirebaseToken(tokenData);

    // get user id
    var id = await api.request(getIdByToken(tokenData));
    if (id == null) {
      print('SEND "NEED REGISTRATION" to authState');
      authState.add(AuthState.NEED_REGISTRATION);
      return;
    }

    // check if user exists on server
    var status = await api.request(getWorkerStatus(tokenData, id));

    if (status != null) {
      // save it to store
      await StorageService.saveWorkerId(id);
      // if status is not null then user is already created
      authState.add(AuthState.LOGGED);
    } else {
      authState.add(AuthState.NEED_REGISTRATION);
    }
  }

  void dispose() {
    authState.close();
  }

  static AuthBloc of(BuildContext context, {bool listen = true}) {
    return Provider.of<AuthBloc>(context, listen: listen);
  }
}

enum AuthState {
  NEED_AUTH,
  FAILED,
  LOGGED,
  NEED_REGISTRATION,
}
