import 'dart:async';
import 'dart:io';

import 'package:car_sharing_services/api_client/api_client.dart';
import 'package:car_sharing_services/blocs/auth.bloc.dart';
import 'package:car_sharing_services/endpoints/feedback.dart';
import 'package:car_sharing_services/endpoints/history.dart';
import 'package:car_sharing_services/endpoints/registration.dart';
import 'package:car_sharing_services/endpoints/worker.dart';
import 'package:car_sharing_services/models/history.dart';
import 'package:car_sharing_services/models/reg_error.dart';
import 'package:car_sharing_services/models/worker.dart';
import 'package:car_sharing_services/service/firebase/auth/auth_service.dart';
import 'package:car_sharing_services/service/storage/storage_service.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

class ProfileBloc {
  BehaviorSubject<Worker> worker = BehaviorSubject();
  BehaviorSubject<CommonHistoryInfo> commonHistoryInfo = BehaviorSubject();
  BehaviorSubject<bool> approveToWork = BehaviorSubject();
  BehaviorSubject<List<RegError>> regErrors = BehaviorSubject();
  StreamSubscription _subscription;
  String token;

  String get userId => worker.value.id;

  String get regionId => worker.value.regionId;

  ApiClient api;
  BuildContext context;

  ProfileBloc(this.context, this.api, Worker worker) {
    this.worker.add(worker);
    _subscription = this.worker.listen((worker) async {
      if (worker == null) {
        return;
      }
      approveToWork
          .add(worker.statusId == workerStatusType[WorkerStatus.accepted]);
      if (worker.statusId != workerStatusType[WorkerStatus.accepted]) {
        regErrors.add(await getErrors());
      }
    });
  }

  updateWorkerInfo() async {
    token = token ?? await StorageService.getFirebaseToken();
    var worker = api.request(getWorkerById(userId, token));
    var info = api.request(getCommonHistoryInfo(userId));
    Future.wait([worker, info]).then((values) {
      if (values[0] != null) {
        this.worker.add(values[0]);
      }
      if (values[1] != null) {
        commonHistoryInfo.add(values[1]);
      }
    });
  }

  Future updateOnlyHistoryInfo() async {
    var info = await api.request(getCommonHistoryInfo(userId));
    if (info != null) {
      commonHistoryInfo.add(info);
    }
  }

  Future<List<RegError>> getErrors() async {
    token = token ?? await StorageService.getFirebaseToken();
    //var testId = '123e4567-e89b-12d3-a456-426614174001';
    var errors = await api.request(getRegErrors(token, userId));
    return errors;
  }

  void dispose() {
    worker.close();
    approveToWork.close();
    regErrors.close();
    commonHistoryInfo?.close();
    _subscription?.cancel();
  }

  logout() async {
    await AuthService().logout();
    AuthBloc.of(context, listen: false).authState.add(AuthState.NEED_AUTH);
    await StorageService.clear();
  }

  static ProfileBloc of(BuildContext context) {
    return Provider.of<ProfileBloc>(context);
  }

  Future<void> updatePhoto(File photo, PhotoType type) async {
    token = token ?? await StorageService.getFirebaseToken();
    if (photo != null)
      switch (type) {
        case PhotoType.passportMain:
          var link = await uploadTo(
            photo,
            '/photos/$userId/passport_main',
          );
          await api
              .request(updateWorker(
                  workerId: userId, token: token, passportMainPhoto: link))
              .whenComplete(() => updateWorkerInfo())
              .catchError((error) => print(error));
          break;
        case PhotoType.passportReg:
          var link = await uploadTo(
            photo,
            '/photos/$userId/passport_reg',
          );
          await api
              .request(updateWorker(
                  workerId: userId, token: token, passportRegPhoto: link))
              .whenComplete(() => updateWorkerInfo())
              .catchError((error) => print(error));
          break;
        case PhotoType.passportSelfie:
          var link = await uploadTo(
            photo,
            '/photos/$userId/passport_self',
          );
          await api
              .request(updateWorker(
                  workerId: userId, token: token, passportSelfiePhoto: link))
              .whenComplete(() => updateWorkerInfo())
              .catchError((error) => print(error));
          break;
        case PhotoType.licenseFront:
          var link = await uploadTo(
            photo,
            '/photos/$userId/license_front',
          );
          await api
              .request(updateWorker(
                  workerId: userId, token: token, licenseFrontPhoto: link))
              .whenComplete(() => updateWorkerInfo())
              .catchError((error) => print(error));
          break;
        case PhotoType.licenseBack:
          var link = await uploadTo(
            photo,
            '/photos/$userId/license_back',
          );
          await api
              .request(updateWorker(
                  workerId: userId, token: token, licenseBackPhoto: link))
              .whenComplete(() => updateWorkerInfo())
              .catchError((error) => print(error));
          break;
      }
  }

  Future<String> uploadTo(File photo, String path) async {
    StorageTaskSnapshot snapshot = await FirebaseStorage.instance
        .ref()
        .child(path)
        .putData(photo.readAsBytesSync())
        .onComplete;
    return snapshot.ref.getDownloadURL().then((value) => value as String);
  }

  Future<void> updateInfo(String name, String surname, String secondName,
      String birthday, bool male) async {
    token = token ?? await StorageService.getFirebaseToken();
    await api
        .request(updateWorker(workerId: userId, token: token, info: {
          'name': name,
          'surname': surname,
          'second_name': secondName,
          'male': male,
          'birthday': birthday,
        }))
        .whenComplete(() => updateWorkerInfo())
        .catchError((error) => print(error));
  }

  Future<void> updateLicense(
      String series, String number, String dateFrom, String dateTo) async {
    token = token ?? await StorageService.getFirebaseToken();
    await api
        .request(updateWorker(workerId: userId, token: token, licenseData: {
          'series': series,
          'number': number,
          'date_from': dateFrom,
          'date_to': dateTo,
        }))
        .whenComplete(() => updateWorkerInfo())
        .catchError((error) => print(error));
  }

  Future<void> updatePassport(String serial, String number, String date,
      String registration, String issuedBy) async {
    token = token ?? await StorageService.getFirebaseToken();
    await api
        .request(updateWorker(workerId: userId, token: token, passportData: {
          'serial': serial,
          'number': number,
          'date': date,
          'registration': registration,
          'issued_by': issuedBy,
        }))
        .whenComplete(() => updateWorkerInfo())
        .catchError((error) => print(error));
  }

  Future<void> updateCard(WorkerCard card) async {
    token = token ?? await StorageService.getFirebaseToken();
    await api
        .request(updateWorker(workerId: userId, token: token, card: card))
        .whenComplete(() => updateWorkerInfo())
        .catchError((error) => print(error));
  }

  Future<void> updateBankAccount(WorkerAccount account) async {
    token = token ?? await StorageService.getFirebaseToken();
    await api
        .request(updateWorker(workerId: userId, token: token, account: account))
        .whenComplete(() => updateWorkerInfo())
        .catchError((error) => print(error));
  }

  Future<void> updateEmail(String email) async {
    token = token ?? await StorageService.getFirebaseToken();
    await api
        .request(
            updateWorkerEmail(workerId: userId, token: token, email: email))
        .whenComplete(() => updateWorkerInfo())
        .catchError((error) => print(error));
  }

  Future<bool> sendFeedback({File photo, @required String feedback}) async {
    token = token ?? await StorageService.getFirebaseToken();
    var link = '';
    if (photo != null) {
      link = await uploadTo(photo, '/photos/$userId/reviews');
    }
    try {
      await api.request(sendReview(
          workerId: userId, token: token, text: feedback, photoLink: link));
    } catch (e) {
      print('sendFeedback: error occurred $e');
      return false;
    }
    return true;
  }
}

enum BadDataType {
  badPassportData,
  badPassportFirstPhoto,
  badPassportRegPhoto,
  badPassportSelfie,
  badLicenseData,
  badLicenseFrontPhoto,
  badLicenseBackPhoto,
  badPersonalData,
  badBankData,
  badCardData,
  badEmailData
}

Map<int, BadDataType> errorType = {
  1: BadDataType.badPassportData,
  2: BadDataType.badPassportFirstPhoto,
  3: BadDataType.badPassportRegPhoto,
  4: BadDataType.badPassportSelfie,
  5: BadDataType.badLicenseData,
  6: BadDataType.badLicenseFrontPhoto,
  7: BadDataType.badLicenseBackPhoto,
  8: BadDataType.badPersonalData,
  9: BadDataType.badBankData,
  10: BadDataType.badCardData,
  11: BadDataType.badEmailData
};

enum WorkerStatus { accepted, needModerate, declined }

Map<WorkerStatus, int> workerStatusType = {
  WorkerStatus.accepted: 1,
  WorkerStatus.needModerate: 2,
  WorkerStatus.declined: 3,
};

enum PhotoType {
  passportMain,
  passportReg,
  passportSelfie,
  licenseFront,
  licenseBack,
}
