import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

/// Bloc which is visible from entire app. Manages some common things
/// like checking internet connection
class CoreBloc {
  BehaviorSubject<bool> _hasInetConnection = BehaviorSubject.seeded(false);
  StreamSubscription _subscriptionConnectivity;
  StreamSubscription _subscriptionConnectionChecker;
  PublishSubject<int> barIndex = PublishSubject();



  Stream get connection => _hasInetConnection.stream;

  bool get hasConnection => _hasInetConnection.value;

  CoreBloc() {
    _subscriptionConnectionChecker =
        DataConnectionChecker().onStatusChange.listen((status) {
      if (status == DataConnectionStatus.disconnected &&
          (_hasInetConnection?.value ?? true)) {
        _hasInetConnection.add(false);
      } else if (status == DataConnectionStatus.connected &&
          !(_hasInetConnection.value ?? false)) {
        _hasInetConnection.add(true);
      }
    });
    _subscriptionConnectivity = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) async {
      if (result != ConnectivityResult.none) {
        _hasInetConnection.add(await DataConnectionChecker().hasConnection);
      } else {
        _hasInetConnection.add(false);
      }
    });
  }

  void dispose() {
    _hasInetConnection.close();
    _subscriptionConnectivity?.cancel();
    _subscriptionConnectionChecker?.cancel();
    barIndex?.close();
  }

  static CoreBloc of(BuildContext context) {
    return Provider.of<CoreBloc>(context);
  }
}
