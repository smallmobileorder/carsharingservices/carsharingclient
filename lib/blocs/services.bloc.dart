import 'package:car_sharing_services/api_client/api_client.dart';
import 'package:car_sharing_services/blocs/profile.bloc.dart';
import 'package:car_sharing_services/endpoints/services.dart';
import 'package:car_sharing_services/models/services.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

class ServicesBloc {
  BehaviorSubject<List<FullAgent>> fullAgents = BehaviorSubject.seeded([]);

  BuildContext context;
  ApiClient api;
  ProfileBloc profileBloc;

  ServicesBloc(this.context, this.api, this.profileBloc);

  Future<void> updateAgents() {
    return api
        .request(getFullAgentsForRegion(profileBloc.regionId))
        .then((value) => fullAgents.add(value));
  }

  void dispose() {
    fullAgents.close();
  }

  static ServicesBloc of(BuildContext context) {
    return Provider.of<ServicesBloc>(context);
  }
}
