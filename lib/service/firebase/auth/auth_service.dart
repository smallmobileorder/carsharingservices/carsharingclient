import 'package:firebase_auth/firebase_auth.dart';

import '../../../application.dart';

class AuthService {
  static final AuthService _authService = AuthService._internal();

  factory AuthService() => _authService;

  AuthService._internal();

  String _verificationID;
  int _forceResendingToken;

  FirebaseAuth get _auth => FirebaseAuth.instance;

  User get currentUser => _auth.currentUser;

  bool get isAuthorized => _auth.currentUser != null;

  Future<void> auth(final String phone, final Function(User) completion,
      final Function codeSent) {
    return _auth.verifyPhoneNumber(
        phoneNumber: phone,
        timeout: Duration(seconds: 25),
        forceResendingToken: _forceResendingToken,
        verificationCompleted: (credential) async {
          completion((await _auth.signInWithCredential(credential))?.user);
        },
        verificationFailed: (_) => completion(null),
        codeSent: (verificationID, [resendingToken]) {
          logger.i('$verificationID $resendingToken');
          _verificationID = verificationID;
          _forceResendingToken = resendingToken;
          codeSent();
        },
        codeAutoRetrievalTimeout: null);
  }

  Future<User> verify(final String smsCode) async {
    logger.i('id $_verificationID, code $smsCode');
    final credential = PhoneAuthProvider.credential(
        verificationId: _verificationID, smsCode: smsCode);
    return (await _auth
            .signInWithCredential(credential)
            .catchError((error) => null))
        ?.user;
  }

  Future<void> logout() {
    return _auth.signOut();
  }
}
