import 'dart:convert';
import 'dart:io';

import 'package:car_sharing_services/app_settings.dart';
import 'package:car_sharing_services/application.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';

const _kBackendKey = 'production_v1.2';

class RemoteConfigService {
  static final defaultServerAddr = 'tscars-develop.ru:8080';

  ///Return String array with two versions
  /// array[0] - minimal version, which need to correct work
  /// array[1] - the newest version of app, which u can download from store
  ///
  static Future<List<String>> getVersions() async {
    final RemoteConfig remoteConfig = await RemoteConfig.instance;

    String platform;
    if (Platform.isIOS) {
      platform = 'ios_version';
    } else {
      platform = 'android_version';
    }

    String configCurrentVersion;
    String configMinimalVersion;

    try {
      await remoteConfig.fetch(expiration: const Duration(seconds: 0));
      await remoteConfig.activateFetched();
      String version = remoteConfig.getString(platform);

      try {
        Map<String, dynamic> json = jsonDecode(version);
        json = json.cast<String, String>();
        configCurrentVersion = json['current'];
        configMinimalVersion = json['minimal'];
      } catch (e) {
        print('ERROR AT DECODE VERSION FROM REMOTE CONFIG: $e');
        configCurrentVersion = AppSettings.defaultVersion;
        configMinimalVersion = AppSettings.defaultVersion;
      }
    } on FetchThrottledException catch (exception) {
      print('FetchThrottledException AT REMOTE CONFIG: $exception');
      configMinimalVersion = AppSettings.defaultVersion;
      configCurrentVersion = AppSettings.defaultVersion;
    } catch (exception) {
      print('ERROR REMOTE CONFIG: $exception');
      configMinimalVersion = AppSettings.defaultVersion;
      configCurrentVersion = AppSettings.defaultVersion;
    }
    return [configMinimalVersion, configCurrentVersion];
  }

  static Future<String> getStoreLink() async {
    final RemoteConfig remoteConfig = await RemoteConfig.instance;

    String storeLink;

    try {
      await remoteConfig.fetch();
      await remoteConfig.activateFetched();
      String version = remoteConfig.getString('store');

      try {
        Map<String, dynamic> json = jsonDecode(version);
        json = json.cast<String, String>();
        if (Platform.isIOS) {
          storeLink = json['ios'];
        } else {
          storeLink = json['android'];
        }
      } catch (e) {
        print('ERROR AT DECODE STORE LINK FROM REMOTE CONFIG: $e');
      }
    } on FetchThrottledException catch (exception) {
      print('FetchThrottledException AT REMOTE CONFIG: $exception');
    } catch (exception) {
      print('ERROR AT REMOTE CONFIG: $exception');
    }
    if (storeLink?.isEmpty ?? true) {
      if (Platform.isIOS) {
        storeLink = 'http://appstore.com/';
      } else {
        storeLink = "https://play.google.com/store/apps/";
      }
    }
    return storeLink;
  }

  static Future<String> getServerAddress() async {
    final RemoteConfig remoteConfig = await RemoteConfig.instance;

    String serverAddress;

    try {
      await remoteConfig.fetch();
      await remoteConfig.activateFetched();
      String version = remoteConfig.getString('server_address');

      try {
        Map<String, dynamic> json = jsonDecode(version);
        json = json.cast<String, String>();
        serverAddress = json[_kBackendKey];
      } catch (e) {
        print('ERROR AT DECODE SERVER ADDRESS FROM REMOTE CONFIG: $e');
      }
    } on FetchThrottledException catch (exception) {
      print('FetchThrottledException AT REMOTE CONFIG: $exception');
    } catch (exception) {
      print('ERROR AT REMOTE CONFIG: $exception');
    }
    if (serverAddress.isEmpty ?? true) {
      serverAddress = defaultServerAddr;
    }
    logger.i('server address: $serverAddress');
    return serverAddress;
  }
}
