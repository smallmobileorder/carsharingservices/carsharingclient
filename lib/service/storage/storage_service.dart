import 'package:flutter_secure_storage/flutter_secure_storage.dart';

const _authKey = 'auth';
const _tokenKey = 'token';
const _uidKey = 'uid';

const _firebaseToken = 'firebase_token';
const _workerId = 'worker_id';
const _firebaseId = 'firebase_id';

class StorageService {
  static Future<String> getFirebaseToken() async {
    FlutterSecureStorage storage = FlutterSecureStorage();
    return await storage.read(key: _firebaseToken);
  }

  static Future<void> saveFirebaseToken(String token) async {
    FlutterSecureStorage storage = FlutterSecureStorage();
    return await storage.write(key: _firebaseToken, value: token);
  }

  static Future<String> getWorkerId() async {
    FlutterSecureStorage storage = FlutterSecureStorage();
    return await storage.read(key: _workerId);
  }

  static Future<void> saveWorkerId(String id) async {
    FlutterSecureStorage storage = FlutterSecureStorage();
    return await storage.write(key: _workerId, value: id);
  }

  static Future<String> getFirebaseId() async {
    FlutterSecureStorage storage = FlutterSecureStorage();
    return await storage.read(key: _firebaseId);
  }

  static Future<void> saveFirebaseId(String id) async {
    FlutterSecureStorage storage = FlutterSecureStorage();
    return await storage.write(key: _firebaseId, value: id);
  }

  static Future<bool> isAuthorized() async {
    FlutterSecureStorage storage = FlutterSecureStorage();
    return (await storage.read(key: _authKey)) == 'true';
  }

  static Future<void> setAuth(bool isAuthorized) async {
    FlutterSecureStorage storage = FlutterSecureStorage();
    await storage.write(key: _authKey, value: isAuthorized.toString());
  }

  static Future<void> saveToken(String token) async {
    FlutterSecureStorage storage = FlutterSecureStorage();
    await storage.write(key: _tokenKey, value: token);
  }

  static Future<String> getToken() async {
    FlutterSecureStorage storage = FlutterSecureStorage();
    return await storage.read(key: _tokenKey);
  }

  static Future<void> saveUid(String uid) async {
    FlutterSecureStorage storage = FlutterSecureStorage();
    return await storage.write(key: _uidKey, value: uid);
  }

  static Future<String> getUid() async {
    FlutterSecureStorage storage = FlutterSecureStorage();
    return await storage.read(key: _uidKey);
  }

  static Future<void> clear() async {
    FlutterSecureStorage storage = FlutterSecureStorage();
    return await storage.deleteAll();
  }
}
