import 'package:car_sharing_services/service/firebase/remote_config/remote_config.dart';
import 'package:flutter/widgets.dart';

import 'application.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  String serverAddr = await RemoteConfigService.getServerAddress();
  runApp(Application(
    serverAddr: serverAddr,
  ));
}
